Dependencies
============

Compilation requires the following software:

* cmake >= 3.3

* doxygen

* gcc or clang C and C++ compilers

* git

* graphviz

Compilation also requires development files for the following libraries:

* jbig2dec-devel

* libjpeg or libjpeg-turbo

* libtiff

* openjpeg2-devel

* tesseract

* poppler-qt5

* qpdf-devel

* Qt5 >= 5.5

* zlib-devel

Runtime:

* Qt Image format plugin for TIFF files, often contained in a package called
  'qt5-qtimageformats', or similar.

Compiling
=========

Scantools uses the standard cmake build system, so compilation is
straightforward.  We include several build script to make life easier.

* The script *buildscript-linux-release.sh* must be called in the main source
  directory.  It creates a subdirectory "build-release" and builds the software
  there.

* The script *buildscript-linux-debug.sh* must be called in the main source
  directory.  It creates a subdirectory "build-debug" and builds the software
  there. The script requires the "clang" compilers. It builds the software in
  "debug" mode and enables clang's "address" and "undefined" sanitisers.

Testing
=======

To run a number of unit tests, go to the build directory and run "make test"
there.

Installation
============

For installation, go to the build directory and run "make install" there.


