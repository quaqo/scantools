#!/bin/bash
set -e

#
# Copyright © 2021 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
#
#
# This script builds a snap for the scantools suite, ready for upload to snapcraft.io
#
# Run this script in the main directory tree.
#

./buildscript-linux-release.sh
cd build-release/packaging
snapcraft clean
snapcraft
cd ../..
