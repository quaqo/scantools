---
title: "hocr2any"
description: "hocr2any"
---


The program **hocr2any** converts HOCR files (which represent the output of a
character reckognition engine) to text, to PDF, or to a variety of raster
graphic formats.


## Frequently asked questions

### Aren’t there similar programs, including those contained in the hocrtools and exactImage packages?

There are. As far as I can see, **hocr2any** produces output that is better than
that of many of the competing programs. But judge for yourself!


### Can this program be used to make PDF files that I have generated from scans searchable?

Yes. Note, however, that the program [image2pdf](image2pdf) is able to include
OCR data natively.
