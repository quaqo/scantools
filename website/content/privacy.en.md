---
title: "Privacy"
menu: footer # Optional, add page to a menu. Options: main, side, footer
---

The privacy policy is only available in German language.  Please have a look at
[this page](/de/privacy).