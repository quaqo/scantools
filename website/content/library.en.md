---
title: "Library"
description: "Library"
menu: main
---

The <strong>scantools</strong> library builds on Qt5.

- Detailed API documentation is found
  [here](https://cplx.vm.uni-freiburg.de/storage/scantools-api/) is found here.
- The source code is hosted on GitLab, and is available from the [scantools
  GitLab page](https://gitlab.com/kebekus/scantools).
