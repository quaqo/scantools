---
title: "Download"
description: "Download"
menu: main
---

## Binaries for Linux

<a href='https://snapcraft.io/scantools'><img width='100px' alt="Get it from the
Snap Store"
src='https://snapcraft.io/static/images/badges/en/snap-store-black.svg'
style='vertical-align:middle'/></a>

We offer [packages for all common Linux
distributions](https://software.opensuse.org/download/package.iframe?project=home:stefan_kebekus&amp;amp;package=scantools).
In addition to the binaries, the packages also include manual pages, development
files and API documentation.

Alternatively, you can download the software as a snap from
[snapcraft.io](https://snapcraft.io/scantools).  The binaries exposed by the
snap are called **scantools.image2pdf**, **scantools.hocr2any** and
**scantools.ocrPDF**.  Manual pages are not included; use to option "--help" for
usage information.


## Binaries for other platforms

I expect that scantools should compile on all modern platforms, desktop or
mobile. Due to limited time, I support Linux only.


## Source code

The source code is hosted on GitLab, and is available from the [scantools GitLab
  page](https://gitlab.com/kebekus/scantools).
