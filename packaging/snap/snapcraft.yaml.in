name: scantools
base: core18
grade: stable
confinement: strict
license: GPL-3.0
architectures: [amd64]
description: |

  Scantools is a high-quality library and a matching set of command-line
  programs for the handling and manipulation of scanned documents. At present,
  the tools can convert image files to PDF/A. Files in JBIG2, JPEG, and JPEG2000
  format are directly included in the PDF, other files are compressed in a
  lossless manner. HOCR files, which are produced by optical character
  recognition programs such as ‘tesseract’, can be used to make the PDF file
  searchable. The resulting files comply with the ISO PDF/A standard for
  long-term archiving of digital documents and offer compression rates
  comparable to that of the DJVU file format.  There are currently three
  command-line utilities.

  - scantools.image2pdf, converts images to a PDF/A compliant PDF file.

  - scantools.hocr2any, converts HOCR files to text or renders them as raster
    graphics or PDF files.

  - scantools.ocrPDF, adds a text layer to a graphics-only PDF file, without
    re-encoding graphics data or otherwise modifying file content.

summary: Command line utilities for conversion of scanned documents
version: @PROJECT_VERSION@

apps:
  hocr2any:
    command: usr/bin/hocr2any
    environment:
      TESSDATA_PREFIX: $SNAP/usr/share/tesseract-ocr/4.00/tessdata
  image2pdf:
    command: usr/bin/image2pdf
    environment:
      TESSDATA_PREFIX: $SNAP/usr/share/tesseract-ocr/4.00/tessdata
    extensions:
      - kde-neon
  ocrPDF:
    command: usr/bin/ocrPDF
    environment:
      TESSDATA_PREFIX: $SNAP/usr/share/tesseract-ocr/4.00/tessdata
      
parts:
  scantools:
    plugin: cmake
    source: https://gitlab.com/kebekus/scantools.git
    source-type: git
    source-commit: @GIT_COMMIT@
    build-snaps:
      - cmake
      - kde-frameworks-5-core18-sdk
      - kde-frameworks-5-core18
    build-packages:
      - libjbig2dec0-dev
      - libleptonica-dev
      - libglvnd-dev
      - libpoppler-qt5-dev
      - libqpdf-dev
      - libzopfli-dev
      - libtesseract-dev
      - doxygen
      - graphviz
    stage-snaps:
      - kde-frameworks-5-core18
    stage-packages:
      - freeglut3
      - libgpm2
      - libjbig2dec0
      - libpoppler-qt5-1
      - libslang2
      - libzopfli1
      - poppler-data
      - qpdf
      - tesseract-ocr-all
    configflags:
      - "-DKDE_INSTALL_USE_QT_SYS_PATHS=ON"
      - "-DCMAKE_INSTALL_PREFIX=/usr"
      - "-DCMAKE_BUILD_TYPE=Release"
      - "-DENABLE_TESTING=OFF"
      - "-DBUILD_TESTING=OFF"
      - "-DKDE_SKIP_TEST_SETTINGS=ON"
