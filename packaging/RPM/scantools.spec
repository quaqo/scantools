#
# spec file for package scantools (Version ${PROJECT_VERSION})
#
# Copyright (c) ${COPYRIGHTYEAR} Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
#
#   This file is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the
#   Free Software Foundation, Inc.,
#   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# Please submit bugfixes or comments to stefan.kebekus@math.uni-freiburg.de
#

Name:           scantools
Group:          Application/Publishing

BuildRequires:  cmake gcc-c++
%if %{defined suse_version}
BuildRequires:  update-desktop-files
%endif
%if %{defined fedora_version}
BuildRequires:  libjpeg-turbo-devel poppler-qt5-devel qt5-qtbase-devel >= 5.5 tesseract-devel zopfli-devel
%else
BuildRequires:  libjpeg-devel libpoppler-qt5-devel libqt5-qtbase-devel >= 5.5 libzopfli-devel tesseract-ocr-devel
%endif
BuildRequires:  cmake >= 3.3.0 doxygen gcc-c++ git graphviz jbig2dec-devel openjpeg2-devel qpdf-devel zlib-devel

License:        GPL-3.0+
Summary:        Graphics manipulation for handling of scanned documents
Version:        ${PROJECT_VERSION}
Release:        1.0
Url:            https://cplx.vm.uni-freiburg.de/scantools
BuildRoot:      %{_tmppath}/%{name}-%{version}-build
Source:         %name-%version.tar.gz



%description
Scantools is a library and a matching set of command line programs for
manipulation of graphic files, written with a view towards handling of scanned
documents.


%prep 
%setup -q
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=RELWITHDEBINFO -DCMAKE_INSTALL_PREFIX=/usr ..


%build
cd build
make


%install
cd build
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install


%post
ldconfig
echo #avoid rpmlint warning on openSUSE systems


%postun
ldconfig
echo #avoid rpmlint warning on openSUSE systems


%clean
rm -rf $RPM_BUILD_ROOT

%files 
%defattr(-,root,root)
%{_libdir}/libscantools.so.*
/usr/bin/hocr2any
/usr/bin/image2pdf
/usr/bin/ocrPDF
/usr/share/man/man1/*
%doc ChangeLog
%doc LICENSE.md
%doc README.md
%docdir /usr/share/doc/scantools
%docdir /usr/share/man/man1

%changelog
* Mon Jan 13 2020 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.6.5
* Wed Feb 13 2019 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.6.4
* Tue Sep 25 2018 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.6.3
* Wed Sep 05 2018 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.6.2
* Fri Jul 06 2018 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.6.1
* Thu Jun 14 2018 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.6.0
* Fri May 18 2018 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.8
* Fri May 04 2018 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.7
* Thu Apr 05 2018 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.6
* Mon Dec 18 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.5
* Wed Nov 15 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.4
* Fri Oct 20 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.3
* Tue Oct 17 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.2
* Fri Sep 22 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.1
* Thu Jul 20 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.5.0
* Fri May 19 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.4.2
* Mon May 15 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.4.1
* Thu Apr 06 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.4.0
* Mon Mar 06 2017 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.3.2
* Thu Sep 15 2016 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.3.1
* Tue Sep 13 2016 stefan.kebekus@math.uni-freiburg.de
- Update to version 0.3.0
* Fri Jul 01 2016 stefan.kebekus@math.uni-freiburg.de
- Initial version


%package devel
Summary:        Development files for the scantools library
Group:          Development/Libraries/C++
%if %{defined fedora_version}
Requires:       qt5-qtbase-devel >= 5.5
%else
Requires:       libqt5-qtbase-devel >= 5.5
%endif
Requires:       libscantools = %{version}

%description devel
Scantools is a library and a matching set of command line programs for
manipulation of graphic files, written with a view towards handling of scanned
documents.

%files devel
%defattr(-,root,root)
%{_libdir}/libscantools.so
/usr/include/scantools
/usr/share/doc/scantools-devel
