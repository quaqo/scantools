# FindJBIG2DEC
# ----------
#
# Try to find JBIG2DEC
#
# Once done this will define
#
# ::
#
#   JBIG2DEC_INCLUDE_DIR - the JBIG2DEC include directory
#   JBIG2DEC_LIBRARIES - The libraries needed to use JBIG2DEC


find_path(JBIG2DEC_INCLUDE_DIR jbig2.h )

find_library(JBIG2DEC_LIBRARIES NAMES jbig2dec jbig2dec-15 libjbig2dec-15 libjbig2dec)

mark_as_advanced(JBIG2DEC_INCLUDE_DIR JBIG2DEC_LIBRARIES)

find_package_handle_standard_args(Jbig2dec FOUND_VAR JBIG2DEC_FOUND REQUIRED_VARS JBIG2DEC_INCLUDE_DIR JBIG2DEC_LIBRARIES)
