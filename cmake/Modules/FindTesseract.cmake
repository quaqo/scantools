# FindTESSERACT
# ----------
#
# Try to find TESSERACT
#
# Once done this will define
#
# ::
#
#   TESSERACT_INCLUDE_DIR - the TESSERACT include directory
#   TESSERACT_LIBRARIES - The libraries needed to use TESSERACT
#   TESSERACT_HAS_OEM_LSTM_ONLY - boolean, indicating if the enum item tesseract::OEM_LSTM_ONLY is defined

find_path(TESSERACT_INCLUDE_DIR tesseract/baseapi.h )

find_library(TESSERACT_LIBRARIES NAMES libtesseract tesseract)

mark_as_advanced(TESSERACT_INCLUDE_DIR TESSERACT_LIBRARIES)

find_package_handle_standard_args(Tesseract FOUND_VAR TESSERACT_FOUND REQUIRED_VARS TESSERACT_INCLUDE_DIR TESSERACT_LIBRARIES)

include(CheckCXXSourceCompiles)

check_cxx_source_compiles("
#include <tesseract/baseapi.h>

int main() {
 return tesseract::OEM_LSTM_ONLY;
}
"
TESSERACT_HAS_OEM_LSTM_ONLY)
