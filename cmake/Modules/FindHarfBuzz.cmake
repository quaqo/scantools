# FindHarfBuzz
# ----------
#
# Try to find HarfBuzz
#
# Once done this will define
#
# ::
#
#   HarfBuzz_INCLUDE_DIR - the HarfBuzz include directory
#   HarfBuzz_LIBRARIES - The libraries needed to use MUPDF

find_path(HarfBuzz_INCLUDE_DIR harfbuzz/hb.h)

find_library(HarfBuzz_LIBRARIES NAMES harfbuzz)

mark_as_advanced(HarfBuzz_INCLUDE_DIR HarfBuzz_LIBRARIES)

find_package_handle_standard_args(HarfBuzz
  REQUIRED_VARS HarfBuzz_INCLUDE_DIR HarfBuzz_LIBRARIES
  )
