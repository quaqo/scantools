# FindLEPTONICA
# ----------
#
# Try to find LEPTONICA
#
# Once done this will define
#
# ::
#
#   LEPTONICA_INCLUDE_DIR - the LEPTONICA include directory
#   LEPTONICA_LIBRARIES - The libraries needed to use LEPTONICA

find_path(LEPTONICA_INCLUDE_DIR leptonica/allheaders.h )

find_library(LEPTONICA_LIBRARIES NAMES libleptonica leptonica lept)

mark_as_advanced(LEPTONICA_INCLUDE_DIR LEPTONICA_LIBRARIES)

find_package_handle_standard_args(Leptonica FOUND_VAR LEPTONICA_FOUND REQUIRED_VARS LEPTONICA_INCLUDE_DIR LEPTONICA_LIBRARIES)
