# FindOpenJP2
# ----------
#
# Try to find OpenJP2
#
# Once done this will define
#
# ::
#
#   OpenJP2_INCLUDE_DIR - the OpenJP2 include directory
#   OpenJP2_LIBRARIES   - the libraries needed to use OpenJP2
#   OpenJP2_VERSION     - version information

# Use PKGCONFIG to find reasonable default values
FIND_PACKAGE(PkgConfig REQUIRED)
PKG_CHECK_MODULES(PC_OpenJP2 REQUIRED libopenjp2)

# Find include dir
FIND_path(OpenJP2_INCLUDE_DIR
  NAMES openjpeg.h
  PATHS ${PC_OpenJP2_INCLUDE_DIRS}
  PATH_SUFFIXES openjpeg
  )

# Find library
FIND_library(OpenJP2_LIBRARIES
  NAMES openjp2
  PATHS ${PC_OpenJP2_LIBRARY_DIRS}
  )

# Now put things together
SET(OpenJP2_VERSION ${PC_OpenJP2_VERSION})

FIND_package_handle_standard_args(OpenJP2
  REQUIRED_VARS OpenJP2_INCLUDE_DIR OpenJP2_LIBRARIES
  )

MARK_AS_ADVANCED(OpenJP2_INCLUDE_DIR OpenJP2_LIBRARIES)
