# FindZOPFLI
# ----------
#
# Try to find ZOPFLI
#
# Once done this will define
#
# ::
#
#   ZOPFLI_INCLUDE_DIR - the ZOPFLI include directory
#   ZOPFLI_LIBRARIES - The libraries needed to use ZOPFLI
#   ZOPFLI_HAS_OEM_LSTM_ONLY - boolean, indicating if the enum item zopfli::OEM_LSTM_ONLY is defined

find_path(ZOPFLI_INCLUDE_DIR zopfli.h PATH_SUFFIXES zopfli)

find_library(ZOPFLI_LIBRARIES NAMES libzopfli zopfli)

mark_as_advanced(ZOPFLI_INCLUDE_DIR ZOPFLI_LIBRARIES)

find_package_handle_standard_args(Zopfli FOUND_VAR ZOPFLI_FOUND REQUIRED_VARS ZOPFLI_INCLUDE_DIR ZOPFLI_LIBRARIES)
