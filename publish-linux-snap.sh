#!/bin/bash
set -e

#
# Copyright © 2021 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
#
#
# This script builds and published a snap for the scantools suite, ready for
# upload to snapcraft.io
#
# Run this script in the main directory tree.
#

./buildscript-linux-snap.sh
echo "Publishing to snapcraft.io"
snapcraft login --with ~/.ubuntuLogin
snapcraft upload --release stable build-release/packaging/scantools_?.?.?_amd64.snap
