#!/bin/bash

#
# Copyright © 2016 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
#
#
# This script builds the scantools library and executables.
#
# Run this script in the main directory tree.

#
# Clean
#

rm -rf build-release

#
# Build the executable
#

mkdir build-release
cd build-release
cmake -DQT_NO_DEPRECATED_WARNINGS=ON \
      -DCMAKE_UNITY_BUILD:BOOL=ON \
      -DCMAKE_BUILD_TYPE=release \
      -DCMAKE_INSTALL_PREFIX=/ \
      -G Ninja \
      ..
ninja
cd ..


#
# Terminate
#

if [ -x build-release/src/apps/image2pdf/image2pdf ]; then
    echo "********************************"
    echo "The program built successfully."
else
    echo "********************************"
    echo "The program did not build successfully."
fi
