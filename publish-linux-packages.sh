#!/bin/bash
set -e

#
# Copyright © 2021 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
#
#
# This script builds and published a snap for the scantools suite, ready for
# upload to snapcraft.io
#
# Run this script in the main directory tree.
#

./buildscript-linux-release.sh
cd build-release
ninja packaging

osc checkout home:stefan_kebekus/scantools
cd home\:stefan_kebekus/scantools
osc rm *
cp ../../packaging/debian/* .
cp ../../packaging/RPM/* .
osc add *
osc commit -m "New upstream version"
