/*
 * Copyright © 2016-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef JBIG2DOCUMENT
#define JBIG2DOCUMENT 1

#include <QSet>

#include "imageInfo.h"
#include "JBIG2Segment.h"


/** Reads, writes and renders JBIG2 files, and chops them into pieces for
    inclusion into a PDF document
    
    This class reads, interprets and renders JBIG2 image files. There exists
    minimal support for file validation, inspection and manipulation.  The main
    use of this class is to chop JBIG2 files into pieces which can then be
    included into a PDF file.

    The methods of this class are reentrant, but not thread safe.
*/

class JBIG2Document
{
 public:
  /** Creates an empty JBIG2 document */
  inline JBIG2Document() {clear();};
  
  /** Constructs a JBIG2 document from a QIODevice
      
      This is a convenience constructor that constructs an empty document and
      calls read(device).
      
      @param device Pointer to a QIODevice from which the file is read
  */
  explicit inline JBIG2Document(QIODevice *device) {read(device);};
  
  /** Constructs a JBIG2 document from a file

      This is a convenience constructor that constructs an empty document, opens
      a file and calls read(QIODevice *device).
      
      @param fileName Name of the JBIG2 file
  */
  explicit inline JBIG2Document(QString fileName) {read(fileName);};
  
  /** Resets the document
      
      This method clears all errors, warnings, and any file content.
   */ 
  void clear();
  
  /** Reads a JBIG2 document from a QIODevice

      This method clears the document, and reads a JBIG2 file from a QIODevice.
      After method returns, the caller shoud check if an error occurred, by
      using the methods hasError() and/or error().  The caller might also wish
      to check if the file contained fixable errors, by calling the method
      warnings().

      The absense of an error or of warnings does not imply that the JBIG2 file
      is valid. In fact, only minimal checks are made.
      
      @param device QIODevice from which the file should be read
  */
  void read(QIODevice *device);

  
  /** Reads a JBIG2 document from a file
      
      This is a convenience method that opens a file and calls read(QIODevice
      *device).
      
      @param fileName Name of the JBIG2 file
  */
  void read(const QString& fileName);
  
  
  /** Checks if the document is empty
      
      @returns True if the document does not contain any pages.
  */
  inline bool isEmpty() const
  {
    QListIterator<JBIG2Segment> i(segments);
    while (i.hasNext()) {
      quint32 number = i.next().pageAssociation();
      if (number != 0)
	return false;
    }
    return true;
  };
  
  /** Error status
      
      @returns True if read() terminated with an error. In that case, an error
      description can be retrieved using the error() method.
  */
  inline bool hasError() const {return !_error.isEmpty();};
  
  /** Error message 
      
      @returns An empty string if read() succeeded.  Otherwise, this method
      returns a description of the error in enlish language.
  */
  inline QString error() const {return _error; };

  /** Warning status

      @returns True if read() found issues in the JBIG2 file that could be
      fixed.  For instance, Google's 'jbig2' encoder is known to produce JBIG2
      files that contain segements whose 'retain bit for this segment' contains
      wrong values.
   */
  inline bool hasWarnings() const {return !_warnings.isEmpty();};

  /** Warning messages

      @returns A list descriptions of fixable issues found in the JBIG2 file.
  */
  inline QSet<QString> warnings() const {return _warnings;};

  /** Page numbers used by this document

      @returns List of page numbers that appear in this document
   */
  QList<quint32> pageNumbers() const;


  /** Human-readable info string about the document
      
      This method is mainly useful for debugging purposes.
      
      @returns A QString with text describing the document. Lines are separated
      by newline characters, '\n'.
   */
  QString info() const;

  
  /** Return data ready for inclusion into a PDF file

      This method returns a concatenation of all JBIG2 segments pertaining to a
      given page.  Following the PDF specification, all end-of-page segements
      are removed, and all page associations other than '0' are set to '1'.
      This method must not be called when an error condition exists. Use
      hasError() or error() to check the error status first.

      @param pageNumber If zero, the method returns the global data, which is
      used by all pages of the document. If larger than zero, the method returns
      the data for the given page.  

      @returns A QByteArray containg the data, or an empty array if no data
      could be found.
   */
  QByteArray getPDFDataChunk(quint32 pageNumber) const;
  

  /** Compute imageInfo describing a given page

      - Resolutions are set to zero if they are not specified in the JBIG2 file,
      of if the resolution is specified, but falls out of the bounds given by
      resolution::minResDPI and resolution::maxResDPI.

      - If the page does not exist, or if no "page info segment" could be found
      in the JBIG2 data, and empty bitmap info is returned.

      @warning This method must not be called when an error condition exists. Use
      hasError() or error() to check the error status first.
      
      @param pageNumber Number of the page that will be described

      @returns imageInfo that describes the given page. 
  */
  imageInfo pageInfo(quint32 pageNumber) const;

  /** Render image
      
      @param pageNumber Number of the page that rendered
      
      @returns A QImage with the rendered page, or a null image in case of error
  */
  QImage operator[](quint32 pageNumber) const;
  
 private:
  // Checks for fixable errors, fixes them, and sets '_warnings' accordingly
  void check_n_fix();
  
  // Helper function for image rendering
  static void myImageCleanupHandler(void *info);
  
  // Returns list of numbers of those segements that refer to given
  // segmentNumber
  QList<quint32> referrers(quint32 segmentNumber);

  // Warnings
  QSet<QString> _warnings;

  // Error
  QString _error;

  // List of segments that make up the JBIG2 file
  QList<JBIG2Segment> segments;
};

#endif
