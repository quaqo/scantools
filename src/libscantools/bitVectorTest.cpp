/*
 * Copyright © 2017 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bitVector.h"
#include "bitVectorTest.h"

QTEST_MAIN(bitVectorTest)


void bitVectorTest::test()
{
    // Set up a vector of 5 bits
    bitVector vector(5);

    // check that all bits are set to zero
    for(quint32 i=0; i<5; i++) {
        QVERIFY(vector.getBit(i) == 0);
    }

    vector.setBit(3, 0);
    QVERIFY(vector.getBit(3) == 0);
    vector.setBit(3, 1);
    QVERIFY(vector.getBit(3) == 1);
    vector.setBit(3, 1);
    QVERIFY(vector.getBit(3) == 1);

    bitVector::miniBitVector mbv{5, 0b11001};
    vector.append(mbv);
    QVERIFY(vector.getBit(5) == 1);
    QVERIFY(vector.getBit(6) == 1);
    QVERIFY(vector.getBit(7) == 0);
    QVERIFY(vector.getBit(8) == 0);
    QVERIFY(vector.getBit(9) == 1);

    QByteArray array = vector;
    QVERIFY(array[0] = 0b00010110);
    QVERIFY(array[1] = 0b01000000);
}

void bitVectorTest::getBit_mBV()
{
    bitVector::miniBitVector mbv = {5, 0b10011};

    QCOMPARE(mbv.getBit(0), (quint8)1);
    QCOMPARE(mbv.getBit(1), (quint8)0);
    QCOMPARE(mbv.getBit(2), (quint8)0);
    QCOMPARE(mbv.getBit(3), (quint8)1);
    QCOMPARE(mbv.getBit(4), (quint8)1);
}

void bitVectorTest::startsWith()
{
    bitVector::miniBitVector mbv = {5, 0b10011};

    bitVector vct;
    vct.append(mbv);
    vct.append(mbv);

    for(quint32 i=0; i<15; i++) {
        if ((i == 0) || (i == 5)) {
            QCOMPARE(vct.startsWith(mbv,i), true);
        } else {
            QCOMPARE(vct.startsWith(mbv,i), false);
        }
    }
}


void bitVectorTest::alignment()
{
    std::array<quint8,256> array {};

    bitVector::fill(array.data()+1, 0, 128, 1);
}

