/*
 * Copyright © 2016-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <utility>

#include "HOCRDocument.h"
#include "PDFAWriter.h"
#include "compression.h"
#include "imageOperations.h"




auto PDFAWriter::generateStreamObject(const QByteArray &input) -> QByteArray
{
    // Get a compressed version of the input
    auto compressed = compression::zopfliCompress(input);

    // Ignore compressed data if gain is less than 50 bytes
    if (input.size() < compressed.size()+50) {
        QByteArray streamObject("<</Length %length>>\nstream\n%content\nendstream\n");
        streamObject.replace("%length", (QString::number(input.size())).toUtf8());
        streamObject.replace("%content", input);
        return streamObject;
    }

    QByteArray streamObject("<</Length %length/Filter/FlateDecode>>\nstream\n%content\nendstream\n");
    streamObject.replace("%length", (QString::number(compressed.size())).toUtf8());
    streamObject.replace("%content", compressed);

    return streamObject;
}


auto PDFAWriter::createImageObject_bw_G4(const QImage &image) -> QByteArray
{
    auto fileContent = compression::faxG4Encode(image);

    // Create the image object and add it to the PDF
    QByteArray imageStreamObject("<</BitsPerComponent 1/ColorSpace/DeviceGray/DecodeParms<</K -1/Columns %cols>>/Filter/CCITTFaxDecode/Height %height/Length %length/Subtype/Image/Width %width>>stream\n%content\nendstream\n");
    imageStreamObject.replace("%cols", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%width", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%height", QString::number(image.height()).toUtf8());
    imageStreamObject.replace("%length", QString::number(fileContent.size()).toUtf8());
    imageStreamObject.replace("%content", fileContent);

    return imageStreamObject;
}


auto PDFAWriter::createImageObject_bitonal_G4(const QImage &image) -> QByteArray
{
    // Create a ByteArray containing the FAX G4 encoded image data
    auto fileContent = compression::faxG4Encode(image);

    // Create a ByteArray containing the color table
    QVector<QRgb> colorTable = image.colorTable();
    QByteArray colorTableByteArray(3*colorTable.size(), 0);
    for(int i=0;i<colorTable.size();i++) {
        QRgb color = colorTable[colorTable.size()-1-i];
        colorTableByteArray[3*i+0] = qRed(color);
        colorTableByteArray[3*i+1] = qGreen(color);
        colorTableByteArray[3*i+2] = qBlue(color);
    }

    // Create the image object and add it to the PDF
    QByteArray imageStreamObject("<</BitsPerComponent 1/ColorSpace[/Indexed/DeviceRGB %numColors(%colorTable)]/DecodeParms<</K -1/Columns %cols>>/Filter/CCITTFaxDecode/Height %height/Length %length/Subtype/Image/Width %width>>stream\n%content\nendstream\n");
    imageStreamObject.replace("%cols", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%width", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%height", QString::number(image.height()).toUtf8());
    imageStreamObject.replace("%numColors", QString::number(qMax(0,colorTable.size()-1)).toUtf8());
    imageStreamObject.replace("%colorTable", colorTableByteArray);
    imageStreamObject.replace("%length", QString::number(fileContent.size()).toUtf8());
    imageStreamObject.replace("%content", fileContent);

    return imageStreamObject;
}


auto PDFAWriter::createImageObject_gray_zlib(const QImage &_image, bool bestCompression) -> QByteArray
{
    // Convert image to Format_Grayscale8, because this is easier to handle
    auto image = _image.convertToFormat(QImage::Format_Grayscale8);
    QByteArray data = imageOperations::QImageToData(image, imageOperations::PNG);

    // Create a ByteArray containing the image data
    QByteArray fileContent;
    if (bestCompression) {
        fileContent = compression::zopfliCompress(data);
    } else {
        fileContent = compression::zlibCompress(data, compression::filtered);
    }

    // Create the image object and add it to the PDF
    QByteArray imageStreamObject("<</BitsPerComponent 8/ColorSpace/DeviceGray/DecodeParms<</Columns %columns/Colors 1/Predictor 10>>/Filter/FlateDecode/Height %height/Length %length/Subtype/Image/Width %width>>stream\n%content\nendstream\n");
    imageStreamObject.replace("%columns", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%width", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%height", QString::number(image.height()).toUtf8());
    imageStreamObject.replace("%length", QString::number(fileContent.size()).toUtf8());
    imageStreamObject.replace("%content", fileContent);

    return imageStreamObject;
}


auto PDFAWriter::createImageObject_indexed_zlib(const QImage &image, bool bestCompression) -> QByteArray
{
    QByteArray data = imageOperations::QImageToData(image);

    // Create a ByteArray containing the image data
    QByteArray fileContent;
    if (bestCompression) {
        fileContent = compression::zopfliCompress(data);
    } else {
        fileContent = compression::zlibCompress(data, compression::filtered);
    }

    // Create a ByteArray containing the color table
    QVector<QRgb> colorTable = image.colorTable();
    QByteArray colorTableByteArray(3*colorTable.size(), 0);
    for(int i=0;i<colorTable.size();i++) {
        QRgb color = colorTable[i];
        colorTableByteArray[3*i+0] = qRed(color);
        colorTableByteArray[3*i+1] = qGreen(color);
        colorTableByteArray[3*i+2] = qBlue(color);
    }

    // Create the image object and add it to the PDF
    QByteArray imageStreamObject("<</BitsPerComponent 8/ColorSpace[/Indexed/DeviceRGB %numColors(%colorTable)]/Filter/FlateDecode/Height %height/Length %length/Subtype/Image/Width %width>>stream\n%content\nendstream\n");
    imageStreamObject.replace("%width", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%height", QString::number(image.height()).toUtf8());
    imageStreamObject.replace("%numColors", QString::number(qMax(0,colorTable.size()-1)).toUtf8());
    imageStreamObject.replace("%colorTable", colorTableByteArray);
    imageStreamObject.replace("%length", QString::number(fileContent.size()).toUtf8());
    imageStreamObject.replace("%content", fileContent);

    return imageStreamObject;
}


auto PDFAWriter::createImageObject_rgb_zlib(const QImage &_image, bool bestCompression) -> QByteArray
{
    auto image = _image.convertToFormat(QImage::Format_RGB888);

    // Create a ByteArray containing the image data
    QByteArray filteredImageData = imageOperations::QImageToData(image, imageOperations::PNG);
    QByteArray compressed;
    if (bestCompression) {
        compressed = compression::zopfliCompress(filteredImageData);
    } else {
        compressed = compression::zlibCompress(filteredImageData, compression::filtered);
    }

    // Create the image object and add it to the PDF
    QByteArray imageStreamObject("<</BitsPerComponent 8/ColorSpace/DeviceRGB/DecodeParms<</Columns %columns/Colors 3/Predictor 10>>/Filter/FlateDecode/Height %height/Length %length/Subtype/Image/Width %width>>stream\n%content\nendstream\n");
    imageStreamObject.replace("%columns", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%width", QString::number(image.width()).toUtf8());
    imageStreamObject.replace("%height", QString::number(image.height()).toUtf8());
    imageStreamObject.replace("%length", QString::number(compressed.size()).toUtf8());
    imageStreamObject.replace("%content", compressed);

    return imageStreamObject;
}


auto PDFAWriter::completePageContentObject_b(QByteArray contentStream, const imageInfo& bInfo, length deltaX, length deltaY, const QImage& imageForOCR, const QStringList& OCRLanguages) -> QByteArray
{
    HOCRDocument doc;
    doc.read(imageForOCR, OCRLanguages);

    HOCRTextBox textBox;
    if (!doc.hasError()) {
        textBox = doc.takeFirstPage();
    }

    return completePageContentObject_a(std::move(contentStream), bInfo, deltaX, deltaY, textBox);
}


auto PDFAWriter::completePageContentObject_a(QByteArray contentStream, const imageInfo& bInfo, length deltaX, length deltaY, const HOCRTextBox& textBox) -> QByteArray
{
    if (textBox.hasText()) {
        contentStream.append(" BT 3 Tr "+textBox.toRawPDFContentStream(QFont("Times"), bInfo.xResolution(), bInfo.yResolution(), deltaX, deltaY)+"ET");
    }

    return generateStreamObject(contentStream);
}
