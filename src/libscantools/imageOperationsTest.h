/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QDebug>
#include <QtGlobal>
#include <QTest>

class imageOperationsTest: public QObject
{
  Q_OBJECT

  QImage Hydrogenea;
  QImage Hydrogenea88;
  QImage HydrogeneaGray;
  QImage HydrogeneaBW;
  QImage Monocolor;

  
  // Test data
  QVector<QImage> images;
  
  private slots:
    void initTestCase();
    void cleanupTestCase();
    
    void colors_data();
    void colors();
    void deAlpha_data();
    void deAlpha();
    void isBlackAndWhite_data();
    void isBlackAndWhite();
    void isOpaque_data();
    void isOpaque();
    void optimizedFormat_data();
    void optimizedFormat();
    void simplifyTransparentPixels();
    void ToAndFromData_data();
    void ToAndFromData();
};
