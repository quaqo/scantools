/*
 * Copyright © 2016 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef JBIG2SEGMENT
#define JBIG2SEGMENT 1

#include <QIODevice>
#include <QVector>


/* Reads and stores a JBIG2 segment, and interprets the segment header.
    
    This class can be used to read a JBIG2 segment.  It is used internally by
    the JBIG2Document class; users will hardly ever access this class directly.
    The class has minimal support for interpretation of the segment header, and
    for manipulating a few flags there.  In order to support JBIG2 with random
    access organisation, instances of this class are constructed in a three-step
    procedure.

    - First, an instance is constructed using the default JBIG2Segment().
    
    - Second, the segment header is read from a QIODevice (usually a QFile),
    using the method readHeader(). If an error occurs at this stage, the
    JBIG2Segment should not be used anymore.
    
    - Third, the segment data is read from a QIODevice, using the method
    readData(). Again, if an error occurs at this stage, the JBIG2Segment
    should not be used anymore.
    
    The instance can then be converted to a QByteArray which contains the full
    segment, or it can be manipulated first.

    The methods of this class are reentrant, but not thread-safe.
*/

class JBIG2Segment
{
 public:
  /** Constructs an empty JBIG2 segment */
  JBIG2Segment();

  /** Clears all data */
  void clear();
  
  /** Reads JBIG2 segment header 
      
      This method reads a JBIG2 segment header. This method can only be called
      once. In case that it method returns with an error, the JBIG2 segment
      should be discarded.
      
      @warning There are currently no checks that the data read is meaningful.
      
      @param device Pointer to a QIODevice from which the header should be
      read.
      
      @returns A brief error message in English language ("file read error") in
      case of error, and QString::null in case of success.
   */
  QString readHeader(QIODevice *device);

  
  /** Checks if header data has been read successfully */
  inline bool hasHeader() const
  {
    return !_header.isEmpty();
  };

  
  /** Returns the JBIG2 segment header 

      This method can only be called after the segment header has been read
      successfully.
  */
  QByteArray header() const;

  
  /** Read JBIG2 segment data
      
      Once a head has successfully been read, use this method to read the data
      of the JBIG2 segment.  This method can only be called once. In case that
      it returns with an error, the JBIG2 segment should be discarded.
      
      @warning There are currently no checks that the data read is meaningful.

      @param device Pointer to a QIODevice from which the header should be read.
   
      @returns A brief error message in English language ("file read error") in
      case of error, and QString::null in case of success.
  */
  QString readData(QIODevice *device);

  
  /** Checks if segment data has been read successfully
      
      @return True if both the segment header and the segment data have been
      read successfully, false otherwise.
   */
  inline bool hasData() const
  {
    return !_data.isNull();
  };

  
  /** Returns the JBIG2 segment data

      This method can only be called after the segment data has been read
      successfully.
  */
  QByteArray data() const;

  
  /** Convert segment to QByteArray

      This method converts the segment to a QByteArray which contains the
      segment header data, and then the segment data.  This method can only be
      called after the segment data has been read successfully.
      
      @returns A QByteArray which contains the segment header data, and then
      the segment data, or a null QByteArray in case of error.
  */
  inline operator QByteArray() const {return header()+data();};

  
  /** Human-readable info string about the segment

      This method is mainly useful for debugging purposes.

      @returns A QString with a few lines of text describing the segment. Lines
      are separated by newline characters, '\n'.
   */
  QString info() const;

  
  /** Segment type

      This method can only be called after the segment header has been read
      successfully.

      @returns The type of the segment, as specified in Section 7.3 of the
      JBIG2 standard.
   */
  quint8 type() const;

  
  /** Retain bit for this segment

      This method can only be called after the segment header has been read
      successfully.

      @returns The retain bit for this segment, as specified in Section 7.2.4 of
      the JBIG2 standard.
   */
  bool retainbitForThisSegment() const;

  
  /** Sets the retain bit for this segment

      This method sets the retain bit for this segment. The meaning of the bit
      is specified in Section 7.2.4 of the JBIG2 standard.  This method can only
      be called after the segment header has been read successfully.
      
      @param bit The new value of the retain bit
  */
  void setRetainbitForThisSegment(bool bit=true);
  
  
  /** Page association of this segment
      
      This method can only be called after the segment header has been read
      successfully.
      
      @returns The page association of this segment, as specified in Section
      7.2.6 of the JBIG2 standard.
  */
  quint32 pageAssociation() const;
  
  
  /** If non-zero, changes the page association of this segment to 'one'
      
      This method checks the page association of the segment, which is specified
      in Section 7.2.6 of the JBIG2 standard.  If the page association is 0,
      nothing will be changed.  Otherwise, the page association will be set to
      1.  This method can only be called after the segment header has been read
      successfully.
  */
  void setPageAssociation();

  
  /** Segment number 
      
      This method can only be called after the segment header has been read
      successfully.
   */
  quint32 number() const;

  
  /** List of segments that this segment refers to
      
      This method can only be called after the segment header has been read
      successfully.

      @returns A vector containing the numbers of those segments that this
      segment refers to. The vector is not sorted in any way.
   */
  QVector<quint32> refersTo() const;

 private:
  // QByteArray containing the segment header
  QByteArray _header;

  // QByteArray containing the segment data
  QByteArray _data;

  // Once the header has been read successfully, this member contains the
  // position of the 'page association' field in the segment header data. This
  // information is used internally to modify the header data.
  qint64 pageAssociationPos{};

  // Once the header has been read successfully, this member contains the
  // position of the byte in the segment header data whose least significant bit
  // is the retain bit for this segment. This information is used internally to
  // modify the header data.
  qint64 retainbitForThisSegmentPos{};

  
  /* Warning!  The following members duplicate data that is also present in the
     member _header, which represents the segment header data. When modifying
     these members, the segment header data must be modified accordingly,
     otherwise data corruption will like follow.
   */
  
  // Length of segment data, as read from the header data
  quint32 _dataLength{};

  // Header flags,
  quint8 _headerFlags{};

  // Number of this segment
  quint32 _number{};

  // Page association
  quint32 _pageAssociation{};

  // Numbers of segments that this segment refers to, in order of appearance in
  // the segment header data.
  QVector<quint32> _referredToSegmentNumbers;
  
  // Convenience method, tells if the page association field in the header data
  // is one byte or four bytes long
  inline bool segmentPageAssociationIs8Bit() const
  {
    return ((_headerFlags & (1 << 6)) == 0);
  };
  
};

#endif
