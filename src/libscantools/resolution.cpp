/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "resolution.h"

struct unitEntry {const char *txt; resolution::unit u; qreal magnitude;};

static std::vector<unitEntry> unitTable = {
  {"dpcm", resolution::dpcm,   254.0},
  {"dpi",  resolution::dpi,    100.0},
  {"dpm",  resolution::dpm,      2.54},
  {nullptr,      resolution::dpi,      0.0}
};


qreal resolution::get(unit u) const
{
  for(int i=0; unitTable[i].txt != nullptr; i++) 
    if (unitTable[i].u == u)
      return _resolution/unitTable[i].magnitude;
  return 0.0;    
}


void resolution::set(qreal l, unit u)
{
  for(int i=0; unitTable[i].txt != nullptr; i++) 
    if (unitTable[i].u == u)
      _resolution = l*unitTable[i].magnitude;
}
