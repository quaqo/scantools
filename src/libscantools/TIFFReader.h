/*
 * Copyright © 2017-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef TIFFREADER
#define TIFFREADER 1

#include <QImage>
#include <QMutex>
#include <QTemporaryFile>


/** Reads TIFF files and converts them to QImages
    
This class reads TIFF files into QImages.  The class exist because the
QImageReader that is shipped with Qt does not handle multi-page TIFF files.

\code 

// Minimal example
// Read the 5th page from a TIFF document

TIFFReader doc("filename.tiff");
QImage fifthPage = doc[4]; // Page numbering starts at zero

\endcode

The methods of this class are reentrant and thread safe.  The implementation
ensures that only one thread can access the document at any given time.  This
class does not use libtiff directly, which is extremely hard to use in a
thread-safe manner.  It does use, however, Qt's TIFF reader.
*/

class TIFFReader
{
 public:
  /** Constructs a TIFF document from a file

      Once the TIFFReader is constructed, the error status should be checked
      using the method hasError().
      
      @param fileName Name of the TIFF file

      @warning This method makes a private copy of the TIFF file as a temporary
      file. This might be a concern if the TIFF file is extremely large
      (e.g. several GB).
  */
  explicit TIFFReader(const QString& fileName);

  /** Standard destructor */
  ~TIFFReader();
  
  /** Error status

      @return True iff an error has occurred
   */
  bool hasError();
  
  /** Error message 
      
      In case that hasError() returns 'true', this method returns a
      human-readable description of the error.

      @returns Human-readable description of the error
  */
  QString error();
  
  /** Number of images in the TIFF document

      @returns Number of images
      
      @warning This method must not be used if an error conditions is set.
  */
  quint32 size();
  
  /** Read image 
      
      This method reads the n.th image of the TIFF file and returns it as a
      QImage. In case of error, an empty image is returned and an error
      condition is set. 

      @param pageNumber Page number

      @returns Image, or an empty image in case of error

      @warning This method must not be used if an error conditions is set.
  */
  QImage operator[](quint16 pageNumber);
  
 private:
  // Prevents simultaneous write access to the tmpFile and the error message by
  // multiple threads
  QMutex mutex;

  // This is a temporary file where a copy of the TIFF is kept. The copy is
  // modified by operator[]
  QTemporaryFile tmpFile;

  // This data stream is used to read from tmpFile. Its endian-ness is set in
  // the constructor to match the endian-ness of the TIFF
  QDataStream ioStream;

  // List of offsets that specify where in the TIFF file the image directories
  // start.
  QList<quint32> IFDOffsets;

  // Error message
  QString _error;
};

#endif
