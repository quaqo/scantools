/*
 * Copyright © 2016 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "JBIG2Segment.h"

#include <QDataStream>
#include <QDebug>


JBIG2Segment::JBIG2Segment()
{
    clear();
}


auto JBIG2Segment::readHeader(QIODevice *device) -> QString
{
    // Clear all
    clear();

    // Paranoid safety checks
    if (device == nullptr) {
        return "Device is zero";
    }
    if (device->isSequential()) {
        return "Cannot read from sequential device";
    }
    if (hasHeader()) {
        return "JBIG2 segment header has already been read, cannot be read again";
    }

    // Read segment header.  The header size is not known a priori, which forces
    // us to simultaneosly read and interpret the header, and to copy it to the
    // _header QByteArray.
    QDataStream inStream(device);
    QDataStream outStream(&_header, QIODevice::WriteOnly);

    // Read segment number
    inStream >> _number;
    outStream << _number;

    // Read segment header flags
    inStream >> _headerFlags;
    outStream << _headerFlags;

    // Read "referred to segment count and retention flags". This is not as easy
    // as it sounds, because the size of this field is variable, and depends on
    // the first byte.
    quint8 byteBuffer = 0;
    inStream >> byteBuffer;
    outStream << byteBuffer;
    quint32 countOfReferredToSegments = 0;
    if ((byteBuffer >> 5) <= 4) {
        // Short form
        countOfReferredToSegments = (byteBuffer >> 5);
        retainbitForThisSegmentPos = _header.size()-1;
    } else {
        // Long form
        countOfReferredToSegments = static_cast<quint32>(byteBuffer)<<24;
        inStream >> byteBuffer;
        outStream << byteBuffer;
        countOfReferredToSegments += static_cast<quint32>(byteBuffer)<<16;
        inStream >> byteBuffer;
        outStream << byteBuffer;
        countOfReferredToSegments += static_cast<quint32>(byteBuffer)<<8;
        inStream >> byteBuffer;
        outStream << byteBuffer;
        countOfReferredToSegments += static_cast<quint32>(byteBuffer);
        countOfReferredToSegments &= 0x1FFFFFFF;

        retainbitForThisSegmentPos = _header.size();

        quint32 sizeForCountOfReferredToSegments = (countOfReferredToSegments+8)/8;
        for(quint32 i=1; i<sizeForCountOfReferredToSegments; i++) {
            inStream >> byteBuffer;
            outStream << byteBuffer;
        }
    }
    _referredToSegmentNumbers.resize(countOfReferredToSegments);

    // Read "referred to segment numbers". Alas, the interpretation of this field
    // depends on how big the segment number is…
    if (_number <= 256) {
        quint8 b = 0;
        for(quint32 i=0; i<countOfReferredToSegments; i++) {
            inStream >> b;
            outStream << b;
            _referredToSegmentNumbers[i] = b;
        }
    }
    if ((256 < _number) && (_number <= 65536)) {
        quint16 b = 0;
        for(quint32 i=0; i<countOfReferredToSegments; i++) {
            inStream >> b;
            outStream << b;
            _referredToSegmentNumbers[i] = b;
        }
    }
    if (65536 < _number) {
        quint32 b = 0;
        for(quint32 i=0; i<countOfReferredToSegments; i++) {
            inStream >> b;
            outStream << b;
            _referredToSegmentNumbers[i] = b;
        }
    }

    // Now read the "segment page association". This field tells us what page this
    // segment belongs to. Alas, the interpretation of this field depends on a bit
    // in the _headerFlags…
    pageAssociationPos = _header.size();
    if (segmentPageAssociationIs8Bit()) {
        quint8 b = 0;
        inStream >> b;
        outStream << b;
        _pageAssociation = b;
    } else {
        inStream >> _pageAssociation;
        outStream << _pageAssociation;
    }

    // Lastly, read the length of the data associated with this segment
    inStream >> _dataLength;
    outStream << _dataLength;

    // Error check. If an error occurred, reset all data that has been read
    // meanwhile and return a message.
    if (inStream.status() != 0) {
        clear(); // Clear all
        return device->errorString();
    }

    return QString();
}


auto JBIG2Segment::header() const -> QByteArray
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::header() called, but segement header has not yet been read.";
    }

    return _header;
}


auto JBIG2Segment::readData(QIODevice *device) -> QString
{
    // Paranoid safety checks
    if (device == nullptr) {
        return  "Device is zero";
    }
    if (!hasHeader()) {
        return "JBIG2 segment header has not yeat been read, cannot read data";
    }
    if (!_data.isNull()) {
        return "JBIG2 segment data has already been read, cannot read again";
    }

    if (_dataLength > 0) {
        _data = device->read(_dataLength);
    } else {
        _data = QByteArray("");
    }

    // Error check. If an error occurred, clear segment data and return a message.
    if (_data.size() != static_cast<int>(_dataLength)) {
        _data.clear();
        return device->errorString();
    }

    // No error, everything went fine.
    return QString();
}


auto JBIG2Segment::data() const -> QByteArray
{
    // Paranoid safety checks
    if (!hasData()) {
        qWarning() << "Internal error. JBIG2Segment::data() called, but segement data has not yet been read.";
    }

    return _data;
}


auto JBIG2Segment::info() const -> QString
{
    // Paranoid safety checks
    if (!hasHeader()) {
        return "Segment header has not yet been read";
    }

    QString referredToSegmentNumberString;
    referredToSegmentNumberString = "[";
    for(int i=0; i<_referredToSegmentNumbers.size(); i++) {
        if (i > 0) {
            referredToSegmentNumberString += ", ";
        }
        referredToSegmentNumberString += QString::number(_referredToSegmentNumbers[i]);
    }
    referredToSegmentNumberString += "]";

    QString result;
    result += QString("Segment Number        : %1\n").arg(_number);
    result += QString("Segment Type          : %1\n").arg(type());
    result += QString("Referred to segements : %1\n").arg(referredToSegmentNumberString);
    if (retainbitForThisSegment()) {
        result += QString("Retain bit for this   : true -- other segments may refer to this segment\n");
    } else {
        result += QString("Retain bit for this   : false -- no other segment may refer to this segment\n");
    }
    result += QString("Page association      : %1\n").arg(pageAssociation());
    result += QString("Data length           : %1\n").arg(_dataLength);

    if (hasData()) {
        result += "Segement data has been loaded successfully.\n";
    } else {
        result += "Segement data has not yet been read.\n";
    }

    return result;
}


auto JBIG2Segment::type() const -> quint8
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::type() called, but segement header has not yet been read.";
        return 0;
    }

    return (_headerFlags & 0x3F);
}


auto JBIG2Segment::retainbitForThisSegment() const -> bool
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::retainbitForThisSegment() called, but segement header has not yet been read.";
        return false;
    }

    return ((_header[static_cast<uint>(retainbitForThisSegmentPos)] & 1) == 1);
}


void JBIG2Segment::setRetainbitForThisSegment(bool bit)
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::setRetainbitForThisSegment(bool) called, but segement header has not yet been read.";
        return;
    }

    if (bit) {
        _header[static_cast<uint>(retainbitForThisSegmentPos)] = _header[static_cast<uint>(retainbitForThisSegmentPos)] | 1;
    } else {
        _header[static_cast<uint>(retainbitForThisSegmentPos)] = _header[static_cast<uint>(retainbitForThisSegmentPos)] & 0xFE;
    }
}


auto JBIG2Segment::pageAssociation() const -> quint32
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::pageAssociation() called, but segement header has not yet been read.";
        return 0;
    }

    return _pageAssociation;
}


void JBIG2Segment::setPageAssociation()
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::setPageAssociation() called, but segement header has not yet been read.";
        return;
    }

    if (_pageAssociation == 0) {
        return;
    }

    if (!segmentPageAssociationIs8Bit()) {
        _headerFlags = _headerFlags & ~(1 << 6);
        _header[4] = _header[4] & ~(1 << 6);
        _header.replace(pageAssociationPos+1, 3, QByteArray());
    }

    _header[static_cast<uint>(pageAssociationPos)] = 1;
    _pageAssociation = 1;
}


auto JBIG2Segment::number() const -> quint32
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::number() called, but segement header has not yet been read.";
        return 0;
    }

    return _number;
}


auto JBIG2Segment::refersTo() const -> QVector<quint32>
{
    // Paranoid safety checks
    if (!hasHeader()) {
        qWarning() << "Internal error. JBIG2Segment::refersTo() called, but segement header has not yet been read.";
    }

    return _referredToSegmentNumbers;
}


void JBIG2Segment::clear()
{
    // Initialize data
    _number = 0;
    _headerFlags = 0;
    _pageAssociation = 0;
    _dataLength = 0;

    retainbitForThisSegmentPos = 0;
    pageAssociationPos = 0;

    _header.clear();
    _data.clear();
}
