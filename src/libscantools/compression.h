/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef COMPRESSION
#define COMPRESSION 1

#include <QByteArray>
#include <QImage>


/** This namespace gathers static methods for data compression and decompression. */

namespace compression
{
  /** Compression strategy

      The zlib compression library allows to tune the compression algorithm, to
      achieve better compression rates depending on the type of input data.
   */
  
  enum zlibCompressionStrategy {
    /** The default strategy is best for general data */
    standard,

    /** Strategy for data thar consists mostly of small values with a somewhat
	random distribution
	
	This compression algorithm works well for images whose data has been
	filtered.
    */
    filtered
  };

  /** Decompresses a bitonal image following the CCITT Group 4 compression
      standard
      
      This method reconstructs a bitonal image from the CCITT Group 4 compressed
      data.
      
      @param data Data that is to be decoded.

      @param width Width of the encoded image. Note that image size cannot be
      deducted from FAX data.

      @param heightEstimate Estimate for the height of the encoded image. The
      height can be deducted from the FAX data, so that this data is optional.
      Decompression speed is, however, slightly better if the correct height is
      known in advance.

      @returns An image of format QImage::Format_Mono without color table. In
      case of error, an empty image is returned.
  */
  QImage faxG4Decode(const QByteArray &data, int width, int heightEstimate=0);

  /** Compresses a bitonal image following the CCITT Group 4 compression standard

      This method compresses a bitonal image following the CCITT Group 4
      compression standard (also known as "Fax G4" compression, defined in ITU-T
      T.6).  This compression method is lossless, typically achieves a 20:1
      compression ratio and is used widely in FAX machines, as well as in TIFF
      and PDF files.

      @note Note that the image colors and size is *not* stored. To restore the
      image later, this information needs to be stored separately.
      
      @note The compression algorithm works best if color 0 is the background
      color of the image -- the algorithm expect long runs of color 0 pixels and
      comparatively short runs of pixels of color 1.

      @note The implementation has been tested for correctness, but has not
      (yet) been optimized for speed.  Unlike all other free implementations of
      FAX compression that I have seen (i.e. libtiff and derivatives), this
      implementation is not base on ancient code of Sam Leffler and Silicon
      Graphics, Inc. dating back to the early 1990's.  I can therefore guarantee
      with confidence that the method works well in a threaded environment.

      @see <a href="https://en.wikipedia.org/wiki/Group_4_compression">Wikipedia page on Group 4 compression</a> 

      @param image The image that is to be encoded. The method works best if the
      image is in format QImage::Format_Mono and if color 0 is the black and
      color 1 is white. If these conditions are not met, a local copy of the
      image is made and transformed to match these conditions; this will use
      extra processing time.

      @returns A QByteArray containing the encoded data, or an empty QByteArray
      in case of error.
   */
  QByteArray faxG4Encode(const QImage &image);

  /** Compresses a QByteArray using zlib

      Compresses the contents of a QByteArray using zlib.  In contrast to Qt's
      method "qCompress", the output of this method is a valid zLib data stream.

      @param src QByteArray holding uncompressed data

      @param strategy Strategy used by zlib in the compression

      @returns A QByteArray containing the compressed data, or an empty
      QByteArray in case of error.
  */
  QByteArray zlibCompress(QByteArray &src, zlibCompressionStrategy strategy=standard);

  /** Uncompresses a QByteArray using zlib

      Compresses the contents of a QByteArray using zlib.
      
      @warning The zlib data stream does not contain any information concerning
      the size of the uncompressed data. If the size of the uncompressed data is
      known, this information should be passed on using the parameter
      'estimatedOutputSize'. If no estimated output size the method reserves a
      buffer that is four times the input size and doubles the buffer size
      whenever uncompression fails with 'insufficient buffer size'. This
      trial-and-error approach is, however, far from optimal and can lead to
      substantial slowdown.

      @param src QByteArray holding compressed data

      @param estimatedOutputSize Expected size of the uncompressed data, in
      bytes.

      @returns A QByteArray containing the uncompressed data, or an empty
      QByteArray in case of error.
  */
  QByteArray zlibUncompress(const QByteArray &src, quint64 estimatedOutputSize=0);

  /** Compresses a QByteArray using zopfli

      Compresses the contents of a QByteArray using Google's 'zopfli'
      implementation, which produces better results than zlib, but is slower by
      a factor of almost 100x.  The output of this method is a valid zLib data
      stream.

      @param src QByteArray holding uncompressed data

      @returns A QByteArray containing the compressed data, or an empty
      QByteArray in case of error.
  */
  QByteArray zopfliCompress(const QByteArray &src);
};

#endif
