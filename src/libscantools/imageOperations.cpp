/*
 * Copyright © 2017-2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstring>
#include <QDebug>
#include <QPainter>
#include "imageOperations.h"


QImage imageOperations::deAlpha(const QImage &image, QRgb background)
{
    // If image has no alpha channel, things are very simple
    if (!image.hasAlphaChannel())
        return image;
    
    // If the image has a color table, things are easy. Just adjust all colors.
    if (image.colorCount() > 0) {
        quint16 bgRed   = qRed(background);
        quint16 bgGreen = qGreen(background);
        quint16 bgBlue  = qBlue(background);

        QImage myCopy(image);

        for(int i=0; i<myCopy.colorCount(); i++) {
            QRgb color = myCopy.color(i);

            quint16 alpha   = qAlpha(color);
            quint8 newRed   = (qRed(color)*alpha+bgRed*(255-alpha))/255;
            quint8 newGreen = (qGreen(color)*alpha+bgGreen*(255-alpha))/255;
            quint8 newBlue  = (qBlue(color)*alpha+bgBlue*(255-alpha))/255;

            myCopy.setColor(i, qRgb(newRed,newGreen,newBlue));
        }
        return myCopy;
    }

    // If there is no color table render
    QImage result(image.size(), QImage::QImage::Format_RGB32);
    // Copy resolution and text keys
    result.setDotsPerMeterX(image.dotsPerMeterX());
    result.setDotsPerMeterY(image.dotsPerMeterY());
    foreach(const QString &key, image.textKeys())
        result.setText(key, image.text(key));

    // Fill with color
    result.fill(background | 0xFF000000);
    QPainter paint(&result);
    paint.drawImage(0,0, image);
    return result;
}


QVector<QRgb> imageOperations::colors(const QImage &image)
{
    QSet<QRgb> colorSet;
    for(int y=0; y<image.height(); y++)
        for(int x=0; x<image.width(); x++) {
            colorSet << image.pixel(x,y);
            if (colorSet.size() > 256)
                return QVector<QRgb>();
            if (colorSet.size() == image.colorCount())
                return image.colorTable();
        }

    QVector<QRgb> colorVector(colorSet.size());
    int i=0;
    foreach(const QRgb &color, colorSet) {
        colorVector[i] = color;
        i++;
    }
    
    return colorVector;
}


bool imageOperations::isBlackAndWhite(const QImage &image)
{
    // If the image has a color table AND if all entries in that table are eiter
    // black or white, then we can be sure that the image is black and white
    QVector<QRgb> colorTable = image.colorTable();
    if (!colorTable.isEmpty()) {
        bool allBW = true;
        for(unsigned int i : colorTable) {
            QRgb color = i & 0x00FFFFFF;
            if ((color != 0x00000000) && (color != 0x00FFFFFF)) {
                allBW = false;
                break;
            }
        }
        if (allBW)
            return true;
    }

    // If there is no color table, or if there are non-BW entries in the color
    // table, we need to check all pixels, regretfully.
    for(int y=0; y<image.height(); y++)
        for(int x=0; x<image.width(); x++) {
            QRgb color = image.pixel(x,y) & 0x00FFFFFF;
            if ((color != 0x00000000) && (color != 0x00FFFFFF))
                return false;
        }

    return true;
}


QImage imageOperations::optimizedFormat(const QImage &inputImage)
{
    if (inputImage.isNull() || (inputImage.format() == QImage::Format_Invalid))
        return QImage(0,0,QImage::Format_Mono);

    // Simplify transparency, get set of colors
    QImage simplifiedImage = simplifyTransparentPixels(inputImage);
    QVector<QRgb> colorVector = colors(simplifiedImage);

    //
    // Monocolor image
    //
    if (colorVector.size() == 1) {
        QImage result = QImage(simplifiedImage.size(), QImage::Format_Mono);

        // Copy resolution and text keys.

        // Note: the following lines are necessary because metadata is not
        // consistently copied by Qt
        result.setDotsPerMeterX(simplifiedImage.dotsPerMeterX());
        result.setDotsPerMeterY(simplifiedImage.dotsPerMeterY());
        foreach(const QString &key, simplifiedImage.textKeys())
            result.setText(key, simplifiedImage.text(key));

        // Fill with color
        result.fill(0);
        result.setColorCount(1);
        result.setColor(0, colorVector[0]);

        return result;
    }

    //
    // Bitonal image
    //
    if (colorVector.size() == 2) {
        /* Ideally, I was trying to use code like the one below, but this seems to
       trigger QTBUG-63163, see https://bugreports.qt.io/browse/QTBUG-63163
       The current code is a workaround
    QRgb color0 = colorVector[0];
    QRgb color1 = colorVector[1];
    if ( qGray(color0) > qGray(color1) ) {
      colorVector[0] = color1;
      colorVector[1] = color0;
    }
    simplifiedImage = simplifiedImage.convertToFormat(QImage::Format_Mono, colorVector);
    */

        simplifiedImage = simplifiedImage.convertToFormat(QImage::Format_Mono);
        if ( qGray(simplifiedImage.color(0)) < qGray(simplifiedImage.color(1)) ) {
            QRgb color0 = simplifiedImage.color(1);
            QRgb color1 = simplifiedImage.color(0);
            simplifiedImage.setColor(0,color0);
            simplifiedImage.setColor(1,color1);
            simplifiedImage.invertPixels();
        }

        // Copy resolution and text keys
        simplifiedImage.setDotsPerMeterX(inputImage.dotsPerMeterX());
        simplifiedImage.setDotsPerMeterY(inputImage.dotsPerMeterY());
        foreach(const QString &key, inputImage.textKeys())
            simplifiedImage.setText(key, inputImage.text(key));

        return simplifiedImage;
    }

    //
    // Indexed color image or perhaps gray image
    //
    if (!colorVector.empty()) {
        // If there is a color that's not gray, convert to indexed color
        foreach(const QRgb &color, colorVector)
            if ((qAlpha(color) != 0xFF) || (qRed(color) != qGreen(color)) || (qRed(color) != qBlue(color))) {
                simplifiedImage = simplifiedImage.convertToFormat(QImage::Format_Indexed8, colorVector);
                // Copy resolution and text keys
                simplifiedImage.setDotsPerMeterX(inputImage.dotsPerMeterX());
                simplifiedImage.setDotsPerMeterY(inputImage.dotsPerMeterY());
                foreach(const QString &key, inputImage.textKeys())
                    simplifiedImage.setText(key, inputImage.text(key));

                return simplifiedImage;
            }

        // Otherwise, convert to Gray
        simplifiedImage = simplifiedImage.convertToFormat(QImage::Format_Grayscale8);
        // Copy resolution and text keys
        simplifiedImage.setDotsPerMeterX(inputImage.dotsPerMeterX());
        simplifiedImage.setDotsPerMeterY(inputImage.dotsPerMeterY());
        foreach(const QString &key, inputImage.textKeys())
            simplifiedImage.setText(key, inputImage.text(key));

        return simplifiedImage;
    }

    //
    // Full-color image
    //
    if (isOpaque(simplifiedImage))
        return simplifiedImage.convertToFormat(QImage::Format_RGB888);
    return simplifiedImage.convertToFormat(QImage::Format_ARGB32);
}


bool imageOperations::isOpaque(const QImage &image)
{
    // If the image format has no alpha channel, we can find a very quick answer.
    if (!image.hasAlphaChannel())
        return true;

    // If the image contains a color table, we might be better off with a quick
    // check of the table rather than looking at all pixels individually. However,
    // this can only give a positive answer, since transparent colors in the color
    // table might simply not be used in the actual picture.
    if (image.colorCount() > 0) {
        bool isAllOpaque = true;
        for(int i=0; i<image.colorCount(); i++)
            if (qAlpha(image.color(i)) != 0xFF) {
                isAllOpaque = false;
                break;
            }
        if (isAllOpaque)
            return true;
    }

    // Now we are left checking all pixels individually. This is slow, sigh.
    for(int y=0; y<image.height(); y++)
        for(int x=0; x<image.width(); x++)
            if (qAlpha(image.pixel(x,y)) != 0xFF)
                return false;
    return true;
}


QImage imageOperations::simplifyTransparentPixels(const QImage &image)
{
    // If the image format has no alpha channel, nothing needs to be done
    if (!image.hasAlphaChannel())
        return image;

    // If the image has an alpha channel, we need to work on a copy of the image
    QImage myImage(image);

    // If the image contains a color table, it suffices to treat all colors in the
    // table
    if (myImage.colorCount() > 0) {
        for(int i=0; i<myImage.colorCount(); i++)
            if (qAlpha(myImage.color(i)) == 0x00)
                myImage.setColor(i, 0x00FFFFFF);
        return myImage;
    }

    // Otherwise, we have to work through all pixels individually
    for(int y=0; y<myImage.height(); y++)
        for(int x=0; x<myImage.width(); x++)
            if (qAlpha(myImage.pixel(x,y)) == 0x00)
                myImage.setPixel(x,y, 0x00FFFFFF);
    return myImage;
}


inline quint8 PaethPredictor(quint8 a, quint8 b, quint8 c)
{
    // a = left, b = above, c = upper left
    qint16 p = (qint16)a + (qint16)b - (qint16)c;        // initial estimate
    qint16 pa = abs(p - a);   //distances to a, b, c
    qint16 pb = abs(p - b);
    qint16 pc = abs(p - c);
    // return nearest of a,b,c,
    // breaking ties in order a,b,c.
    if ((pa <= pb) && (pa <= pc))
        return a;

    if (pb <= pc)
        return b;
    
    return c;
}

inline quint8 averagePredictor(quint8 a, quint8 b, quint8 c)
{
    return ((qint16)a + (qint16)b - (qint16)c)/3;
}


QByteArray imageOperations::QImageToData(const QImage &image, imageDataFilter filter)
{
    // Paranoid safety tests
    if (image.isNull())
        return QByteArray();

    QByteArray result;

    int scanLineSize;
    quint8 bytesPerPixel;
    switch(image.format()) {
    case QImage::Format_Mono:
        scanLineSize = (image.width()+7)/8;
        bytesPerPixel = 0;
        break;
    case QImage::Format_Indexed8:
        scanLineSize = image.width();
        bytesPerPixel = 1;
        break;
    case QImage::Format_ARGB32:
        scanLineSize = 4*image.width();
        bytesPerPixel = 4;
        break;
    case QImage::Format_RGB888:
        scanLineSize = 3*image.width();
        bytesPerPixel = 3;
        break;
    case QImage::Format_Grayscale8:
        scanLineSize = image.width();
        bytesPerPixel = 1;
        break;
    default:
        return QByteArray();
    }

    // Filter 'identity'
    if (filter == identity) {
        result.resize(scanLineSize*image.height());
        for(int i=0; i<image.height(); i++)
            std::memcpy(result.data()+i*scanLineSize, image.constScanLine(i), scanLineSize);
        return result;
    }


    // The only other remaining filter is 'PNG'. Check that this filter is chosen,
    // check that the prerequisites of this filter are satisfied, and return an
    // empty array if this is not the case.
    if ((filter != PNG) || (bytesPerPixel == 0))
        return QByteArray();

    // Array size and data pointers
    result.resize((scanLineSize+1)*image.height());

    // We encode each scanline five times, and compare the outcomes. Set up a
    // vector to store five encoded scan lines.
    QVector<QByteArray> encodedScanLines(5);
    for(int i=0; i<5; i++)
        encodedScanLines[i].resize(scanLineSize);

    // Go through all scanlines, encode
    for(int y=0; y<image.height(); y++) {
        // Set up pointers
        const quint8 *prior     = (y == 0) ? nullptr : (const quint8 *)image.constScanLine(y-1);
        const auto *scanLine  = (const quint8 *)image.constScanLine(y);

        // Filter Type: None
        std::memcpy(encodedScanLines[0].data(), scanLine, scanLineSize);

        // Filter Type: Sub
        auto *dataPtr = (qint8 *)encodedScanLines[1].data();
        for(int x=0;x<bytesPerPixel;x++)
            *(dataPtr++) = scanLine[x];
        for(int x=bytesPerPixel; x<scanLineSize; x++)
            *(dataPtr++) = scanLine[x] - scanLine[x-bytesPerPixel];

        // Filter Type: Up
        dataPtr = (qint8 *)encodedScanLines[2].data();
        if (y == 0) {
            std::memcpy(dataPtr, scanLine, scanLineSize);
        } else {
            for(int x=0; x<scanLineSize; x++)
                *(dataPtr++) = scanLine[x] - prior[x];
        }

        // Filter Type: Average
        dataPtr = (qint8 *)encodedScanLines[3].data();
        if (y == 0) {
            for(int x=0;x<bytesPerPixel;x++)
                *(dataPtr++) = scanLine[x] - averagePredictor( 0, 0, 0);
            for(int x=bytesPerPixel; x<scanLineSize; x++)
                *(dataPtr++) = scanLine[x] - averagePredictor( scanLine[x-bytesPerPixel], 0, 0);
        } else {
            for(int x=0;x<bytesPerPixel;x++)
                *(dataPtr++) = scanLine[x] - averagePredictor( 0, prior[x], 0 );
            for(int x=bytesPerPixel; x<scanLineSize; x++)
                *(dataPtr++) = scanLine[x] - averagePredictor( scanLine[x-bytesPerPixel], prior[x], prior[x-bytesPerPixel] );
        }

        // Filter Type: Paeth
        dataPtr = (qint8 *)encodedScanLines[4].data();
        if (y == 0) {
            for(int x=0;x<bytesPerPixel;x++)
                *(dataPtr++) = scanLine[x] - PaethPredictor( 0, 0, 0);
            for(int x=bytesPerPixel; x<scanLineSize; x++)
                *(dataPtr++) = scanLine[x] - PaethPredictor( scanLine[x-bytesPerPixel], 0, 0);
        } else {
            for(int x=0;x<bytesPerPixel;x++)
                *(dataPtr++) = scanLine[x] - PaethPredictor( 0, prior[x], 0 );
            for(int x=bytesPerPixel; x<scanLineSize; x++)
                *(dataPtr++) = scanLine[x] - PaethPredictor( scanLine[x-bytesPerPixel], prior[x], prior[x-bytesPerPixel] );
        }

        // Ok, now the scanline has been encoded. Use the 'sum of absolute
        // differences' heuristics to choose what encoding fits us best
        quint32 bestSAD = scanLineSize*255;
        quint8  bestFilter = 0;
        for(int i=0; i<5; i++) {
            quint32 mySAD = 0;
            for(int x=0; x<scanLineSize; x++)
                mySAD += abs(encodedScanLines[i][x]);
            if (mySAD < bestSAD) {
                bestSAD = mySAD;
                bestFilter = i;
            }
        }

        // Copy scanline with the dest encoding into result array
        quint8 filterType = bestFilter;
        result[y*(scanLineSize+1)] = filterType;
        std::memcpy(result.data()+y*(scanLineSize+1)+1, encodedScanLines[filterType].data(), scanLineSize);
    }

    return result;
}


QImage imageOperations::DataToQImage(const QByteArray &data, int width, int height, QImage::Format format, imageDataFilter filter)
{
    QImage result(width,height,format);
    if (result.isNull())
        return QImage();

    int scanLineSize;
    quint8 bytesPerPixel;
    switch(format) {
    case QImage::Format_Mono:
        scanLineSize = (width+7)/8;
        bytesPerPixel = 0;
        break;
    case QImage::Format_Indexed8:
        scanLineSize = width;
        bytesPerPixel = 1;
        break;
    case QImage::Format_ARGB32:
        scanLineSize = 4*width;
        bytesPerPixel = 4;
        break;
    case QImage::Format_RGB888:
        scanLineSize = 3*width;
        bytesPerPixel = 3;
        break;
    case QImage::Format_Grayscale8:
        scanLineSize = width;
        bytesPerPixel = 1;
        break;
    default:
        return QImage();
    }


    // Filter 'identity'
    if (filter == identity) {
        // Check array size
        if (data.size() != scanLineSize*height)
            return QImage();

        // Copy data into image
        for(int y=0; y<height; y++)
            std::memcpy(result.scanLine(y), data.data()+y*scanLineSize, scanLineSize);

        // Done
        return result;
    }

    // Filter 'PNG'
    if (filter == PNG) {
        // Check array size and image format
        if (bytesPerPixel == 0)
            return QImage();
        if (data.size() != (scanLineSize+1)*height)
            return QImage();

        // Data pointers
        const qint8 *dataPtr = (qint8 *)data.data();

        // Apply filter
        qint8 *scanLine = nullptr;
        for(int y=0;y<height;y++) {
            qint8 *prior = scanLine;
            scanLine = (qint8 *)result.scanLine(y);

            switch(*(dataPtr++)) {
            case 0: // Filter Type: None
                std::memcpy(scanLine, dataPtr, scanLineSize);
                dataPtr += scanLineSize;
                break;

            case 1: // Filter Type: Sub
                for(int x=0;x<bytesPerPixel;x++)
                    scanLine[x] = *(dataPtr++);
                for(int x=bytesPerPixel; x<scanLineSize; x++)
                    scanLine[x] = *(dataPtr++) + scanLine[x-bytesPerPixel];
                break;

            case 2: // Filter Type: Up
                if (y == 0) {
                    std::memcpy(scanLine, dataPtr, scanLineSize);
                    dataPtr += scanLineSize;
                } else {
                    for(int x=0; x<scanLineSize; x++)
                        scanLine[x] = *(dataPtr++) + prior[x];
                }
                break;

            case 3: // Filter Type: Average
                if (y == 0) {
                    for(int x=0;x<bytesPerPixel;x++)
                        scanLine[x] = *(dataPtr++) + averagePredictor( 0, 0, 0);
                    for(int x=bytesPerPixel; x<scanLineSize; x++)
                        scanLine[x] = *(dataPtr++) + averagePredictor( scanLine[x-bytesPerPixel], 0, 0);
                } else {
                    for(int x=0;x<bytesPerPixel;x++)
                        scanLine[x] = *(dataPtr++) + averagePredictor( 0, prior[x], 0 );
                    for(int x=bytesPerPixel; x<scanLineSize; x++)
                        scanLine[x] = *(dataPtr++) + averagePredictor( scanLine[x-bytesPerPixel], prior[x], prior[x-bytesPerPixel] );
                }
                break;

            case 4: // Filter Type: Paeth
                if (y == 0) {
                    for(int x=0;x<bytesPerPixel;x++)
                        scanLine[x] = *(dataPtr++) + PaethPredictor( 0, 0, 0);
                    for(int x=bytesPerPixel; x<scanLineSize; x++)
                        scanLine[x] = *(dataPtr++) + PaethPredictor( scanLine[x-bytesPerPixel], 0, 0);
                } else {
                    for(int x=0;x<bytesPerPixel;x++)
                        scanLine[x] = *(dataPtr++) + PaethPredictor( 0, prior[x], 0 );
                    for(int x=bytesPerPixel; x<scanLineSize; x++)
                        scanLine[x] = *(dataPtr++) + PaethPredictor( scanLine[x-bytesPerPixel], prior[x], prior[x-bytesPerPixel] );
                }
                break;

            default: // Unknown filter
                return QImage();
            }

        }

        return result;
    }


    // Unknown filter
    return QImage();
}
