/*
 * Copyright © 2017 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "length.h"

struct unitEntry {const char *txt; length::unit u; qreal magnitude;};

static std::vector<unitEntry> unitTable = {
  {"cm", length::cm, 3600.0},
  {"in", length::in, 9144.0},
  {"mm", length::mm,  360.0},
  {"pt", length::pt,  127.0},
  {nullptr,    length::pt,    0.0}
};


bool length::set(QString lstring)
{
  bool myOk;
  
  lstring = lstring.simplified().toLower();
  qreal d = lstring.leftRef(lstring.size()-2).toDouble(&myOk);
  
  if (myOk) {
    for(int i=0; unitTable[i].txt != nullptr; i++) {
      if (lstring.endsWith(unitTable[i].txt)) {
	set(d, unitTable[i].u);
	return true;
      }
    }
  }
  
  _length = 0.0;  
  return false;
}


qreal length::get(unit u) const
{
  for(int i=0; unitTable[i].txt != nullptr; i++) 
    if (unitTable[i].u == u)
      return _length/unitTable[i].magnitude;
  return 0.0;    
}


void length::set(qreal l, unit u)
{
  for(int i=0; unitTable[i].txt != nullptr; i++) 
    if (unitTable[i].u == u)
      _length = l*unitTable[i].magnitude;
}
