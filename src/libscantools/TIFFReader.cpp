/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QDebug>
#include <QImageReader>

#include "TIFFReader.h"

/* Implementation details

   On construction, the TIFFReader copies the TIFF file to a temporary file.  It
   reads the file, and determines the locations of the individual directories,
   which is TIFF speak for 'imges'.  The locations are stored in IFDOffsets, as
   byte offsets relative to the beginning of the file.  

   The operator[], which is the only non-trivial method in this class, seek's to
   position 4, which is the place where the offset for the first image is
   stored.  If the n.th page is requested, it writes the offset of the n.th
   directory to that place, so that the temporary TIFF file effectively become a
   multi-page TIFF whose first image is the one that we want.  The file is then
   read using standard Qt methods for reading TIFF files; these methods always
   read the first image in a multi-page TIFF, and thus the image that we want.
*/


TIFFReader::TIFFReader(const QString& fileName) : ioStream(&tmpFile)
{
  if (!QImageReader::supportedImageFormats().contains("tiff")) {
    _error = "Internal error: no TIFF image format plugin is available for Qt. "
      "Please ensure that the relevant plugin is installed. "
      "On many systems, the plugin is contained in a software package calles "
      "'qt5-qtimageformats' or similar.";
    return;
  }
    

  // Open temporary file
  if (!tmpFile.open()) {
    return;
  }
  
  // Copy fileName to a temporary file
  QFile originalFile(fileName);
  if (!originalFile.open(QIODevice::ReadOnly)) {
    return;
  }
  if (tmpFile.write(originalFile.readAll()) == -1) {
    return;
  }
  if (!tmpFile.seek(0)) {
    return;
  }
  
  // TIFFs come in big and small-endian flavor. The first two bytes of the file
  // decide which is which. We read this datum, and set the endian-ness of our
  // QDataStream accordingly.
  quint16 endianIndicator = 0;
  ioStream >> endianIndicator;
  if (tmpFile.error() != QFileDevice::NoError) {
    return;
  }
  switch(endianIndicator) {
  case 0x4949:
    ioStream.setByteOrder(QDataStream::LittleEndian);
    break;
  case 0x4D4D:
    ioStream.setByteOrder(QDataStream::BigEndian);
    break;
  default:
    _error = QString("File '%1' is not a TIFF file: the endian-ness marker is missing at the beginning of the file.").arg(fileName);
    return;
  }
  
  // The next two bytes contain a magic number, which also servers as a test for
  // endian-ness. Read it, and refuse to go on if we do not find the right
  // number.
  quint16 magicNumber = 0;
  ioStream >> magicNumber;
  if (tmpFile.error() != QFileDevice::NoError) {
    return;
  }
  if (magicNumber != 42) {
    _error = QString("File '%1' is not a TIFF file: the magic number is missing at the beginning of the file.").arg(fileName);
    return;
  }
  
  // Next, go through the file and obtain list of offsets that mark the
  // beginnings of the 'directories', that is, of the images contained in the
  // TIFF file.
  quint32 IFDOffset = 0;
  ioStream >> IFDOffset;
  while(IFDOffset != 0) {
    if (IFDOffset > tmpFile.size()) {
      _error = QString("File format error. An IFDOffset in the file '%1' points to a location outside of the file.").arg(fileName);
      return;
    }
    IFDOffsets << IFDOffset;
    
    tmpFile.seek(IFDOffset);
    quint16 numberOfDirectoryEntries = 0;
    ioStream >> numberOfDirectoryEntries;
    qint64 offsetOfnextIFDOffset = IFDOffset+2+numberOfDirectoryEntries*12;
    if (offsetOfnextIFDOffset > tmpFile.size())  {
      _error = QString("File format error. The file '%1' ends abruply.").arg(fileName);
      return;
    }
    tmpFile.seek(IFDOffset+2+numberOfDirectoryEntries*12);
    ioStream >> IFDOffset;
  }

  // In case that there was an error, we make sure to not use the file.
  if (tmpFile.error() != QFileDevice::NoError) {
    IFDOffsets.clear();
  }
}


TIFFReader::~TIFFReader()
{
  // Lock the mutex to make sure that no pending read-processes use the image
  // that we are about to delete.
  QMutexLocker locker(&mutex);
}

auto TIFFReader::hasError() -> bool
{
  // Lock the mutex to make sure that the error string is not modified while we
  // are trying to access it.
  QMutexLocker locker(&mutex);
  
  return ((!_error.isEmpty()) || (tmpFile.error() != QFileDevice::NoError));
}


auto TIFFReader::error() -> QString
{
  // Lock the mutex to make sure that the error string is not modified while we
  // are trying to access it.
  QMutexLocker locker(&mutex);
  
  if (!_error.isEmpty()) {
    return _error;
  }
  return tmpFile.errorString();
}


auto TIFFReader::size() -> quint32
{
  // Lock the mutex to make sure that the instance is not being deleted while we
  // are trying to access it.
  QMutexLocker locker(&mutex);
  
  return IFDOffsets.size();
}


auto TIFFReader::operator[](quint16 pageNumber) -> QImage
{
    // Lock the mutex to make sure that no-one else modifies the file that we are
    // trying to modify.
    QMutexLocker locker(&mutex);

    // Paranoid safety check: if pageNumber is too big, simply return an empty
    // image
    if (pageNumber >= IFDOffsets.size()) {
        return QImage();
    }

    // Move to the position in the file where the 'offset of the first directory'
    // is stored. Then write the offset of the desired image there.
    tmpFile.seek(4);
    ioStream << IFDOffsets[pageNumber];

    // The command 'tmpFile.seek(4)' guarantees that the changes are written to
    // the file; the command 'tmpFile.flush()' does not seem to guarantee that.
    tmpFile.seek(4);
    tmpFile.flush();

    // Now use standard Qt machinery to read the file.
    return QImage(tmpFile.fileName());
}
