/*
 * Copyright © 2018 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "HOCRDocument.h"

#include <QBuffer>
#include <QReadWriteLock>
#include <leptonica/allheaders.h>
#if (LIBLEPT_MAJOR_VERSION == 1 && LIBLEPT_MINOR_VERSION >= 83) || LIBLEPT_MAJOR_VERSION > 1
#include "leptonica/pix_internal.h"
#endif
#include <tesseract/baseapi.h>
#if TESSERACT_MAJOR_VERSION < 5
#include <tesseract/genericvector.h>
#endif


// Lock used to provide thread-safety
QReadWriteLock tesseract_languageCacheLock;

QStringList tesseract_languageCache;



void HOCRDocument::read(const QImage &image, const QStringList& languages)
{
    // Paranoid safety check. Make sure that every language given is actually
    // supported by tesseract
    if (!areLanguagesSupportedByTesseract(languages)) {
        _error = QString("Error. Trying to set non-supported language in HOCRDocument::read().");
        return;
    }

    // Handle empty images properly
    clear();
    if (image.isNull()) {
        return;
    }

    // Convert image to grayscale, unless bitonal
    QImage myImage;
    if (image.format() == QImage::Format_Mono) {
        myImage = image;
    } else {
        myImage = image.convertToFormat(QImage::Format_Grayscale8);
    }

    // Convert image to leptonicaImage
    Pix leptonicaImage{};
    leptonicaImage.w = myImage.width();                     /* width in pixels                   */
    leptonicaImage.h = myImage.height();                    /* height in pixels                  */
    leptonicaImage.d = (myImage.format() == QImage::Format_Mono) ? 1 : 8;                        /* depth in bits                     */
    leptonicaImage.spp = 1;                                 /* the image data                    */
    leptonicaImage.wpl = myImage.bytesPerLine()/4;          /* 32-bit words/line                 */
    leptonicaImage.refcount = 1;                            /* reference count (1 if no clones)  */
    leptonicaImage.xres = image.dotsPerMeterX()*2.54/100.0;   /* image res (ppi) in x direction (use 0 if unknown)                */
    leptonicaImage.yres = image.dotsPerMeterY()*2.54/100.0;   /* image res (ppi) in y direction (use 0 if unknown)                */
    leptonicaImage.informat = 1;                            /* input file format, IFF_*          */
    leptonicaImage.text = nullptr;                                /* text string associated with pix   */
    leptonicaImage.colormap = nullptr;                            /* colormap (may be null)            */
    leptonicaImage.data = (l_uint32 *)myImage.scanLine(0);  /* the image data                    */
    pixEndianByteSwap(&leptonicaImage);

    // Work around a bug in tesseract. Experiments show that tesseract is
    // locale-dependent and does not work properly in locales where the numbers
    // are not US-style.  So, we temporarily set the locale to "C".
    QByteArray oldLocale(std::setlocale(LC_ALL, nullptr));
    std::setlocale(LC_ALL, "C");

    // Initialize tesseract-ocr with English, without specifying tessdata path
    auto *api = new tesseract::TessBaseAPI();
    QString languageString = languages.join("+");
    if (languageString.isEmpty()) {
        languageString = "eng";
    }

#ifdef TESSERACT_HAS_OEM_LSTM_ONLY
    auto initSuccess = api->Init(nullptr, languageString.toLocal8Bit(), tesseract::OEM_LSTM_ONLY);
#else
    auto initSuccess = api->Init(NULL, languageString.toLocal8Bit());
#endif
    if (initSuccess != 0) {
        _error = QString("Error. Cannot initialise the tesseract OCR engine.");
        delete api;

        // Restore locale
        std::setlocale(LC_ALL, oldLocale.constData());
        return;
    }

    // Set image, get OCR result in HOCR format
    api->SetImage(&leptonicaImage);
    auto* outText = api->GetHOCRText(0);
    QByteArray bA(outText);
    delete [] outText;

    // Destroy used object and release memory
    api->End();
    delete api;

    // Restore locale
    std::setlocale(LC_ALL, oldLocale.constData());

    // Open HOCR in buffer, read buffer via standard method
    QBuffer buffer(&bA);
    buffer.open(QIODevice::ReadOnly);
    read(&buffer);
}


auto HOCRDocument::tesseractLanguages() -> QStringList
{
    tesseract_languageCacheLock.lockForRead();
    auto myCache = tesseract_languageCache;
    tesseract_languageCacheLock.unlock();

    if (myCache.isEmpty()) {
        tesseract_languageCacheLock.lockForWrite();

        // Work around a bug in tesseract. Experiments show that tesseract is
        // locale-dependent and does not work properly in locales where the numbers
        // are not US-style.  So, we temporarily set the locale to "C".
        QByteArray oldLocale(std::setlocale(LC_ALL, nullptr));
        std::setlocale(LC_ALL, "C");

        auto *api = new tesseract::TessBaseAPI();
        api->Init(nullptr, "eng");
#if TESSERACT_MAJOR_VERSION < 5
        GenericVector<STRING> langs;
#else
        std::vector<std::string> langs;
#endif

        api->GetAvailableLanguagesAsVector(&langs);
        for(std::vector<std::string>::size_type i=0; i<langs.size(); i++) {
#if TESSERACT_MAJOR_VERSION < 5
            auto line = QString::fromLocal8Bit(langs[i].string());
#else
            auto line = QString::fromStdString(langs[i]);
#endif
            if (line.startsWith("List of")) {
                continue;
            }
            if (line.startsWith("osd")) {
                continue;
            }
            if (line.isEmpty()) {
                continue;
            }
            myCache.append(line);
        }
        api->End();
        tesseract_languageCache = myCache;
        delete api;

        // Restore locale
        std::setlocale(LC_ALL, oldLocale.constData());

        tesseract_languageCacheLock.unlock();
    }

    return myCache;
}


auto HOCRDocument::areLanguagesSupportedByTesseract(const QStringList& lingos) -> bool
{
    QStringList supportedLingos = tesseractLanguages();

    foreach(auto language, lingos) {
        if (supportedLingos.contains(language)) {
            continue;
        }
        return false;
    }

    return true;
}
