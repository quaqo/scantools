/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QImage>
#include <QPainter>

#include "imageOperations.h"
#include "imageOperationsTest.h"

QTEST_MAIN(imageOperationsTest)

// Helper function -- collects all metadata in one convenient string
QString metaData(const QImage &image)
{
    QString result = image.text();
    result += "\n" + QString::number(image.dotsPerMeterX());
    result += "\n" + QString::number(image.dotsPerMeterY());
    return result;
}


void imageOperationsTest::initTestCase()
{
    // Full color image with unused alpha channel
    Hydrogenea = QImage(QFINDTESTDATA("testData/images/PNG/Hydrogenea.png"));

    // Indexed color with 88 colors
    Hydrogenea88 = Hydrogenea.convertToFormat(QImage::Format_Indexed8);

    // Grayscale with 241 colors
    QImage HydrogeneaGray = Hydrogenea.convertToFormat(QImage::Format_Grayscale8);

    // Black and White image, black comes first in colortable
    QVector<QRgb> bwColors(2);
    bwColors[0] = 0xFF000000;
    bwColors[1] = 0xFFFFFFFF;
    QImage HydrogeneaBW = Hydrogenea.convertToFormat(QImage::Format_Mono, bwColors);

    // Monocolor
    QImage Monocolor = QImage(100,100,QImage::Format_RGB888);
    Monocolor.fill(0x123456);


    QImage image;

    // Full color image with unused alpha channel
    image = Hydrogenea;
    image.setText("Description", "Full color image with unused alpha channel");
    image.setText("NumCols", "0");
    image.setText("BW", "N");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_RGB888));
    images.append(image);

    // Full color image with one transparent pixel
    image = Hydrogenea.convertToFormat(QImage::Format_ARGB32);
    image.setPixel(100,100,0x00123456);
    image.setText("Description", "Full color image with one transparent pixel");
    image.setText("NumCols", "0");
    image.setText("BW", "N");
    image.setText("Opaque", "N");
    image.setText("BestFormat", QString::number((int)QImage::Format_ARGB32));
    images.append(image);

    // Full color image that uses only 88 colors
    image = Hydrogenea88.convertToFormat(QImage::Format_ARGB32);
    image.setText("Description", "Full color image that uses only 88 colors");
    image.setText("NumCols", "88");
    image.setText("BW", "N");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Indexed8));
    images.append(image);

    // Indexed color with 88 colors and with inefficient color table
    image = Hydrogenea88;
    image.setText("Description", "Indexed color with 88 colors and with inefficient color table");
    image.setText("NumCols", "88");
    image.setText("BW", "N");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Indexed8));
    images.append(image);

    // Indexed color with one transparent color
    image = Hydrogenea88;
    image.setColor(0, 0x00123456);
    image.setText("Description", "Indexed color with one transparent color");
    image.setText("NumCols", "88");
    image.setText("BW", "N");
    image.setText("Opaque", "N");
    image.setText("BestFormat", QString::number((int)QImage::Format_Indexed8));
    images.append(image);

    // Grayscale image
    image = HydrogeneaGray;
    image.setText("Description", "Grayscale image");
    image.setText("NumCols", "241");
    image.setText("BW", "N");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Grayscale8));
    images.append(image);

    // Black and White image, black comes first in colortable
    image = HydrogeneaBW;
    image.setText("Description", "Black and White image, black comes first in colortable");
    image.setText("NumCols", "2");
    image.setText("BW", "Y");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Mono));
    images.append(image);

    // Black and White image, white comes first in colortable
    image = HydrogeneaBW;
    image.setColor(0, HydrogeneaBW.color(1));
    image.setColor(1, HydrogeneaBW.color(0));
    image.setText("Description", "Black and White image, white comes first in colortable");
    image.setText("NumCols", "2");
    image.setText("BW", "Y");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Mono));
    images.append(image);

    // Black and white image with an inefficient color table
    QVector<QRgb> colors;
    colors.append(0xFF000000);
    colors.append(0xFFFFFFFF);
    colors.append(0x00123456);  // Add one fully transparent color that is not gray or black-and-white
    image = HydrogeneaBW.convertToFormat(QImage::Format_Indexed8,colors);
    image.setText("Description", "Black and white image with an inefficient color table");
    image.setText("NumCols", "2");
    image.setText("BW", "Y");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Mono));
    images.append(image);

    // Black and white image saved as full color
    image = HydrogeneaBW.convertToFormat(QImage::Format_RGB888);
    image.setText("Description", "Black and white image saved as full color");
    image.setText("NumCols", "2");
    image.setText("BW", "Y");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Mono));
    images.append(image);

    // Monocolor image
    image = Monocolor;
    image.setText("Description", "Monocolor image");
    image.setText("NumCols", "1");
    image.setText("BW", "N");
    image.setText("Opaque", "Y");
    image.setText("BestFormat", QString::number((int)QImage::Format_Mono));
    images.append(image);
}

void imageOperationsTest::cleanupTestCase()
{
    ;
}


void imageOperationsTest::colors_data()
{
    QTest::addColumn<QImage>("image");
    QTest::addColumn<int>("numColors");

    foreach(auto image, images) {
        QTest::newRow(image.text("Description").toLocal8Bit()) << image << image.text("NumCols").toInt();
    }
}

void imageOperationsTest::colors()
{
    QFETCH(QImage, image);
    QFETCH(int, numColors);
    QCOMPARE(imageOperations::colors(image).size(), numColors);
}


void imageOperationsTest::deAlpha_data()
{
    QTest::addColumn<QImage>("image");

    foreach(auto image, images) {
        QTest::newRow(image.text("Description").toLocal8Bit()) << image;
    }
}

void imageOperationsTest::deAlpha()
{
    QFETCH(QImage, image);

    QRgb background = 0x20123456;
    QImage deAlpha1 = imageOperations::deAlpha(image,background).convertToFormat(QImage::Format_RGB32);

    QImage deAlpha2(image.size(), QImage::Format_RGB32);
    deAlpha2.fill(background | 0xFF000000);
    QPainter paint(&deAlpha2);
    paint.drawImage(0,0, image);
    paint.end();

    QCOMPARE(deAlpha1, deAlpha2);
    QCOMPARE(metaData(deAlpha1), metaData(image));
    QVERIFY(imageOperations::isOpaque(deAlpha1));
}



void imageOperationsTest::isBlackAndWhite_data()
{
    QTest::addColumn<QImage>("image");
    QTest::addColumn<bool>("isBlackAndWhite");

    foreach(auto image, images) {
        QTest::newRow(image.text("Description").toLocal8Bit()) << image << (image.text("BW") == "Y");
    }
}

void imageOperationsTest::isBlackAndWhite()
{
    QFETCH(QImage, image);
    QFETCH(bool, isBlackAndWhite);
    QCOMPARE(imageOperations::isBlackAndWhite(image), isBlackAndWhite);
}



void imageOperationsTest::isOpaque_data()
{
    QTest::addColumn<QImage>("image");
    QTest::addColumn<bool>("isOpaque");

    foreach(auto image, images) {
        QTest::newRow(image.text("Description").toLocal8Bit()) << image << (image.text("Opaque") == "Y");
    }
}

void imageOperationsTest::isOpaque()
{
    QFETCH(QImage, image);
    QFETCH(bool, isOpaque);
    QCOMPARE(imageOperations::isOpaque(image), isOpaque);
}


void imageOperationsTest::optimizedFormat_data()
{
    QTest::addColumn<QImage>("image");
    QTest::addColumn<int>("format");

    foreach(auto image, images) {
        QTest::newRow(image.text("Description").toLocal8Bit()) << image << image.text("BestFormat").toInt();
    }
}

void imageOperationsTest::optimizedFormat()
{
    QFETCH(QImage, image);
    QFETCH(int, format);

    QImage optimized = imageOperations::optimizedFormat(image);

    // Check if the optimized image has the desired format
    QCOMPARE((int)optimized.format(), format);
    QCOMPARE(metaData(optimized), metaData(image));
    QCOMPARE(imageOperations::deAlpha(image).convertToFormat(QImage::Format_ARGB32), imageOperations::deAlpha(optimized).convertToFormat(QImage::Format_ARGB32));

    if ((optimized.format() == QImage::Format_Mono) && (optimized.colorCount() == 2))
        QVERIFY(qGray(optimized.color(0)) >= qGray(optimized.color(1)));
}


void imageOperationsTest::simplifyTransparentPixels()
{
    QImage image; QImage image2;

    // Indexed image
    image = Hydrogenea88;
    image.setColor(0, 0x00123456);
    image2 = imageOperations::simplifyTransparentPixels(image);
    QCOMPARE((quint32)image2.color(0), (quint32)0x00FFFFFF);
    QCOMPARE(metaData(image2), metaData(image));

    // Full color with one transparent pixel
    image = Hydrogenea.convertToFormat(QImage::Format_ARGB32);
    image.setPixel(100,100,0x00123456);
    image2 = imageOperations::simplifyTransparentPixels(image);
    QCOMPARE((quint32)image2.pixel(100,100), (quint32)0x00FFFFFF);
    QCOMPARE(metaData(image2), metaData(image));
}


void imageOperationsTest::ToAndFromData_data()
{
    QTest::addColumn<QImage>("image");

    foreach(auto image, images) {
        QTest::newRow(image.text("Description").toLocal8Bit()) << imageOperations::optimizedFormat(image);
    }
}


void imageOperationsTest::ToAndFromData()
{
    QFETCH(QImage, image);

    QByteArray data;
    QImage reconstructed;

    data = imageOperations::QImageToData(image);
    reconstructed = imageOperations::DataToQImage(data, image.width(), image.height(), image.format());
    reconstructed.setColorTable(image.colorTable());
    QCOMPARE(image, reconstructed);

    if (image.format() != QImage::Format_Mono) {
        data = imageOperations::QImageToData(image, imageOperations::PNG);
        reconstructed = imageOperations::DataToQImage(data, image.width(), image.height(), image.format(), imageOperations::PNG);
        reconstructed.setColorTable(image.colorTable());
        QCOMPARE(image, reconstructed);
    }
}

