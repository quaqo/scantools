/*
 * Copyright © 2017-2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QBuffer>
#include <QDataStream>

#include "JP2Box.h"


JP2Box::JP2Box()
{
    clear();
}


JP2Box::JP2Box(QIODevice &device, quint32 nTBox)
{
    read(device, nTBox);
}


auto JP2Box::subBox(quint32 nTBox) -> JP2Box
{
    QBuffer buffer(&content);
    buffer.open(QIODevice::ReadOnly);
    return JP2Box(buffer, nTBox);
}


void JP2Box::clear()
{
    content.clear();
    error = QString();
    TBox = 0;
}


void JP2Box::read(QIODevice &device, quint32 nTBox)
{
    clear();

    while(!device.atEnd()) {
        quint32 LBox = 0;
        quint64 XLBox = 0;

        QDataStream stream(&device);

        stream >> LBox;
        stream >> TBox;

        // Read content of the box. The length of the content is a little
        // complicated to compute…
        if (LBox == 0) {
            // If LBox == 0, read to the end of the file. No way to check if all bytes
            // have been read or if the file has been truncted.
            content = device.readAll();
        } else {
            // If LBox != 0, the number of bytes depends on the values of LBox
            qint64 bytesToBeRead = 0;
            if (LBox == 1) {
                stream >> XLBox;
                bytesToBeRead = XLBox - 16;
            } else {
                bytesToBeRead = LBox - 8;
            }

            // Read bytes
            content = device.read(bytesToBeRead);

            // Check for errors
            if (content.size() != bytesToBeRead) {
                clear();
                error = device.errorString();
                return;
            }
        }

        // If the box that we have read has the correct TBox value, of if _TBox ==
        // 0, return now. Otherwise, continue reading the next box.
        if ((nTBox == 0) || (TBox == nTBox)) {
            return;
        }
    }

    // If all boxes have been read and no box with the correct TBox value has been
    // found, return an empty box.
    clear();
}
