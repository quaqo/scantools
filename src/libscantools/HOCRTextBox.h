/*
 * Copyright © 2016--2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HOCRTEXTBOX
#define HOCRTEXTBOX 1

#include <QPainter>
#include <QSet>
#include <QXmlStreamReader>

#include "resolution.h"

class HOCRDocument;


/** Text box, as defined in an HOCR file

    This class represents a box that may contain text and also other text
    boxes. Every text box has a bounding rectagle, text, a type, and a list of
    attributes. There are several helper functions to interpret the attribules.  

    Texboxes can be rendered on a QPainter.  For users who wish to implement
    their own rendering routines, there are several helper functions that give
    suggestions for optimal rendering.

    The methods of this class are reentrant, but not thread safe.
*/

class HOCRTextBox
{
 public:
  /** Constructs an empty text box */
  HOCRTextBox();

  /** Decide if the text box contains non-trivial text

      @returns True if the text box or any of its subboxes contains text other
      than white space.
   */
  bool hasText() const;
  
  /** Text angle
       
      @returns The text angle, as specified in the HOCR file or inherited from
      parent. If no angle line was specified, this number is zero.
  */
  qreal angle() const {return _angle;};
  
  /** Returns the attributes of the textBox 
      
      @returns The attributes of the textBox, as stored in the corresponding
      entity of the HOCR file.
  */
  QXmlStreamAttributes attributes() const {return _attributes;};
  
  /**  Base line as a polynomial
       
       @returns The base line as a polynomial, as specified in the HOCR file or
       inherited from parent. If no base line was specified, this vector is
       empty.
  */
  QVector<qreal> baselinePolynomial() const {return _baselinePolynomial;};
  
  /** Base line reference point
      
      @returns The base line reference point, as specified in the HOCR file or
      inherited from parent. If no base line polynomial is specified, this
      member is meaningless.
  */
  QPoint baselineReferencePoint() const {return _baselineReferencePoint;};
  
  /** Bounding box

      @returns The bounding box of this textBox.  If no bounding box was
      specified in the HOCR file, an empty box is returned.
  */
  QRect boundingBox() const {return _boundingBox;};

  /** Class of this textBox

      @returns The type of this textBox. Typical types are "ocr_page",
      "ocr_carea", "ocr_par", "ocr_line" or "ocrx_word".  If the HOCR element
      described by this box is not of OCR-related type, an empty string is
      returned.
   */
  QString classType() const;

  /** Confidence level

      @returns The confidence level for the content of this textBox.  If no
      confidence level was specified in the HOCR file, -1 is returned.
   */
  int confidence() const {return _confidence;};

  /** Text flow direction

      @returns The text flow direction of the corresponding element in the HOCR
      file.  Returns 'ltr' for left-to-right, and 'rtl' for right-to-left.  Any
      other return value means 'undefined'.
   */
  QString direction() const {return _direction;};

  /** Font size

      @returns The font size for the content of this textBox.  If no font size
      was specified in the HOCR file, 0.0 is returned.
   */
  qreal fontSize() const {return _fontSize;};
  
  /** Image associated with content of this text box

      @returns The name of an image file associated with the content of this
      text box. If nothing is specified in the HOCR file, an empty string is
      returned.
  */
  QString imageName() const {return _imageName;};

  /** Language of the content of this text box. 

      @returns The name of the language of the content of this text box ('eng').
      If nothing is specified in the HOCR file, this string is empty.
  */
  QString language() const {return _language;};

  /** Paint the contents of the text box to a painter
      
      Paint the contents of the text box to a painter.  Coordinates are in
      pixels. This is convenient if the contents of an HOCR file is drawn onto a
      bitmap that is meant to resemble the original scanned image as closely as
      possible. When writing to a PDF, for instance, resolution needs to be
      taken into account.

      @param painter QPainter to be used in the paint job
  */
  void render(QPainter &painter) const;

  /** Export this text box as an image
      
      Paint the contents of the text box to an image and returns the image. In
      case of an error, an empty image is returned.

      @param overrideFont If null, a standard font will be taken. If not null,
      then the specified font will be taken.

      @param format Format of the returned QImage

      @returns An image
  */
  QImage toImage(const QFont &overrideFont, QImage::Format format=QImage::Format_Grayscale8) const;

  /** Return raw PDF text rendering commands

      This method converts the text box into a sequence of raw PDF text
      rendering command that are suitable for inclusion in a PDF file. The
      commands refer to the font /F1 and assume that this font exists.  The text
      is encoded using the Windows-1252 encoding. Characters that cannot be
      represented in this encoding are discarded.

      @param font A reference to a QFont that should match the font "/F1" as
      closely as possible. The font is used to optimise placement of the text.

      @param xRes Horizontal resolution. Used to transform the bitmap
      coordinates found in the HOCR file to physical sizes.  Needs to be valid,
      or else the result is undefined.

      @param yRes Vertical resolution. Used to transform the bitmap coordinates
      found in the HOCR file to physical sizes.  Needs to be valid, or else the
      result is undefined.

      @param deltaX Horizontal offest for text placement

      @param deltaY Vertical offest for text placement

      @returns A QByteArray containing PDF drawing commands
   */
  QByteArray toRawPDFContentStream(const QFont &font, resolution xRes, resolution yRes, length deltaX=length(), length deltaY=length()) const;

  /** Export this text box as text
      
      @returns The contents of this text box, and of all child text boxes, with
      line breaks added.
  */
  QString toText() const;
  
  /** Estimate how well a given font fits the textbox

      For this box, and for each subbox, this method computes the width of the
      text when rendered with the given font, and compares it to the width of
      the bounding box.  The square of the difference is then computed, and the
      results are added up.

      @param font Font with which the text is to be rendered. The font's
      pixelSize will be changed in the process.

      @returns Sum of squares. The lower the number, the better does the font
      fit the textbox
   */
  qint64 estimateFit(const QFont &font) const;
    
  /** Suggest font
      
      Suggests a font for rendering this document.  Three standard fonts
      ("Helvetica", "Times", "Courier") are tried, and the one is chose that
      fits the text box best.
      
      @returns The best-fitting font

      @warning This method is expensive
  */
  QFont suggestFont() const;
  
  /** Text content of the text box
      
      @returns The text of this text box. If nothing is specified in the HOCR
      file, an empty string is returned.
  */
  QString text() const {return _text;};

 private:
  // Specifies how and where the textbox' content should be drawn.
  struct renderingHints {
    /** Suggested font size */
    int fontSize;

    /** Suggested point from which to draw the text of this text box. */
    QPoint referencePoint;

    /** Suggested horizontal stretch factor */
    qreal horizontalStretchFactor;    
  };

  // For a given font, this method computes rendering hints so that the text
  // fits the bounding box best.
  //
  // - If the HOCR file specifies a font size, this size is taken. Otherwise,
  // - the font metric is used to find a size that best fits the bounding
  // - box. As a last resort, a standard value is returned.
  //
  // - The font metric is used to suggest a point from which to draw the text of
  // - this text box, and a horizontal stretch factor
  renderingHints getRenderingHints(const QFont &font) const;

  // This is to ensure that text boxes can be constructed from HOCR documents.
  friend HOCRDocument;
  
  // Constructs a textbox by reading in an xml file. This constructor expects
  // that the QXmlStreamReader points to a start element.  It will read the file
  // until it reaches the corresponding end element.  When the method returns,
  // the QXmlStreamReader points to this end element.  In case of problems,
  // warnings are added to 'warnings'.
  HOCRTextBox(QXmlStreamReader &xml, QSet<QString> &warnings, HOCRTextBox *parent=0);

  // Interprets _attributes and fills in the members _baseLine, _boundingBox,
  // _class, _confidence, _fontSize, _imageName.  Problems encoutered in the
  // interpretation are added to the set 'warnings'.  This method is called only
  // in the constructor.  The code is not part of the constructor to keep the
  // source readable.
  void interpretAttributes(QSet<QString> &warnings, qint64 line, qint64 column);

  // Attributes, as read from the HOCR file
  QXmlStreamAttributes _attributes;

  // List of sub boxes, as read from the HOCR file
  QList<HOCRTextBox> _subBoxes;
  
  /*
   * Attributes extracted from the HOCR file
   */

  // Textangle, as specified in the HOCR file or inherited from parent. If no
  // angle was specified, this number is zero.
  qreal _angle;
  
  // Base line as a polynomial, as specified in the HOCR file or inherited from
  // parent. If no base line was specified, this vector is empty.
  QVector<qreal> _baselinePolynomial;

  // Base line reference point, as specified in the HOCR file or inherited from
  // parent. If no base line polynomial is specified, this member is
  // meaningless.
  QPoint _baselineReferencePoint;

  // Bounding box, as specified in the HOCR file. If no bounding box was
  // specified, this box is empty.
  QRect _boundingBox;
  
  // Contains the class of the corresponding element in the HOCR file. Typical
  // values are "ocr_page", "ocr_carea", "ocr_par", "ocr_line" or "ocrx_word".
  QString _class;

  // Contains the confidence level of the corresponding element in the HOCR
  // file.  If no confidence level is specified, this member contains '-1'.
  int _confidence;
  
  // Contains the text flow direction of the corresponding element in the HOCR
  // file.  The value 'ltr' means left-to-right, 'rtl' means right-to-left.  Any
  // other value means 'undefined'.
  QString _direction;
  
  // Contains the font size specified in the corresponding element in the HOCR
  // file.  If no font size is specified, this member contains '0.0'.
  qreal _fontSize;

  // Contains the name of an image associated with the content of this text
  // box. If nothing is specified in the HOCR file, this string is empty.
  QString _imageName;

  // Language of the content of this text box. If nothing is specified in the
  // HOCR file, this string is empty.
  QString _language;

  // Contains the text of this text box. If nothing is specified in the HOCR
  // file, this string is empty.
  QString _text;

  /*
   * Helper functions
   */
  
  // Expects a string of the form "blabla int int int …" and returns a vector
  // containing the integers
  QVector<int> getIntegers(const QString& spec) const;

  // Expects a string of the form "blabla qreal qreal qreal …" and returns a
  // vector containing the qreals
  QVector<qreal> getFloats(const QString& spec) const;  

  // Trivial method that writes out a floating point number in ASCII, up to four
  // decimal points of precision. Trailing zeroes are deleted for brevity's
  // sake. It seems that Qt cannot do that
  static QByteArray toNumber(qreal x);

  // Internal method that actually does the work for the user method with the
  // same name.  This method is applies recursively over all sub-boxes, and the
  // results are joined. It differs from the user method in that it takes two
  // additional arguments: 'height' is the height of the bounding box for which
  // the user method was called; this is necessary for correct text
  // placement. The argument "currentSize" is the font size last set; this is
  // used to set and re-set the same sizes times and again. The parameter
  // 'codec' is a pointer to the "Windows-1252" QTextCodec.
  QByteArray toRawPDFContentStream(const QFont &font, resolution xRes, resolution yRes, length deltaX, length deltaY, quint16 height, qreal &currentFontSize, QTextCodec *codec) const;
};


#endif
