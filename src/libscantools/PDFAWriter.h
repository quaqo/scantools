/*
 * Copyright © 2016 - 2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PDFDOCUMENT
#define PDFDOCUMENT 1

#include <QFuture>
#include <QList>
#include <QReadWriteLock>
#include <QString>

#include "HOCRDocument.h"
#include "JBIG2Document.h"
#include "paperSize.h"
#include "resolution.h"


/** Simple generator for PDF/A-2b compliant documents

The class takes a number of images and embeds them into a PDF/A file
(Conformance level PDF/A-2b), generating one page per image.  OCR data can
optionally used to create an invisible text overlay, which means that the PDF/A
file will be searchable and that text can be copied from the file.  The class
contains an conversion operator to a QByteArray, which generates the actual
PDF/A file.  This makes it extremely simple to write the PDF data to a QFile, or
any other I/O device.  The life cycle of a PDF/A writer object is mostly this

1. Create a PDF/A writer

2. Set meta data using the methods setAuthor(), setTitle(), etc.

3. Add pages to the PDF/A, using one of the methods addPages()

4. Generate a PDF/A document using the operator QByteArray()

5. Delete the writer object.


@section A0 Examples

A minimal example which creates a PDF/A file might look like this.

\code 
// Minimal example

// Generate document and add JBIG2 images as pages to the document.
// Perform OCR so that the resulting PDF file will have a text layer.
PDFAWriter writer(); 
setAutoOCR(true); // Run tesseract OCR engine using default language English
writer.addPages("Test.jb2");

// Write PDF/A file to 'output.pdf'
QFile outFile("output.pdf");
outFile.open(QIODevice::WriteOnly)
outFile.write(doc);  // Implicitly uses conversion operator to create PDF

// End
\endcode

A more sophisticated example which uses preprocessed HOCR files to create a text
overlay might look like this.

\code 
// Not quite minimal example

// Prepare an HOCR document which will be used to create a text overlay
HOCRDocument  hoDoc("Test.hocr");

// Generate document
PDFAWriter writer();

// Add two image files to the document, using pages from hoDoc to create a text
// overlay.
writer.appendToOCRData(hoDoc);
writer.addPages("Test-1.jb2"); 
writer.addPages("Test-2.tif");

// Write PDF/A file to 'output.pdf'
QFile outFile("output.pdf");
outFile.open(QIODevice::WriteOnly)
outFile.write(doc);  // Implicitly uses conversion operator to create PDF

// End
\endcode


@section A2 Implementation details and limitations

- If the input data is a valid, then output files comply with ISO PDF/A-2b,
  which is the industry standard for scanned documents.  Input data is not
  checked for validity.

- When generating a text overlay, Characters that cannot be encoded by the
  Windows-1252 encoding are silently igored. As a result, OCR data currently
  works with western languages only.

- The PDF file is created in memory. This might be a problem when creating huge
  files that are several GB in size.

- Metadata, which is mandatory in PDF/A files, is stored both as embedded XMP
  and as a PDF info directory.  The PDF info directory, which is optional in the
  PDF/A standard, might be removed in future versions of this library.  For now,
  info directories are included because many current PDF handling programs do
  not interpret XMP data correctly.

- The creation data contained in the metadata section of the PDF file refers to
  the time when the constructor was called.

All methods of this class are reentrant and thread-safe.
*/

class PDFAWriter : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString author READ author WRITE setAuthor NOTIFY authorChanged)
  Q_PROPERTY(QString keywords READ keywords WRITE setKeywords NOTIFY keywordsChanged)
  Q_PROPERTY(QString subject READ subject WRITE setSubject NOTIFY subjectChanged)
  Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
  Q_PROPERTY(paperSize pageSize READ pageSize WRITE setPageSize NOTIFY pageSizeChanged)
  Q_PROPERTY(resolution resolutionOverrideHorizontal READ resolutionOverrideHorizontal WRITE setResolutionOverrideHorizontal NOTIFY resolutionOverrideHorizontalChanged)
  Q_PROPERTY(resolution resolutionOverrideVertical READ resolutionOverrideVertical WRITE setResolutionOverrideVertical NOTIFY resolutionOverrideVerticalChanged)
  Q_PROPERTY(bool autoOCR READ autoOCR WRITE setAutoOCR NOTIFY autoOCRChanged)
  Q_PROPERTY(QStringList autoOCRLanguages READ autoOCRLanguages WRITE setAutoOCRLanguages NOTIFY autoOCRLanguagesChanged)

 public:
  /** Destructor
      
      The destructor waits for all worker threads to finish and can therefore
      take considerable time.
  */
  ~PDFAWriter();
  
  /** Constructor
      
      Constructs a PDFAWriter. The following default values are set.
      
      - The metadata strings 'author', 'keywords', 'subject', 'title' are set to
        empty.

      - The paper size is set to 'empty', so pages will be exactly the size of
        the graphics added to the PDFAWriter

      - The override resolutions are set to zero, so all graphic files are
        expected to contain correct information about their resolutions.
	
      - autoOCR is set to 'false', so no OCR engine will be run automatically.

      @param bestCompression If true, then use the slow, but very effective
      zopfli compression algorithm for the lossless compression of bitmap
      graphics.  Once the PDFAWriter is constructed, this property cannot be
      changed anymore.

      @param parent Standard parent pointer
  **/
  explicit PDFAWriter(bool bestCompression=false, QObject* parent=nullptr);
  
  /** Metadata: Author

      @returns the author string from the PDF/A meta data */
  QString author();
  
  /** Set the author string in the PDF/A meta data

      @param author Name of author
  */
  void setAuthor(const QString &author);
  
  /** Metadata: Keywords

      @returns the keywords string from the PDF/A meta data */
  QString keywords();
  
  /** Set the author string in the PDF/A meta data

      @param keywords Keyword string
  */
  void setKeywords(const QString &keywords);
  
  /** Metadata: Subject string

      @returns the subject string from the PDF/A meta data
  */
  QString subject();
  
  /** Set the subject string in the PDF/A meta data

      @param subject Subject string
  */
  void setSubject(const QString &subject);
  
  /** Metadata: Title String
      
      @returns the title string from the PDF/A meta data */
  QString title();
  
  /** Set the title string in the PDF/A meta data

      @param title Title string
  */
  void setTitle(const QString &title);
  
  /** Page Size 

      @returns the page size set with setPageSize()
  */
  paperSize pageSize();

  /** Sets page size, effective for future calls of the methods addPage()

      @param size Paper size
   */
  void setPageSize(const paperSize size);
  
  /** Sets page size, effective for future calls of the methods addPage()

      @param size Paper size
   */
  void setPageSize(paperSize::format size=paperSize::empty);
  
  /** Horizontal resolution

      @returns horizontal resolution that is currently set
  */
  resolution resolutionOverrideHorizontal();

  /** Set horizontal resolution

      @param horizontal Resolution

      @see setResolutionOverride
  */
  void setResolutionOverrideHorizontal(resolution horizontal);

  /** Vertical resolution

      @returns vertical resolution that is currently set
  */
  resolution resolutionOverrideVertical();

  /** Set vertical resolution

      @param vertical Resolution
      
      @see setResolutionOverride
  */
  void setResolutionOverrideVertical(resolution vertical);
  
  /** Sets graphic resolution for future calls of the methods addPage()
      
      To add a raster graphic to a PDF file, the resolution of the raster
      graphic needs to be known. This method can be used to manually set
      resolutions before adding graphic files that either do not specify their
      resolution, or that contain incorrect information.

      @param horizontal Horizontal resolution, which must be either be valid (in
      other words, horizonal.isValid() must return true), or zero.  If zero,
      this is interpreted as "no override resolution set".

      @param vertical Ditto for vertical resolution.
  */
  void setResolutionOverride(resolution horizontal, resolution vertical);

  /** Overloaded method that sets horizontal and vertical resolution to the same value

      @param res resolution
  */
  void setResolutionOverride(resolution res)
  {
    setResolutionOverride(res, res);
  }

  /** Set horizontal and vertical override resolution to zero */
  void clearResolutionOverride()
  {
    setResolutionOverride(resolution(), resolution());
  }
  
  /** AutoOCR

      @returns the value set previously with setAutoOCR() */
  bool autoOCR();
  
  /** Specify if the tesseract OCR engine should be run automatically
      
      @param autoOCR If set to true, then the PDFAWriter will automatically run
      the tesseract OCR engine in the background whenever pages are added to the
      PDF, unless preprocessed ocr data has been specified via the method
      appendToOCRData().
      
      @see setAutoOCRLanguages()
  */
  void setAutoOCR(bool autoOCR);
  
  /** List of languages used for OCR
      
      @returns the list of languages used in by the tesseract OCR engine, as set previously with setAutoOCRLanguages()
  */
  QStringList autoOCRLanguages();

  /** Specify languages used by the tesseract OCR engine
      
      To improve recognition quality, the tesseract OCR engine needs to know the
      language(s) of the text.  The languages specified here will be passed on
      to tesseract in future runs.

      @param nOCRLanguages List of languages to be used in the OCR
      process. Tesseract identifies languages by their 3-character ISO 639-2
      language codes (e.g. "deu" for German or "fra" for French).  The languages
      specified must be present in the current tesseract installation.  If an
      empty list is provided, English will be used as a default language.

      @returns An empty string in case of success or else a human-readable error
      message in English.

      @see HOCRDocument::tesseractLanguages()
   */
  QString setAutoOCRLanguages(const QStringList& nOCRLanguages);

  /** Specify pre-processed OCR data

      This method can be used to specify pre-processed OCR data that will be
      used to generate a text layer whenever pages are added to the document.

      To be more precisely: every PDFAWriter keeps an internal HOCRDocument, and
      this methods appends the given HOCRDocument to the internal one. Whenever
      pages are added to the document, the first page of the internal document
      is used to generate a text layer and is then removed from the internal
      document.  If the internal document is empty, then either the tesseract
      OCR engine is run (if setAutoOCR() has been set to true) or no text layer
      is generated at all.

      @param doc HOCRDocument that will be appended to the internal document
   */
  void appendToOCRData(const HOCRDocument &doc);

  /** Return a copy of the internal HOCRDocument

      @returns copy of the internal HOCRDocument

      @see appendToOCRData()
  */
  HOCRDocument OCRData();

  /** Delete all pages from the internal HOCRDocument 

      @see appendToOCRData()
  */
  void clearOCRData();
  
  /** Add an image to the PDF document
      
      This method differs from the generic method addPages() only in the
      arguments: it expects a QImage instead of a filename.  The input image
      must not be empty.  The format of the PDF/A data stream will be chosen
      according to the image content.
      
      - Black and white images are written into the PDF/A as images with depth
      one.
      
      - Bitonal images are written into the PDF/A as images with depth 1 and a
        color table of length two.
      
      - Grayscale images are written into the PDF/A as images with depth 8.

      - Images with an indexed color palette are written into the PDF/A without
        change.
      
      - All other images are written into the PDF/A as 24-bit RGB.

      Alpha-channels will be deleted.  The images will be compressed using a
      lossless compressor.  The method is therefore slow.  Currently, Black and
      white and bitonal images are compressed using FAX G4 compression, all
      other images are compressed using state-of-the-art zlib or zopfli
      compression with heurestic prediction.

      @param image Image that is added to the document
      
      @param warnings If non-zero, pointer to a QStringList where warnings will
      be stored

      @returns Error message, or empty string on success.
  **/
  QString addPages(const QImage &image, QStringList *warnings=0);
  
  /** Add JBIG2 images to the PDF document
      
      This method differs from the generic method addPages() only in the
      arguments: it expects a JBIG2Document instead of a filename.
      
      The images contained in jbig2doc will be embedded in the PDF without
      re-encoding.  The method does not check in detail if the file complies
      with the JBIG2 standard.  If invalid input data is fed into this method,
      then the resulting PDF file might possibly not comply to the PDF/A
      standard.

      @param jbig2doc Reference to a document whose images will be added to the
      PDF file

      @param warnings If non-zero, pointer to a QStringList where warnings will
      be stored

      @returns Error message, or empty string on success.
  **/
  QString addPages(const JBIG2Document &jbig2doc, QStringList *warnings=0);

  /** Add images to the PDF document

      Adds all images contained in 'imageFileName' as individual pages to the
      PDF document.  The method accepts file in JBIG2, JPEG and JPX format, and
      any other format that Qt can read.  The way that the image is encoded in
      the PDF file depends on the file type.

      - JBIG2 files will be added without re-encoding

      - JPEG files will be added without re-encoding

      - JPEG2000 files in JPX format will be added without re-encoding. 

      - JPEG2000 files in JP2 format will be converted to RGB, and encoded
        losslessly.

      - All all other file types will be converted to RGB, and encoded
        losslessly in a way that depends on the image characteristics. The
        documentation of the method addImage() explains this in detail.
      
      @warning There are two image formats associated with JPEG2000, JP2 (file
      ending = jp2) and JPX (file ending = jpf or jpx).  The PDF standard allows
      to include JPX files directly into a PDF file, while JP2 files cannot be
      included.

      If a non-empty page size has been set using the method setPageSize(), then
      the page will be of that size, and the graphics will be centered on their
      pages.  Otherwise, the page size will be chosen to fit the graphic size
      exactly.

      If preprocessed OCR data has been added to the internal HOCRDocument
      through then method appendToOCRData(), then a text overlay is generated
      from the first page of the internal HOCRDocument, and the first page is
      then deleted.  If the interal HOCRDocument is empty and the property
      autoOCR is true, then the tesseract OCR engine is run to create the data
      needed to generate a text overlay.  If autoOCR is false, no text overlay
      is generated.

      This method will never leave the PDFAWriter in any invalid state. It will
      add as many pages to the document as can be read from the file without
      errors.

      - At present, the PDF text overlay created by this method can only handle
        latin characters, or more precisely, the character set supported by the
        Windows-1252 encoding.  If '*hocrdoc' contains text with characters that
        cannot be encoded by Windows-1252, then these characters are silently
        deleted.

      - This method expects that the images retrieved vom 'imageFileName' and
        the pages from the internal HOCRDocument share the same coordinate
        system. In particular, both documents must use the same resolution.
        Information about page size and resolution are taken from
        'imageFileName' or from the properties resolutionOverrideHorizontal and
        resolutionOverrideVertical.

      The method might or might not return immediately, as most of the
      computationally intense jobs (image conversion, optical character
      recognition, compression) are run concurrently in separate worker threads.

      @param imageFileName Name of a graphics file whose images are added
      one-by-one as pages to the PDF/A document.

      @param warnings If non-zero, warnings that come up while reading the
      graphics files are added to this list.

      @returns QString::null on success, or an english, human-readble error
      message otherwise.
  **/
  QString addPages(const QString &imageFileName, QStringList *warnings=0);
  
  /** Conversion to a QByteArray containing PDF data
      
      This operator converts the document to a QByteArray holding a PDF/A file.
      This allows to write a PDFAWriter directly to a QFile, resulting in a
      valid PDF/A document on the disk.

      This method waits for all worker threads to finish and can therefore take
      considerable time. Just before returning, the signal done() is emitted.

      @return QByteArray containing the PDF
  **/
  operator QByteArray();

 public slots:
   /** Waits for all worker threads to finish 

       This method blocks until all worker slots finished execution.  While this
       method is running, the signal progress() is emitted at infrequent
       intervals.  The signal finished() is emitted before the method exits,
       even if there were no running thread at the time that the method was
       called.

       @see finished()
       @see progress(qreal)
    */
  void waitForWorkerThreads();
  
 signals:
  /** Emitted when author changes */
  void authorChanged();

  /** Emitted when keywords change */
  void keywordsChanged();

  /** Emitted when subject changes */
  void subjectChanged();

  /** Emitted when title changes */
  void titleChanged();

  /** Emitted when pageSize changes */
  void pageSizeChanged();

  /** Emitted when resolutionOverrideHorizontal changes */
  void resolutionOverrideHorizontalChanged();
  
  /** Emitted when resolutionOverrideVertical changes */
  void resolutionOverrideVerticalChanged();

  /** Emitted when autoOCR changes */
  void autoOCRChanged();

  /** Emitted when autoOCRLanguages change */
  void autoOCRLanguagesChanged();

  /** Emitted just before waitForWorkerThreads() returns
      
      This signal is emitted by the methods waitForWorkerThreads(), immediately
      before the method returns.

      @see waitForWorkerThreads()
   */
  void finished();
  
  /** Progress indicator

      This signal is emitted at irregular intervals while the method
      waitForWorkerThreads() is running, in order to provide progress
      information.

      @param percentage Number in the interval [0.0 .. 1.0] that indicates the
      fraction of PDF objects that are still being constructed by worker threads
      among all PDF objects.

      @see waitForWorkerThreads()
   */
  void progress(qreal percentage);
  
 private:
  // Meta data
  QString _author, _keywords, _subject, _title;

  // Paper size
  paperSize _pageSize;

  // HOCR Document
  HOCRDocument userSpecifiedOCRData;
  QStringList OCRLanguages;
  bool _autoOCR;
  
  // Override resolutions
  resolution horizontalResolutionOverride;
  resolution verticalResolutionOverride;
  
  // This private method adds a JBIG2 image to the PDF document. It differs from
  // the generic method addPages() only in the arguments it expects the name of
  // a JBIG file inestead of an abitrary graphics file.
  //
  // The image will be embedded in the PDF without re-encoding.  The method does
  // not check in detail if the file complies with the JBIG2 standard.  If
  // invalid input data is fed into this method, then the resulting PDF file
  // might possibly not comply to the PDF/A standard.
  QString addJBIG2(const QString &fileName, QStringList *warnings=0);

  // This private method adds a JPEG image to the PDF document. It differs from
  // the generic method addPages() only in the arguments it expects the name of
  // a JPEG file inestead of an abitrary  graphics file.
  //
  // The image will be embedded in the PDF without re-encoding.  The method does
  // not check in detail if the file complies with the JPEG standard.  If
  // invalid input data is fed into this method, then the resulting PDF file
  // might possibly not comply to the PDF/A standard.
  QString addJPEG(const QString &fileName);

  // This private method adds a JPEG2000 (ISO/IEC 15444-2) image to the PDF
  // document. The method expects a JPX or JPF file, and NOT a JP2 file.  It
  // differs from the generic method addPages() only in the arguments. It
  // expects the name of a JPEG2000 file inestead of an abitrary graphics file.
  //
  // The image will be embedded in the PDF without re-encoding.  The method does
  // not check in detail if the file complies with the JPEG standard.  If
  // invalid input data is fed into this method, then the resulting PDF file
  // might possibly not comply to the PDF/A standard.
  QString addJPX(const QString &fileName);

  // This private method adds a TIFF image to the PDF document. The method
  // exists because QImageReader cannot handle multi-page TIFF files. The method
  // reads all images contained in the file, and calls addImage() to add them to
  // the PDF
  QString addTIFF(const QString &fileName);

  // This private method is used internally to generate a page containing a
  // given graphicObject, and optionally a text overlay.  This method assumes
  // that the arguments have been checked and are correct. It also assumes that
  // the PDFAWriter has been locked for writing.
  void addGFXPage(quint32 graphicObjectIndex, const imageInfo& bInfo, const QImage& imageForOCR = QImage());

  // Lock used to provide thread-safety
  QReadWriteLock lock;
  
  // PDF protoObject. This is either a QByteArray or QFuture<QByteArray>.
  class protoObject {
  public:
    // cppcheck-suppress noExplicitConstructor
    protoObject(QByteArray _data) : data(_data) {
      ;
    };
    
    // cppcheck-suppress noExplicitConstructor
    protoObject(QFuture<QByteArray> _future) : future(_future) {
      ;
    };
    
    inline operator QByteArray() {
      if (!future.isCanceled()) {
	data = future.result();
	future = QFuture<QByteArray>();
      }
      return data;
    };
    
    QString description;
    QByteArray data;
    QFuture<QByteArray> future;
  };
  
  // List of PDF objects
  QList<protoObject> objects;

  // Index of the PDF object in the 'objects' list that contains …
  quint32 catalogObjectIndex;        // … the catalog of the PDF file
  quint32 metaDataObjectIndex;       // … the meta data
  quint32 infoObjectIndex;           // … the info object
  quint32 pageDirectoryObjectIndex;  // … the page directory
  quint32 colorProfileObjectIndex;   // … the color profile
  quint32 fontObjectIndex;           // … the font object itself

  // Use zopfli compression for bitmap graphics
  bool bestCompression;
  
  // Indices of the PDF page objects in the 'objects' list
  QList<quint32> pageIndices;

  // Reads file content into QByteArray
  static QByteArray readFile(const QString& fileName);

  // Constructs a page directory object
  QByteArray generatePageDirectoryObject() const;

  // Takes data from input, checks is zlib compression actually shrinks the
  // data, and then generates a stream object, either unencoded or zlib encoded.
  static QByteArray generateStreamObject(const QByteArray &input);

  // Returns the index of a font object for Times-Roman. Creates the object, if necessary
  quint32 getFontObjectIndex();

  // Assumes that the image is black-and-white, as returned by
  // imageOperations::optimizedFormat(), and returns a QByteArray containing a
  // PDF object containing the FAX G4 compressed image.
  static QByteArray createImageObject_bw_G4(const QImage &image);

  // Assumes that the image is bitonal, as returned by
  // imageOperations::optimizedFormat(), and returns a QByteArray containing a
  // PDF object containing the FAX G4 compressed image.
  static QByteArray createImageObject_bitonal_G4(const QImage &image);

  // Assumes that the image is grayscale, as returned by
  // imageOperations::optimizedFormat(), and returns a QByteArray containing a
  // PDF object containing the zlib/zopfli compressed image.
  static QByteArray createImageObject_gray_zlib(const QImage &image, bool bestCompression);

  // Assumes that the image has an indexed palette, as returned by
  // imageOperations::optimizedFormat(), and returns a QByteArray containing a
  // PDF object containing the zlib/zopfli compressed image.
  static QByteArray createImageObject_indexed_zlib(const QImage &image, bool bestCompression);

  // Assumes that the image is full color, as returned by
  // imageOperations::optimizedFormat(), and returns a QByteArray containing a
  // PDF object containing the zlib/zopfli compressed image.
  static QByteArray createImageObject_rgb_zlib(const QImage &image, bool bestCompression);

  // Internal method. The method takes a page content stream and generates a
  // well-compressed pageContent object, using the textBox to create a text
  // overlay.
  static QByteArray completePageContentObject_a(QByteArray contentStream, const imageInfo& bInfo, length deltaX, length deltaY, const HOCRTextBox& textBox);

  // Internal method. The method takes runs the tesseract OCR engine to create a
  // HOCRTextBox and then calls completePageContentObject_a
  static QByteArray completePageContentObject_b(QByteArray contentStream, const imageInfo& bInfo, length deltaX, length deltaY, const QImage& image, const QStringList& OCRLanguages);
};

#endif
