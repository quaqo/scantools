/*
 * Copyright © 2017--2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "paperSize.h"
#include "paperSizeTest.h"

QTEST_MAIN(paperSizeTest)


void paperSizeTest::paperSizeConstructions()
{
  paperSize s(paperSize::A4);
  QCOMPARE(s.width().get(length::mm),  210.0);
  QCOMPARE(s.height().get(length::mm), 297.0);

  s.setSize(paperSize::A5);
  QCOMPARE(s.width().get(length::mm),  148.0);
  QCOMPARE(s.height().get(length::mm), 210.0);

  s.setSize(paperSize::Letter);
  QCOMPARE(s.width().get(length::in),    8.5);
  QCOMPARE(s.height().get(length::in),  11.0);

  s.setSize(paperSize::Legal);
  QCOMPARE(s.width().get(length::in),    8.5);
  QCOMPARE(s.height().get(length::in),  14.0);

}

void paperSizeTest::stringParsing_data()
{
  QTest::addColumn<QString>("string");
  QTest::addColumn<bool>("expectedOk");
  QTest::addColumn<length>("width");
  QTest::addColumn<length>("height");

  QTest::newRow("lethal") << "lethal" << false << length(  8.5, length::in) << length(  11.0, length::in);
  QTest::newRow("leGal")  << "leGal"  << true  << length(  8.5, length::in) << length(  14.0, length::in); 
  QTest::newRow("10mm x 2.6in")  << "10mm x 2.6in"  << true  << length(10, length::mm) << length(2.6, length::in);
  QTest::newRow("10mmx2.6in")  << "10mmx2.6in"  << true  << length(10, length::mm) << length(2.6, length::in);
  QTest::newRow("10 mmx2.6in")  << "10 mmx2.6in"  << true  << length(10, length::mm) << length(2.6, length::in); 
  QTest::newRow("20x30mm")  << "20x30mm"  << true  << length(20, length::mm) << length(30, length::mm);
  QTest::newRow("20×30mm - unicode")  << "20x30mm"  << true  << length(20, length::mm) << length(30, length::mm); 
}


void paperSizeTest::stringParsing()
{
  QFETCH(QString, string);
  QFETCH(bool, expectedOk);
  QFETCH(length, width);
  QFETCH(length, height);

  paperSize s;
  bool ok = s.setSize(string);
  QCOMPARE(ok, expectedOk);
  if (ok) {
    QCOMPARE(s.width(), width);
    QCOMPARE(s.height(), height);
  }
}
