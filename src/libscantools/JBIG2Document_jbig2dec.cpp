/*
 * Copyright © 2018 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "JBIG2Document.h"
#include "scantools.h"

#include <QDebug>
#include <cstddef>
#include <cstdint>
#include <jbig2.h>



void JBIG2Document::myImageCleanupHandler(void *info)
{
    delete[] static_cast<uchar*>(info);
}


auto JBIG2Document::operator[](quint32 pageNumber) const -> QImage
{
    // Create global decoder context, holding the global data
    auto* auxiliary_ctx = jbig2_ctx_new(nullptr, JBIG2_OPTIONS_EMBEDDED, nullptr, nullptr, nullptr);
    if (auxiliary_ctx == nullptr) {
        return QImage();
    }
    QByteArray globalData = getPDFDataChunk(0);
    if (jbig2_data_in(auxiliary_ctx, reinterpret_cast<const unsigned char *>(globalData.constData()), globalData.size()) != 0) {
        return QImage();
    }
    auto* global_ctx = jbig2_make_global_ctx(auxiliary_ctx);
    if (global_ctx == nullptr) {
        return QImage();
    }

    // Create local decoder context, holding the page data
    auto* ctx = jbig2_ctx_new(nullptr, JBIG2_OPTIONS_EMBEDDED, global_ctx, nullptr, nullptr);
    if (ctx == nullptr) {
        return QImage();
    }
    QByteArray localData = getPDFDataChunk(pageNumber);
    if (jbig2_data_in(ctx, reinterpret_cast<const unsigned char *>(localData.constData()), localData.size()) != 0) {
        return QImage();
    }
    if (jbig2_complete_page(ctx) != 0) {
        return QImage();
    }
    jbig2_global_ctx_free(global_ctx);

    auto* jb2img = jbig2_page_out(ctx);
    if (jb2img == nullptr) {
        qWarning() << "No image" << pageNumber;
        jbig2_ctx_free(ctx);
        return QImage();
    }


    // Interpret results
    auto* data = new uchar[jb2img->height*jb2img->stride];
    memcpy(data, jb2img->data, jb2img->height*jb2img->stride);
    auto img = QImage(data, jb2img->width, jb2img->height, jb2img->stride, QImage::Format_Mono, myImageCleanupHandler, data);
    jbig2_release_page(ctx, jb2img);
    jbig2_ctx_free(ctx);

    // Set colors correctly
    img.setColorCount(2);
    img.setColor(0, 0xFFFFFFFF);
    img.setColor(1, 0xFF000000);

    // Set resolution correctly
    auto info = pageInfo(pageNumber);
    img.setDotsPerMeterX(info.xResolution().get(resolution::dpm));
    img.setDotsPerMeterY(info.yResolution().get(resolution::dpm));

    return img;
}
