/*
 * Copyright © 2017--2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "imageInfo.h"
#include "imageInfoTest.h"

QTEST_MAIN(imageInfoTest)


void imageInfoTest::constructor_data()
{
 QTest::addColumn<QString>("path");
 QTest::addColumn<quint32>("width");
 QTest::addColumn<quint32>("height");
 QTest::addColumn<quint32>("xResolutionDPM");


 QString jb2File = QFINDTESTDATA("testData/images/JB2/Manuscript.jb2");
 QTest::newRow(jb2File.toLocal8Bit().constData()) << jb2File << (quint32)4963 << (quint32)6569 << (quint32)0;

 QFileInfoList files;
 files += QDir(QFINDTESTDATA("testData/images/JPG")).entryInfoList( QStringList() << "*.jpg");
 files += QDir(QFINDTESTDATA("testData/images/JP2")).entryInfoList( QStringList() << "*.jp2");
 files += QDir(QFINDTESTDATA("testData/images/PNG")).entryInfoList( QStringList() << "*.png");
 files += QDir(QFINDTESTDATA("testData/images/TIFF")).entryInfoList( QStringList() << "*.tif");
 foreach(const QFileInfo &file, files) {
   QImage image(file.absoluteFilePath());
   QTest::newRow(file.absoluteFilePath().toLocal8Bit().constData())
     << file.absoluteFilePath()
     << (quint32)image.width()
     << (quint32)image.height()
     << (quint32)image.dotsPerMeterX();
 }

}

void imageInfoTest::constructor()
{
  QFETCH(QString, path);
  QFETCH(quint32, width);
  QFETCH(quint32, height);
  QFETCH(quint32, xResolutionDPM);
  
  imageInfo imgInfo(path);
  QCOMPARE(width, (quint32)imgInfo.widthInPixel);
  QCOMPARE(height, (quint32)imgInfo.heightInPixel);
  QCOMPARE(xResolutionDPM, (quint32)(imgInfo.xResolution().get(resolution::dpm)));
}

void imageInfoTest::readAll()
{
  QList<imageInfo> infos;

  infos = imageInfo::readAll(QFINDTESTDATA("testData/images/JB2/Manuscript.jb2"));
  QCOMPARE(infos.size(), 8);
  foreach(imageInfo info, infos) {
    QCOMPARE(info.widthInPixel,  (quint32)4963);
    QCOMPARE(info.heightInPixel, (quint32)6569);
    QCOMPARE(info.xResolution().get(resolution::dpi), 0.0);
  }
  
  infos = imageInfo::readAll(QFINDTESTDATA("testData/images/TIFF/Manuscript-1.tif"));
  QCOMPARE(infos.size(), 8);
  foreach(imageInfo info, infos) {
    QCOMPARE(info.widthInPixel,  (quint32)4963);
    QCOMPARE(info.heightInPixel, (quint32)6569);
    QCOMPARE(qRound(info.xResolution().get(resolution::dpi)), qRound(600.0));
  }

  infos = imageInfo::readAll(QFINDTESTDATA("testData/images/PNG/Hydrogenea.png"));
  QCOMPARE(infos.size(), 1);
  foreach(imageInfo info, infos) {
    QCOMPARE(info.widthInPixel,  (quint32)500);
    QCOMPARE(info.heightInPixel, (quint32)633);
    QCOMPARE(qRound(info.xResolution().get(resolution::dpi)), qRound(72.0));
  }
}
