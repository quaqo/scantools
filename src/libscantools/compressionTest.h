/*
 * Copyright © 2016-2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QTest>

class compressionTest: public QObject
{
  Q_OBJECT
  
  public:
    // Uses undocumented TIFF FAX encoder
    QByteArray referenceFAXEncoder(const QImage &image);

    // Analyses errors in FAX compression
    bool bisectAndAnalyseFAXCompressionError(QImage &image);

  private slots:
    void faxG4Compression_data();
    void faxG4Compression();
    void zlibCompression_data();
    void zlibCompression();
    void zopfliCompression_data();
    void zopfliCompression();
};
