/*
 * Copyright © 2017 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef LENGTH
#define LENGTH

#include <QMetaType>
#include <QString>
#include <QtGlobal>


/** The length stores a length and converts between units

    This is a trivial class that helps with length storage and
    conversions. Lengthes are stored intenally in 1/360mm, which allows
    conversions between units without rounding error.

    The class "length" is known to the QMetaType systen and can be used as a
    custom type in QVariant.
*/

class length
{
public:
    /** List of supported units */
    enum unit {
        cm,    /**< Centimeter */
        in,    /**< Inch */
        mm,    /**< Millimeter */
        pt     /**< Points (=1/72 inch) */
    };

    /** Constructs a zero length */
    length() : _length(0) {

    }

    /** Constructs length of given value and unit

      @param l Scalar value of the length

      @param u Unit for the length
  */
    length(qreal l, unit u) {
        set(l, u);
    }

    /** Constructs length from a string

      This is a convenience method that calls set(QString) internally.

      @param lstring A string of the form "<number> unit", such as "12.3 mm".

      @param ok If the string is not recoginzed *ok is set to false and the
      paper size if empty. Otherwise, *ok is set to true.
  */
    explicit length(QString lstring, bool *ok=0) {
        bool myOk = set(lstring);
        if (ok)
            *ok = myOk;
    }

    /** Returns length in given unit
      
      @param u Unit

      @return Value for length in the given unit
   */
    qreal get(unit u) const;

    /** Sets length

      @param l Scalar value of the length

      @param u Unit for the length
  */
    void set(qreal l, unit u);

    /** Constructs length from a string

      @param lstring A string of the form "<number> unit", such as "12.3 mm".

      @returns If a conversion error occurs, false is returned and the length is
      undefined. Otherwise, the method returns true.
  */
    bool set(QString lstring);

    /** Check if length is positive
      
      @returns True if length is zero or less */
    bool isNonPositive() const {
        return (_length <= 0);
    }

    /** Convert to human-readable string

      @returns Length in millimeter as a human-readable string
  */
    operator QString() const {
        return QString("%1mm").arg(get(mm));
    }

    /** Checks for equality

      @param other Other length to check against

      @returns True if lengths are equal
  **/
    bool operator==(const length other) const
    {
        return _length == other._length;
    }

    /** Sum of the lengths

      @param rhs Length to add

      @return Sum of the lengths
  **/
    const length operator+(const length rhs) const
    {
        length result;
        result._length = _length + rhs._length;
        return result;
    }

    /** Difference of two lengths

      @param rhs Length to subtract from this length

      @return Difference of the lengths
  **/
    const length operator-(const length rhs) const
    {
        length result;
        result._length = _length - rhs._length;
        return result;
    }

    /** Divide a length by a number

      @param div Scalar to divide this length by

      @returns Divided length
   **/
    const length operator/(const qreal div) const
    {
        length result;
        result._length = _length/div;
        return result;
    }

private:
    // Length in 1/360 mm
    qreal _length;
};

// Makes "length" known to QMetaType
Q_DECLARE_METATYPE(length)

#endif
