/*
 * Copyright © 2017--2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QStringList>

#include "paperSize.h"

struct paperSizeEntry {const char *txt; paperSize::format f; length::unit u; qreal width; qreal height;};

static std::vector<paperSizeEntry> sizeTable = {
  {"a4",     paperSize::A4,     length::mm, 210.0, 297.0},
  {"a5",     paperSize::A5,     length::mm, 148.0, 210.0},
  {"empty",  paperSize::empty,  length::mm,   0.0,   0.0},
  {"letter", paperSize::Letter, length::in,   8.5,  11.0},
  {"legal",  paperSize::Legal,  length::in,   8.5,  14.0},
  {nullptr,        paperSize::A4,     length::pt,   0.0,   0.0}
};


void paperSize::setSize(format f)
{
  for(int i=0; sizeTable[i].txt != nullptr; i++) 
    if (sizeTable[i].f == f) {
      _width  = length(sizeTable[i].width, sizeTable[i].u);
      _height = length(sizeTable[i].height, sizeTable[i].u);
      return;
    }
  }


bool paperSize::setSize(QString formatString)
{
  _width  = length(0, length::mm);
  _height = length(0, length::mm);
  
  formatString = formatString.simplified().toLower();

  // Check if the formatString describes one of the known formats, such as "a4"
  // or "letter"
  for(int i=0; sizeTable[i].txt != nullptr; i++) 
    if (formatString == QString(sizeTable[i].txt)) {
      _width  = length(sizeTable[i].width, sizeTable[i].u);
      _height = length(sizeTable[i].height, sizeTable[i].u);
      return true;;
    }

  // If not, then the formatString should contain either "x" or "×", and the
  // string will be split accordingly
  QStringList strings;
  if (formatString.contains("x")) 
    strings = formatString.split("x");
  else if (formatString.contains("×")) 
    strings = formatString.split("×");
  else
    return false;
  
  if (strings.size() != 2) 
    return false;

  strings[0] = strings[0].simplified();
  strings[1] = strings[1].simplified();
  
  if (!_height.set(strings[1])) 
    return false;
  
  if (!_width.set(strings[0])) 
    if (!_width.set(strings[0]+strings[1].right(2))) 
      return false;
  
  return true;
}
