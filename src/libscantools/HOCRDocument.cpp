/*
         * Copyright © 2016-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
         *
         * This program is free software: you can redistribute it and/or modify it under
         * the terms of the GNU General Public License as published by the Free Software
         * Foundation, either version 3 of the License, or (at your option) any later
         * version.
         *
         * This program is distributed in the hope that it will be useful, but WITHOUT
         * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
         * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
         * details.
         *
         * You should have received a copy of the GNU General Public License along with
         * this program.  If not, see <http://www.gnu.org/licenses/>.
         */

#include "HOCRDocument.h"
#include "scantools.h"

#include <QDebug>
#include <QFile>
#include <QPdfWriter>
#include <QtConcurrent>
#include <functional>

void HOCRDocument::clear()
{
    _error = QString();
    _OCRSystem.clear();
    _OCRCapabilities.clear();
    _warnings.clear();
}


auto HOCRDocument::hasText() const -> bool
{
    // If error condition is set, then document has no text.
    if (hasError()) {
        return false;
    }

    // Go through any of the pages and check if they have text.
    for(const auto & _page : _pages) {
        if (_page.hasText()) {
            return true;
        }
    }

    // No text found? Then we have no text.
    return false;
}


void HOCRDocument::read(QIODevice *device)
{
    // Clear document
    clear();

    // Paranoid safety checks
    if (device == nullptr) {
        _error = QString("Internal error. HOCRDocument::HOCRDocument(QIODevice *device) called with device == 0.");
        return;
    }

    // Open XML document
    QXmlStreamReader xml(device);

    // Main loop. Read all elements of the XML document.
    while (!xml.atEnd()) {
        // Ignore everything, except start elements
        if (xml.readNext() != QXmlStreamReader::StartElement) {
            continue;
        }

        // Start element found. Get attributes
        QXmlStreamAttributes atts = xml.attributes();

        // Start element describes the OCR system that generated this file
        if (atts.value("name").contains("ocr-system")) {
            _OCRSystem << atts.value("content").toString().simplified();
            continue;
        }

        // Start element describes the structural elements that are used in this
        // file
        if (atts.value("name").contains("ocr-capabilities")) {
            _OCRCapabilities =  QSet<QString>::fromList(atts.value("content").toString().simplified().split(" "));
            continue;
        }

        // Start element describes something, but we do not know what. Create a
        // warning.
        if (atts.value("name").startsWith("ocr-"))
        {
            _warnings << QString("Line %1, column %2. Unrecognized OCR attribute: '%3'. "
                                 "Please report this, so we can extend scantools appropriately. "
                                 "Please include a sample file in your report.").arg(xml.lineNumber()).arg(xml.columnNumber()).arg(atts.value("name").toString());
            continue;
        }

        // Start element begins a page. Read that page
        if (atts.value("class").contains("ocr_page")) {
            _pages.append(HOCRTextBox(xml, _warnings));
            continue;
        }
    }

    // Check if there has been an error reading the XML file
    if (xml.hasError()) {
        clear();
        _error = QString("Error interpreting file. Problem in line %1, column %2. %3").arg(xml.lineNumber()).arg(xml.columnNumber()).arg(xml.errorString());
        return;
    }
}


void HOCRDocument::read(const QString& fileName)
{
    // Clear document
    clear();

    // Open file
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        _error = QString("Cannot open file '%1'.").arg(fileName);
        return;
    }

    // Read
    read(&file);
}


auto HOCRDocument::suggestFont() const -> QFont
{
    // Paranoid safety checks
    if (hasError()) {
        qWarning() << "Internal error: HOCRDocument::suggestFont() called, but HOCRDocument has error condition.";
        return QFont();
    }

    // List of fonts to try
    QStringList fontNames;
    fontNames << "Helvetica" << "Times" << "Courier";

    // Try every font from the list
    QVector<qint64> fits;
    foreach(auto fontName, fontNames) {
        QFont font(fontName);

        qint64 fit = 0;
        foreach(auto page, _pages)
            fit += page.estimateFit(font);

        fits << fit;
    }

    // Find best fitting font
    int bestFontIndex = 0;
    int bestFontFit = fits[0];
    for(int i=1; i<fits.size(); i++) {
        if (fits[i] < bestFontFit) {
            bestFontIndex = i;
            bestFontFit = fits[i];
        }
    }

    // Return result
    return QFont(fontNames[bestFontIndex]);
}


auto HOCRDocument::findPageSize(int pageNumber, resolution _resolution, const QPageSize &overridePageSize) const -> QPageSize
{
    // Paranoid safety checks
    if (pageNumber >= _pages.size()) {
        qWarning() << "Internal error: HOCRDocument::findPageSize called, with pageNumber larger than the actual number of pages.";
        return QPageSize();
    }

    if (overridePageSize.isValid()) {
        return overridePageSize;
    }

    return QPageSize(QSizeF(_pages[pageNumber].boundingBox().width()/_resolution.get(resolution::dpi), _pages[pageNumber].boundingBox().height()/_resolution.get(resolution::dpi)), QPageSize::Inch);
}


auto HOCRDocument::toPDF(const QString& fileName, resolution _resolution, const QString& title, const QPageSize& overridePageSize, QFont *overrideFont) const -> QString
{
    // Paranoid safety checks
    if (hasError()) {
        QString msg = "Internal error: HOCRDocument::toPDF() called, but HOCRDocument has error condition.";
        qWarning() << msg;
        return msg;
    }
    if (!_resolution.isValid()) {
        return QString("Value %1 for resolution is out of range. Needs to be zero, or in range [%2..%3].").arg(_resolution).arg(resolution::minResDPI).arg(resolution::maxResDPI);
    }

    // Construct a PDF writer, set some metadata
    QPdfWriter pdf(fileName);
    pdf.setCreator(QString("scantools %1").arg(scantools::versionString));
    if (!title.isEmpty()) {
        pdf.setTitle(title);
    }
    pdf.setResolution(_resolution.get(resolution::dpi));

    // Set the page size on the PDF writer.  Regretfully, this is a little
    // complicated. According to the Qt documentation, the page size must be
    // called before the painter begins painting, and again immediately before
    // every call of newPage(). Therefore, the same code line is found again
    // below.
    if (!_pages.empty()) {
        pdf.setPageSize(findPageSize(0, _resolution, overridePageSize));
    }
    pdf.setPageMargins(QMarginsF(0,0,0,0));

    // Construct a painter and set the font
    QPainter painter(&pdf);

    if (overrideFont == nullptr) {
        painter.setFont(suggestFont());
    } else {
        painter.setFont(*overrideFont);
    }

    for(int pageNumber=0; pageNumber<_pages.size(); pageNumber++) {
        // If we are rendering a page other than the first page, need to call
        // newPage() first. Right before that, the page size for the coming page
        // must be set.
        if (pageNumber > 0) {
            pdf.setPageSize(findPageSize(pageNumber, _resolution, overridePageSize));
            pdf.setPageMargins(QMarginsF(0,0,0,0));
            pdf.newPage();
        }

        // Now render.
        _pages[pageNumber].render(painter);
    }

    return QString();
}


auto HOCRDocument::toImages(QFont *overrideFont, QImage::Format format) const -> QList<QImage>
{
    // Paranoid safety checks
    if (hasError()) {
        qWarning() <<  "Internal error: HOCRDocument::toImages() called, but HOCRDocument has error condition.";
        return QList<QImage>();
    }

    // Suggest a font, if need be
    QFont suggestedFont;
    if (overrideFont == nullptr) {
        suggestedFont = suggestFont();
    } else {
        suggestedFont = *overrideFont;
    }

    // Now generate images.
    return QtConcurrent::blockingMapped(_pages, std::bind(&HOCRTextBox::toImage, std::placeholders::_1, suggestedFont, format));
}


auto HOCRDocument::toText() const -> QString
{
    // Paranoid safety checks
    if (hasError()) {
        qWarning() <<  "Internal error: HOCRDocument::toText() called, but HOCRDocument has error condition.";
        return QString();
    }

    // Now generate text.
    QString result;
    for(const auto & _page : _pages) {
        result += _page.toText();
    }

    return result;
}


void HOCRDocument::append(const HOCRDocument &other)
{
    // Paranoid safety checks
    if (hasError()) {
        qWarning() << "Internal error: HOCRDocument::append() called, but this HOCRDocument has error condition.";
    }
    if (other.hasError()) {
        qWarning() << "Internal error: HOCRDocument::append() called, but other HOCRDocument has error condition.";
    }

    // Don't do anything if the other document is empty
    if (other._pages.isEmpty()) {
        return;
    }

    // Join _OCRSystem
    _OCRSystem += other._OCRSystem;

    // Join _OCRCapabilities
    _OCRCapabilities +=  other._OCRCapabilities;

    // Join _pages
    _pages += other._pages;

    // Join _warnings
    _warnings += other._warnings;
}
