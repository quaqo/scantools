/*
 * Copyright © 2016--2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "JBIG2Document.h"

#include <QDebug>
#include <QFile>

auto JBIG2Document::pageNumbers() const -> QList<quint32>
{
    QList<quint32> numbers;

    QListIterator<JBIG2Segment> i(segments);
    while (i.hasNext()) {
        quint32 number = i.next().pageAssociation();
        if ((number == 0) || (numbers.contains(number))) {
            continue;
        }
        numbers += number;
    }
    std::sort(numbers.begin(), numbers.end());

    return numbers;
}


auto JBIG2Document::getPDFDataChunk(quint32 pageNumber) const -> QByteArray
{
    // Paranoid safety checks
    if (hasError()) {
        qWarning() << "Internal error: JBIG2Document::getPDFDataChunk() called, but JBIG2Document has error condition.";
        return QByteArray();
    }

    QByteArray result;
    for(const auto & i : segments) {
        if ((i.pageAssociation() == pageNumber) && (i.type() != 49)) {
            JBIG2Segment segment = i;
            segment.setPageAssociation();
            result += segment;
        }
    }

    return result;
}


auto JBIG2Document::pageInfo(quint32 pageNumber) const -> imageInfo
{
    // Paranoid safety checks
    if (hasError()) {
        qWarning() << "Internal error: JBIG2Document::pageInfo() called, but JBIG2Document has error condition.";
        return imageInfo();
    }

    QByteArray data;
    for(const auto & segment : segments) {
        if ((segment.pageAssociation() == pageNumber) && (segment.type() == 48)) {
            data = segment.data();
            break;
        }
    }

    if (data.isEmpty()) {
        return imageInfo();
    }

    imageInfo bi;
    QDataStream stream(data);

    quint32 xResolutionInDPM = 0;
    quint32 yResolutionInDPM = 0;

    stream >> bi.widthInPixel >> bi.heightInPixel >> xResolutionInDPM >> yResolutionInDPM;
    if (stream.status() != 0) {
        return imageInfo();
    }

    bi._xResolution.set(xResolutionInDPM, resolution::dpm);
    bi._yResolution.set(xResolutionInDPM, resolution::dpm);

    return bi;
}


auto JBIG2Document::info() const -> QString
{
    QString result;

    result += "Error condition: " + _error + "\n\n";
    result += "Warnings:\n";
    foreach (const QString &warning, _warnings)
        result += warning+"\n";
    result += "\n\nSegment info:\n\n";
    for(const auto & segment : segments) {
        result += segment.info() + "\n";
    }

    return result;
}


void JBIG2Document::check_n_fix()
{
    for(auto & segment : segments) {
        quint32 segNumber = segment.number();
        if (!segment.retainbitForThisSegment()) {
            QList<quint32> refs = referrers(segNumber);
            if (!refs.isEmpty()) {
                _warnings += QString("Segment %1 refers to segment %2, but in segment %2 the 'retain bit for this segment' is set "
                                     "to 'false'. See Section 7.2.4 of the JBIG2 specification, ISO/IEC 14492.").arg(refs[0]).arg(segNumber);
                segment.setRetainbitForThisSegment();
            }
        }
    }

}


auto JBIG2Document::referrers(quint32 segmentNumber) -> QList<quint32>
{
    QList<quint32> result;

    for(auto & segment : segments) {
        if (segment.refersTo().contains(segmentNumber)) {
            result += segment.number();
        }
    }
    return result;
}


void JBIG2Document::clear()
{
    _warnings.clear();
    segments.clear();
    _error = QString();
}


void JBIG2Document::read(const QString& fileName)
{
    clear();
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        _error = QString("Cannot open file '%1'.").arg(fileName);
        return;
    }
    read(&file);
}


void JBIG2Document::read(QIODevice *device)
{
    clear();

    // Paranoid safety checks
    if (device == nullptr) {
        _error = QString("Internal error. JBIG2Document::JBIG2Document(QIODevice *device) called with device == 0.");
        return;
    }

    // Open data stream
    QDataStream stream(device);

    // Read file header
    quint64 ID = 0;
    quint8 fileHeaderFlags = 0;
    quint32 numberOfPages = 0;

    stream >> ID >> fileHeaderFlags;
    if (ID != 0x974A42320D0A1A0A) {
        _error = QString("Data is not in JBIG2 format, ID is invalid.");
        return;
    }
    if ((fileHeaderFlags & 2) == 0) {
        stream >> numberOfPages;
    }
    if (stream.status() != 0) {
        _error = QString("Error reading data: %1.").arg(device->errorString());
        return;
    }

    if ((fileHeaderFlags & 1) != 0) {
        // File is in sequential organisation. Every segment header is immediately
        // followed by its data.
        while(!device->atEnd()) {
            JBIG2Segment segment;
            _error = segment.readHeader(device);
            if (!_error.isEmpty()) {
                segments.clear();
                return;
            }
            _error = segment.readData(device);
            if (!_error.isEmpty()) {
                segments.clear();
                return;
            }
            segments += segment;
        }
    } else {
        // File is in random access organisation. Read segment headers first, and
        // then the segement data.
        while(true) {
            JBIG2Segment segment;
            _error = segment.readHeader(device);
            if (!_error.isEmpty()) {
                segments.clear();
                return;
            }
            segments += segment;
            if (segment.type() == 51) {
                break;
            }
        }
        for(int i=0; i<segments.size(); i++) {
            _error = segments[i].readData(device);
            if (!_error.isEmpty()) {
                segments.clear();
                return;
            }
        }
    }

    check_n_fix();
}
