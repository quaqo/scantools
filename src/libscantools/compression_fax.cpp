/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bitVector.h"
#include "compression.h"

/*
 * miniBitVector implementing code words from the FAX G4 specification
 */

static bitVector::miniBitVector EOFB       = {24, 0b000000000001000000000001};
static bitVector::miniBitVector HOR        = { 3, 0b001};
static bitVector::miniBitVector makeUp2560 = {12, 0b000000011111};
static bitVector::miniBitVector PASS       = { 4, 0b0001};
static bitVector::miniBitVector V0         = { 1, 0b1};	
static bitVector::miniBitVector VR1        = { 3, 0b011};	
static bitVector::miniBitVector VR2        = { 6, 0b000011};	
static bitVector::miniBitVector VR3        = { 7, 0b0000011};	
static bitVector::miniBitVector VL1        = { 3, 0b010};	
static bitVector::miniBitVector VL2        = { 6, 0b000010};	
static bitVector::miniBitVector VL3        = { 7, 0b0000010};	

//static QVector<bitVector::miniBitVector> terminatingCodesWhite =
QVector<bitVector::miniBitVector> terminatingCodesWhite =
{
  { 8, 0b00110101},
  { 6, 0b000111},
  { 4, 0b0111},
  { 4, 0b1000},
  { 4, 0b1011},
  { 4, 0b1100},
  { 4, 0b1110},
  { 4, 0b1111},
  { 5, 0b10011},
  { 5, 0b10100},
  { 5, 0b00111},
  { 5, 0b01000},
  { 6, 0b001000},
  { 6, 0b000011},
  { 6, 0b110100},
  { 6, 0b110101},
  { 6, 0b101010},
  { 6, 0b101011},
  { 7, 0b0100111},
  { 7, 0b0001100},
  { 7, 0b0001000},
  { 7, 0b0010111},
  { 7, 0b0000011},
  { 7, 0b0000100},
  { 7, 0b0101000},
  { 7, 0b0101011},
  { 7, 0b0010011},
  { 7, 0b0100100},
  { 7, 0b0011000},
  { 8, 0b00000010},
  { 8, 0b00000011},
  { 8, 0b00011010},
  { 8, 0b00011011},
  { 8, 0b00010010},
  { 8, 0b00010011},
  { 8, 0b00010100},
  { 8, 0b00010101},
  { 8, 0b00010110},
  { 8, 0b00010111},
  { 8, 0b00101000},
  { 8, 0b00101001},
  { 8, 0b00101010},
  { 8, 0b00101011},
  { 8, 0b00101100},
  { 8, 0b00101101},
  { 8, 0b00000100},
  { 8, 0b00000101},
  { 8, 0b00001010},
  { 8, 0b00001011},
  { 8, 0b01010010},
  { 8, 0b01010011},
  { 8, 0b01010100},
  { 8, 0b01010101},
  { 8, 0b00100100},
  { 8, 0b00100101},
  { 8, 0b01011000},
  { 8, 0b01011001},
  { 8, 0b01011010},
  { 8, 0b01011011},
  { 8, 0b01001010},
  { 8, 0b01001011},
  { 8, 0b00110010},
  { 8, 0b00110011},
  { 8, 0b00110100}
};

static QVector<bitVector::miniBitVector> terminatingCodesBlack = {
  {10, 0b0000110111},
  { 3, 0b010},
  { 2, 0b11},
  { 2, 0b10},
  { 3, 0b011},
  { 4, 0b0011},
  { 4, 0b0010},
  { 5, 0b00011},
  { 6, 0b000101},
  { 6, 0b000100},
  { 7, 0b0000100},
  { 7, 0b0000101},
  { 7, 0b0000111},
  { 8, 0b00000100},
  { 8, 0b00000111},
  { 9, 0b000011000},
  {10, 0b0000010111},
  {10, 0b0000011000},
  {10, 0b0000001000},
  {11, 0b00001100111},
  {11, 0b00001101000},
  {11, 0b00001101100},
  {11, 0b00000110111},
  {11, 0b00000101000},
  {11, 0b00000010111},
  {11, 0b00000011000},
  {12, 0b000011001010},
  {12, 0b000011001011},
  {12, 0b000011001100},
  {12, 0b000011001101},
  {12, 0b000001101000},
  {12, 0b000001101001},
  {12, 0b000001101010},
  {12, 0b000001101011},
  {12, 0b000011010010},
  {12, 0b000011010011},
  {12, 0b000011010100},
  {12, 0b000011010101},
  {12, 0b000011010110},
  {12, 0b000011010111},
  {12, 0b000001101100},
  {12, 0b000001101101},
  {12, 0b000011011010},
  {12, 0b000011011011},
  {12, 0b000001010100},
  {12, 0b000001010101},
  {12, 0b000001010110},
  {12, 0b000001010111},
  {12, 0b000001100100},
  {12, 0b000001100101},
  {12, 0b000001010010},
  {12, 0b000001010011},
  {12, 0b000000100100},
  {12, 0b000000110111},
  {12, 0b000000111000},
  {12, 0b000000100111},
  {12, 0b000000101000},
  {12, 0b000001011000},
  {12, 0b000001011001},
  {12, 0b000000101011},
  {12, 0b000000101100},
  {12, 0b000001011010},
  {12, 0b000001100110},
  {12, 0b000001100111}
};

static QVector<bitVector::miniBitVector> makeUpCodesWhite = {
  { 5, 0b11011},
  { 5, 0b10010},
  { 6, 0b010111},
  { 7, 0b0110111},
  { 8, 0b00110110},
  { 8, 0b00110111},
  { 8, 0b01100100},
  { 8, 0b01100101},
  { 8, 0b01101000},
  { 8, 0b01100111},
  { 9, 0b011001100},
  { 9, 0b011001101},
  { 9, 0b011010010},
  { 9, 0b011010011},
  { 9, 0b011010100},
  { 9, 0b011010101},
  { 9, 0b011010110},
  { 9, 0b011010111},
  { 9, 0b011011000},
  { 9, 0b011011001},
  { 9, 0b011011010},
  { 9, 0b011011011},
  { 9, 0b010011000},
  { 9, 0b010011001},
  { 9, 0b010011010},
  { 6, 0b011000},
  { 9, 0b010011011}
};

static QVector<bitVector::miniBitVector> makeUpCodesBlack = {
  {10, 0b0000001111},
  {12, 0b000011001000},
  {12, 0b000011001001},
  {12, 0b000001011011},
  {12, 0b000000110011},
  {12, 0b000000110100},
  {12, 0b000000110101},
  {13, 0b0000001101100},
  {13, 0b0000001101101},
  {13, 0b0000001001010},
  {13, 0b0000001001011},
  {13, 0b0000001001100},
  {13, 0b0000001001101},
  {13, 0b0000001110010},
  {13, 0b0000001110011},
  {13, 0b0000001110100},
  {13, 0b0000001110101},
  {13, 0b0000001110110},
  {13, 0b0000001110111},
  {13, 0b0000001010010},
  {13, 0b0000001010011},
  {13, 0b0000001010100},
  {13, 0b0000001010101},
  {13, 0b0000001011010},
  {13, 0b0000001011011},
  {13, 0b0000001100100},
  {13, 0b0000001100101}
};

static QVector<bitVector::miniBitVector> makeUpCodesLarge = {
  {11, 0b00000001000},
  {11, 0b00000001100},
  {11, 0b00000001101},
  {12, 0b000000010010},
  {12, 0b000000010011},
  {12, 0b000000010100},
  {12, 0b000000010101},
  {12, 0b000000010110},
  {12, 0b000000010111},
  {12, 0b000000011100},
  {12, 0b000000011101},
  {12, 0b000000011110},
  {12, 0b000000011111}
};


/*
 * Auxilary functions used by encode and decoder. 
 *
 */

// Append code words for a 'run' black/white pixels of given size 'runSize' to
// the end of the bitArray

static void addRun(int runSize, quint8 runValue, bitVector &result)
{
  // For run sizes >= 2560 bits use the makeup code for 2560 repeatedly
  while(runSize >= 2560) {
    result.append(makeUp2560);
    runSize -= 2560;
  }
	
  // Next, for run sizes >= 1792 bits, use the makeup code for long runs,
  // otherwise use short markup codes that depend on values
  if (runSize >= 1792) 
    result.append(makeUpCodesLarge[(runSize-1792)/64]);
  else
    if (runSize >= 64) { 
      int makeUpCode = (runSize-64)/64;
      if (runValue == 0) 
	result.append(makeUpCodesWhite[makeUpCode]);
      else 
	result.append(makeUpCodesBlack[makeUpCode]);
    }
  
  // Next, write the terminating code
  int terminatingCode = runSize%64;
  if (runValue == 0) 
    result.append(terminatingCodesWhite[terminatingCode]);
  else 
    result.append(terminatingCodesBlack[terminatingCode]);
}


// Find value for b1. After this, b1 will either be equal to width or point to
// the first bit in the reference line right of a0 whose value is unequal to
// a0Value, and whose preceding value is equal to a0Value

static inline int findB1(const int &a0, const quint8 &a0Value, const quint8 *referenceLine, int width)
{
  if ((a0 == -1) && ((*referenceLine & 0b10000000) != 0))
    return 0;
  
  int tmp = bitVector::findEndOfRun(referenceLine, (a0 == -1) ? 0 : a0, width, 1-a0Value);
  return bitVector::findEndOfRun(referenceLine, tmp, width, a0Value);
}


// Read the size of a run from the bitVector 'bits', starting a bit given by
// bitPointer, and increase bit pointer.

static int getRunLength(const bitVector &bits, int &bitPointer, quint8 value)
{
  int runLength = 0;
  
  // Read (possibly repeated makeup code for 2056
  while (bits.startsWith(makeUp2560, bitPointer)) {
    runLength  += 2560;
    bitPointer += makeUp2560.numBits;
  }
  
  // Read makeup code for large runs
  for(int i=0; i<13; i++)
    if (bits.startsWith(makeUpCodesLarge[i], bitPointer)) {
      runLength  += 1792 + i*64;
      bitPointer += makeUpCodesLarge[i].numBits;
      break;
    }
  
  // Read makeup code for medium-size runs. These are separated by color.
  if (value == 0) {
    for(int i=0; i<27; i++) {
      if (bits.startsWith(makeUpCodesWhite[i], bitPointer)) {
	runLength  += 64 + i*64;
	bitPointer += makeUpCodesWhite[i].numBits;
	break;
      }
    }
  } else {
    for(int i=0; i<27; i++)
      if (bits.startsWith(makeUpCodesBlack[i], bitPointer)) {
	runLength  += 64 + i*64;
	bitPointer += makeUpCodesBlack[i].numBits;
	break;
      }
  }
  
  // Read terminating code
  bool haveTerminatingCode = false;
  if (value == 0) {
    for(int i=0; i<64; i++)
      if (bits.startsWith(terminatingCodesWhite[i], bitPointer)) {
	runLength  += i;
	bitPointer += terminatingCodesWhite[i].numBits;
	haveTerminatingCode = true;
	break;
      }
  } else {
    for(int i=0; i<64; i++)
      if (bits.startsWith(terminatingCodesBlack[i], bitPointer)) {
	runLength  += i;
	bitPointer += terminatingCodesBlack[i].numBits;
	haveTerminatingCode = true;
	break;
      }
  }
  
  if (!haveTerminatingCode)
    return -1;
  
  return runLength;
}


/*
 * FAX G4 encoder method
 *
 */

QByteArray compression::faxG4Encode(const QImage &image)
{
  // Paranoid safety checks
  if (image.isNull())
    return QByteArray();
  
  // Convert input image to QImage::Format_Mono
  QImage myImage;
  if (image.format() != QImage::Format_Mono) 
    myImage = image.convertToFormat(QImage::Format_Mono);
  else
    myImage = image;
  
  // Construct bit vector that will eventually hold the result
  bitVector result(0);
  
  // Go through all scanlines
  QByteArray whiteLine(myImage.bytesPerLine(), 0); 
  
  for(int y=0; y<myImage.height(); y++) {
    const quint8 *referenceLine = (y == 0) ? (const quint8 *)whiteLine.constData() : (const quint8 *)myImage.constScanLine(y-1);
    const auto *codingLine = (const quint8 *)myImage.constScanLine(y);
    
    // We begin with an a0 = -1
    int a0 = -1;
    do{
      quint8 a0Value = (a0 == -1) ? 0 : bitVector::getBit(codingLine, a0);
      
      // Find value for a1. After this, a1 will either be equal to
      // myImage.width() or point to the first bit right of a0 whose value is
      // unequal to a0Value
      int a1 = bitVector::findEndOfRun(codingLine, (a0 == -1) ? 0 : a0, myImage.width(), a0Value);
      
      // Find b1 and b2
      int b1 = findB1(a0, a0Value, referenceLine, myImage.width());
      int b2 = bitVector::findEndOfRun(referenceLine, b1, myImage.width(), 1-a0Value);
      
      // Now we can encode data.
      if (b2 < a1) {
	// Pass mode
	result.append( bitVector::miniBitVector{4, 0b0001} );
	a0 = b2;
	continue;
      } if (qAbs(a1-b1) <= 3) {
	// Vertical mode
	if (a1 == b1)
	  result.append( V0 );	
	if (a1 == b1+1)
	  result.append( VR1 );	
	if (a1 == b1+2)
	  result.append( VR2 );	
	if (a1 == b1+3)
	  result.append( VR3 );	
	if (a1+1 == b1)
	  result.append( VL1 );	
	if (a1+2 == b1)
	  result.append( VL2 );	
	if (a1+3 == b1)
	  result.append( VL3 );	
	a0 = a1;
	continue;
      } 
	// Horizontal mode
	result.append( HOR );

	// Find value for a2. After this, a2 will either be equal to
	// myImage.width() or point to the first bit right of a0 whose value is
	// unequal to a0Value
	int a2 = bitVector::findEndOfRun(codingLine, a1, myImage.width(), 1-a0Value);

	a0 = qMax(a0, 0);
	addRun(a1-a0, a0Value, result);
	addRun(a2-a1, (a0Value == 0) ? 1 : 0, result);
	a0 = a2;
      
      
    }while(a0 < myImage.width());
  }

  // Add an end-of-facsimile block
  result.append(EOFB);
  
  // This converts the result to a QByteArray
  return result;
}


/*
 * FAX G4 decoder method
 *
 */

QImage compression::faxG4Decode(const QByteArray &data, int width, int height)
{
  // Open access to individual bits in the input data
  bitVector bits(data);
  
  // Prepare resulting image
  QImage result(width, (height <= 0) ? 100 : height, QImage::Format_Mono);
  result.setColorCount(2);
  result.setColor(0, 0xFFFFFFFF);
  result.setColor(1, 0xFF000000);
  
  // Prepare a few auxiliary variables: current bit that we are reading, and
  // current scanLine that we are reading
  int bitPointer = 0; 
  int currentLine = 0;

  // Prepare a completely white line as a reference line when reading the first
  // scanLine
  QByteArray whiteLine((width+7)/8, 0); 

  // Read scanlines until we have read the specified number of lines, or until
  // we ran into an error.
  for(;;) {
    // If bitPointer points to an end-of-facsimile block, we have successfully
    // ready everything and return our result.
    if (bits.startsWith(EOFB, bitPointer))
      return (result.height() == currentLine) ? result : result.copy(0,0,width,currentLine);
    
    // If we're done, return image.
    if ((height != 0) && (currentLine == height))
      return result.copy(0,0,width,currentLine);

    // If the resulting image is not sufficiently high, increase the image
    // height
    if (currentLine >= result.height())
      result = result.copy(0, 0, result.width(), 2*result.height());
    
    // Prepare pointer to the reference line
    const quint8 *referenceLine = (currentLine == 0) ? (const quint8 *)whiteLine.constData() : (const quint8 *)result.constScanLine(currentLine-1);
    quint8 *outputLine = result.scanLine(currentLine);
    
    // Read one scanLine.
    int a0 = -1;
    quint8 a0Value = 0;
    do{
      // Find b1 and b2
      int b1 = findB1(a0, a0Value, referenceLine, width);
      int b2 = bitVector::findEndOfRun(referenceLine, b1, width, 1-a0Value);
      a0 = qMax(a0,0);
      
      if (bits.startsWith(PASS, bitPointer)) {
	bitPointer += PASS.numBits;
	bitVector::fill(outputLine, a0, b2-a0, a0Value);
	a0 = b2;
      } else if (bits.startsWith(V0, bitPointer)) {
	bitPointer += V0.numBits;
	bitVector::fill(outputLine, a0, b1-a0, a0Value);  
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 = b1;
      } else if (bits.startsWith(VR1, bitPointer)) {
	bitPointer += VR1.numBits;
	bitVector::fill(outputLine, a0, b1+1-a0, a0Value);  
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 = b1+1;
      } else if (bits.startsWith(VR2, bitPointer)) {
	bitPointer += VR2.numBits;
	bitVector::fill(outputLine, a0, b1+2-a0, a0Value);  
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 = b1+2;
      } else if (bits.startsWith(VR3, bitPointer)) {
	bitPointer += VR3.numBits;
	bitVector::fill(outputLine, a0, b1+3-a0, a0Value);  
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 = b1+3;
      } else if (bits.startsWith(VL1, bitPointer)) {
	bitPointer += VL1.numBits;
	bitVector::fill(outputLine, a0, b1-1-a0, a0Value);  
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 = b1-1;
      } else if (bits.startsWith(VL2, bitPointer)) {
	bitPointer += VL2.numBits;
	bitVector::fill(outputLine, a0, b1-2-a0, a0Value);  
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 = b1-2;
      } else if (bits.startsWith(VL3, bitPointer)) {
	bitPointer += VL3.numBits;
	bitVector::fill(outputLine, a0, b1-3-a0, a0Value);  
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 = b1-3;
      } else if (bits.startsWith(HOR, bitPointer)) {
	bitPointer += HOR.numBits;
	qint64 rl = getRunLength(bits, bitPointer, a0Value);
	a0 = qMax(a0,0);
	bitVector::fill(outputLine, a0, rl, a0Value);
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 += rl;
	rl = getRunLength(bits, bitPointer, a0Value);
	bitVector::fill(outputLine, a0, rl, a0Value);
	a0Value = (a0Value == 0) ? 1 : 0;
	a0 += rl;
      } else
	// If the beginning of the bitVector could not be parsed, return with an
	// error.
	return QImage();
    }while(a0 < width); // Loop to read scanLine
    
    // Line has been read successfully
    currentLine++;
  };

  // We should never reach this point
  return QImage();
}
