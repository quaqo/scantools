/*
 * Copyright © 2017-2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef PAPERSIZE
#define PAPERSIZE

#include "length.h"


/** The paperSize class identifies and stores paper sizes
    
    This is a trivial class that helps to identify and store paper sizes. It
    knows about a few standard sizes.
*/

class paperSize
{
 public:
  /** List of supported standard sizes */
  enum format {
    A4,       /**< DIN A4 */
    A5,       /**< DIN A5 */
    empty,    /**< 0x0mm */
    Letter,   /**< US Standard Letter */
    Legal     /**< US Legal */
  };
  
  /** Constructs empty paperSize */
  paperSize() : _width(), _height() {
  }
  
  /** Constructs paperSize of given width and height
      
      @param width Width of the new paper size
      
      @param height Height of the new paper size
   */
  paperSize(length width, length height) : _width(width), _height(height) {
  }
    
  /** Constructs paperSize for given standard format

      @param f Standard paper format
   */
  // cppcheck-suppress noExplicitConstructor
  paperSize(format f) {
   setSize(f);
  }

  /** Constructs paperSize from a string

      This method calls setSize(QString, bool *) interally.

      @param ok If the string is not recoginzed *ok is set to false and the
      paper size if empty. Otherwise, *ok is set to true.
      
      @param formatString Should be "A4", "Letter", "297mmx210mm" etc.
  */
  paperSize(QString formatString, bool *ok=0) {
    bool myOk = setSize(formatString);
    if (ok)
      *ok = myOk;
  }
  
  /** Checks for empty paper size
      
      @return True if either width <= 0mm or height <= 0.mm, false otherwise
  */
  bool isEmpty() const {
    return (_width.isNonPositive() || _height.isNonPositive());
  }
  
  /** Sets paper height
      
      @param height Height of the papger
   */
  void setHeight(length height) {
    _height = height;
    return;
  }
  
  /** Sets paper width

      @param width Width of the papger
  */
  void setWidth(length width) {
    _width = width;
    return;
  }
  
  /** Height of the paper

      @returns height
  */
  length height() const {
    return _height;
  }

  /** Width of the paper

      @returns width
  */
  length width() const {
    return _width;
  }

  /** Sets paperSize to a given standard format

      @param f On of the standard formats defined above
   */
  void setSize(format f);

  /** Reads paperSize from a string
      
      @param formatString Should be "A4", "Letter", "297mmx210mm" etc.

      @returns If the string is not recoginzed false is returned and the paper
      size if empty. Otherwise, the methods returns true.
  */
  bool setSize(QString formatString);

 private:
  length _width;
  length _height;
};

#endif
