/*
 * Copyright © 2017--2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "length.h"
#include "lengthTest.h"

QTEST_MAIN(lengthTest)


void lengthTest::lengthConstructions()
{
  length l;
  QCOMPARE( l.get(length::mm), 0.0);

  l.set(1.0, length::cm);
  QCOMPARE( l.get(length::mm), 10.0);

  l.set(1.0, length::in);
  QCOMPARE( l.get(length::mm), 25.4);

  l.set(1.0, length::pt);
  QCOMPARE( 72.0*l.get(length::mm), 25.4);

  l.set(1.0, length::pt);
  QCOMPARE( 72.0*l.get(length::mm), 25.4);
  QCOMPARE( 72.0*l.get(length::in),  1.0);
  QCOMPARE(      l.get(length::pt),  1.0);

  bool ok;
  ok = l.set("1.0 cm");
  QCOMPARE( ok, true);
  QCOMPARE( l.get(length::mm), 10.0);

  ok = l.set("1.0cM");
  QCOMPARE( ok, true);
  QCOMPARE( l.get(length::mm), 10.0);

  ok = l.set("1.0 mm");
  QCOMPARE( ok, true);
  QCOMPARE( l.get(length::mm),  1.0);

  ok = l.set("1.0 in");
  QCOMPARE( ok, true);
  QCOMPARE( l.get(length::mm), 25.4);

  ok = l.set("1.0 pt");
  QCOMPARE( ok, true);
  QCOMPARE( 72*l.get(length::mm), 25.4);

  ok = l.set("1.0mt");
  QCOMPARE( ok, false);

  length l1("2.54cm");
  QCOMPARE( l1.get(length::in), 1.0);
}
