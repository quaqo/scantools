/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bitVector.h"
#include <utility>

bitVector::bitVector()
{
    _size = 0;
}


bitVector::bitVector(int numBits)
    : content((numBits+7)/8, 0)
{
    _size = numBits;
}


bitVector::bitVector(QByteArray data)
    : content(std::move(data))
{
    _size = content.size()*8;
}


void bitVector::resize(int newSize)
{
    // Paranoid safety check
    newSize = qMax(newSize, 0);

    content.resize((newSize+7)/8);
    _size = newSize;
}


auto bitVector::startsWith(const miniBitVector mbv, int bitIndex) const -> bool
{
    if (bitIndex+mbv.numBits > _size) {
        return false;
    }

    for(int bit=0; bit<mbv.numBits; bit++) {
        if (mbv.getBit(bit) != getBit(bitIndex+bit)) {
            return false;
        }
    }

    return true;
}


bitVector::operator QByteArray()
{
    for(int i=_size; (i%8) != 0; i++) {
        setBit(i, 0);
    }

    return content.left( (_size+7)/8 );
}


bitVector::operator QString() const
{
    QString result;

    for(int i=0; i<_size; i++) {
        if (getBit(i) != 0) {
            result += "1";
        } else {
            result += "0";
        }
        if ((i > 0) && (i%8 == 7)) {
            result += " ";
        }
    }

    return result;
}
