/*
 * Copyright © 2016 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <QCryptographicHash>
#include <QDataStream>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QImageReader>
#include <QMimeDatabase>
#include <QTextCodec>
#include <QtConcurrent>
#include <functional>

#include "jpeglib.h"

#include "HOCRDocument.h"
#include "JBIG2Document.h"
#include "PDFAWriter.h"
#include "TIFFReader.h"
#include "compression.h"
#include "imageInfo.h"
#include "imageOperations.h"
#include "scantools.h"



PDFAWriter::PDFAWriter(bool _bestCompression, QObject* parent)
    : QObject(parent), _autoOCR(false), fontObjectIndex(0), bestCompression(_bestCompression)
{
    /*
     * General preparation.
     */

    // Pull in ressource
    Q_INIT_RESOURCE(libscantools);


    /*
     * Generate fundamental PDF objects, and store their indices
     */

    // Object 1: Catalog
    objects << readFile(":PDFtemplates/catalogObject.tmpl");
    catalogObjectIndex = objects.size();

    // Object 2: Placeholder for XMP MetaData
    objects << QByteArray();
    metaDataObjectIndex = objects.size();

    // Object 3: Placeholder for Info directory
    objects << QByteArray();
    infoObjectIndex = objects.size();

    // Object 4: Page Directory
    objects << generatePageDirectoryObject();
    pageDirectoryObjectIndex = objects.size();

    // Object 5: ICC color profile file
    QByteArray iccContent = readFile(":/sRGB.icc.zlib");
    QByteArray ICCObject = readFile(":PDFtemplates/ICCObject.tmpl");
    ICCObject.replace("%length", QString::number(iccContent.size()).toUtf8());
    ICCObject.replace("%content", iccContent);
    objects << ICCObject;
    quint32 ICCObjectIndex = objects.size();

    // Object 6: Color Profile Object
    QByteArray colorProfileObject = readFile(":PDFtemplates/colorProfileObject.tmpl");
    colorProfileObject.replace("%iccIndex", QString::number(ICCObjectIndex).toUtf8());
    objects << colorProfileObject;
    colorProfileObjectIndex = objects.size();
}


PDFAWriter::~PDFAWriter()
{
    QWriteLocker locker(&lock);  // Thread safety

    foreach(auto object, objects)
        object.future.waitForFinished();
}


auto PDFAWriter::author() -> QString
{
    QReadLocker locker(&lock);
    return _author;
}


void PDFAWriter::setAuthor(const QString &author)
{
    QWriteLocker locker(&lock);
    _author = author;
    emit authorChanged();
}


auto PDFAWriter::keywords() -> QString
{
    QReadLocker locker(&lock);
    return _keywords;
}


void PDFAWriter::setKeywords(const QString &keywords)
{
    QWriteLocker locker(&lock);
    _keywords = keywords;
    emit keywordsChanged();
}


auto PDFAWriter::subject() -> QString
{
    QReadLocker locker(&lock);
    return _subject;
}


void PDFAWriter::setSubject(const QString &subject)
{
    QWriteLocker locker(&lock);
    _subject = subject;
    emit subjectChanged();
}


auto PDFAWriter::title() -> QString
{
    QReadLocker locker(&lock);
    return _title;
}


void PDFAWriter::setTitle(const QString &title)
{
    QWriteLocker locker(&lock);
    _title = title;
    emit titleChanged();
}


void PDFAWriter::setPageSize(const paperSize size)
{
    QWriteLocker locker(&lock);
    _pageSize = size;
    emit pageSizeChanged();
}


void PDFAWriter::setPageSize(paperSize::format size)
{
    QWriteLocker locker(&lock);
    _pageSize = paperSize(size);
    emit pageSizeChanged();
}


auto PDFAWriter::pageSize() -> paperSize
{
    QReadLocker locker(&lock);
    return _pageSize;
}


void PDFAWriter::setResolutionOverrideHorizontal(resolution horizontal)
{
    // Paranoid safety checks
    if (!horizontal.isZero() && !horizontal.isValid()) {
        return;
    }

    // If input is ok, set override values
    QWriteLocker locker(&lock);
    horizontalResolutionOverride = horizontal;
    emit resolutionOverrideHorizontalChanged();
}


void PDFAWriter::setResolutionOverrideVertical(resolution vertical)
{
    // Paranoid safety checks
    if (!vertical.isZero() && !vertical.isValid()) {
        return;
    }

    // If input is ok, set override values
    QWriteLocker locker(&lock);
    verticalResolutionOverride   = vertical;
    emit resolutionOverrideVerticalChanged();
}


void PDFAWriter::setResolutionOverride(resolution horizontal, resolution vertical)
{
    // Paranoid safety checks
    if (!horizontal.isZero() && !horizontal.isValid()) {
        return;
    }
    if (!vertical.isZero() && !vertical.isValid()) {
        return;
    }

    // If input is ok, set override values
    QWriteLocker locker(&lock);

    horizontalResolutionOverride = horizontal;
    verticalResolutionOverride   = vertical;
    emit resolutionOverrideHorizontalChanged();
    emit resolutionOverrideVerticalChanged();
}


auto PDFAWriter::resolutionOverrideHorizontal() -> resolution
{
    QReadLocker locker(&lock);
    return horizontalResolutionOverride;
}


auto PDFAWriter::resolutionOverrideVertical() -> resolution
{
    QReadLocker locker(&lock);
    return verticalResolutionOverride;
}


void PDFAWriter::setAutoOCR(bool a)
{
    QWriteLocker locker(&lock);
    _autoOCR = a;
    emit autoOCRChanged();
}


auto PDFAWriter::autoOCR() -> bool
{
    QReadLocker locker(&lock);
    return _autoOCR;
}


auto PDFAWriter::setAutoOCRLanguages(const QStringList &nOCRLanguages) -> QString
{
    // Paranoid safety check
    QStringList supportedLingos = HOCRDocument::tesseractLanguages();
    foreach(auto language, nOCRLanguages) {
        if (supportedLingos.contains(language)) {
            continue;
        }

        return QString("Error. Language '%1' is not supported in the current installation of the tesseract OCR engine. Supported language(s) are %2.").arg(language, supportedLingos.join(", "));
    }

    // Set language
    QWriteLocker locker(&lock);
    OCRLanguages = nOCRLanguages;
    emit autoOCRLanguagesChanged();
    return QString();
}


auto PDFAWriter::autoOCRLanguages() -> QStringList
{
    QReadLocker locker(&lock);
    return OCRLanguages;
}


void PDFAWriter::appendToOCRData(const HOCRDocument &doc)
{
    if (doc.hasError()) {
        return;
    }

    QWriteLocker locker(&lock);
    userSpecifiedOCRData.append(doc);
}


auto PDFAWriter::OCRData() -> HOCRDocument
{
    QReadLocker locker(&lock);
    return userSpecifiedOCRData;
}


void PDFAWriter::clearOCRData()
{
    QWriteLocker locker(&lock);
    userSpecifiedOCRData.clear();
}


auto PDFAWriter::addPages(const QImage &image, QStringList * /*warnings*/) -> QString
{
    /*
     * Paranoid safety checks
     */
    if (image.isNull() || (image.width() == 0) || (image.height() == 0)) {
        return QString("Cannot add empty image to PDF/A file");
    }

    /*
     * Read dimensions and other data from image
     */
    imageInfo bInfo(image);

    // Override resolution info, if relevant
    if (horizontalResolutionOverride.isValid()) {
        bInfo._xResolution = horizontalResolutionOverride;
    }
    if (verticalResolutionOverride.isValid()) {
        bInfo._yResolution = verticalResolutionOverride;
    }

    // Check that resolution info is valid
    if (!bInfo.xResolution().isValid()) {
        return QString("The image does not define a valid horizontal resolution, and no valid default has been given.");
    }
    if (!bInfo.yResolution().isValid()) {
        return QString("The image does not define a valid vertical resolution, and no valid default has been given.");
    }

    /*
     * At this point, everything has been checked. There have not been any errors,
     * so we are sure that we can modify the PDF now without possibility of
     * failure.
     */

    // Thread safety: get exclusive write access to this PDFAWriter
    QWriteLocker locker(&lock);

    // Create a copy of the image that we can modify, with a well-definded set of
    // QImage::Format
    QImage myImage = imageOperations::optimizedFormat(image);

    // Create a PDF graphics object. The method used
    if (imageOperations::isBlackAndWhite(myImage)) {
        objects << QtConcurrent::run(createImageObject_bw_G4, myImage);    // Black-and-white
    } else if (myImage.format() == QImage::Format_Mono) {
        objects << QtConcurrent::run(createImageObject_bitonal_G4, myImage);    // Bitonal, not black-and-white
    } else if (myImage.allGray()) {
        objects << QtConcurrent::run(createImageObject_gray_zlib, myImage, bestCompression);    // Gray scale
    } else if (myImage.format() == QImage::Format_Indexed8) {
        objects << QtConcurrent::run(createImageObject_indexed_zlib, myImage, bestCompression);    // Indexed color
    } else {
        objects << QtConcurrent::run(createImageObject_rgb_zlib, myImage, bestCompression);    // Full color
    }

    // Now that the graphics object has been added, create a page object
    addGFXPage(objects.size(), bInfo, myImage);

    // Return without error
    return QString();
}


auto PDFAWriter::addPages(const JBIG2Document &jbig2doc, QStringList * /*warnings*/) -> QString
{
    //  The JBIG2 file must not have any error condition set.
    if (jbig2doc.hasError()) {
        return QString("JBIG2 document has error: %1.").arg(jbig2doc.error());
    }

    // Check that the JBIG2 contains no pages with empty page data, and that all
    // the resolution data that we need is actually present, and within range.
    QList<quint32> pageNumbers = jbig2doc.pageNumbers();
    quint32 pageNumber = 0;
    foreach(pageNumber, pageNumbers) {
        // Check that page data is not empty
        if (jbig2doc.getPDFDataChunk(pageNumber).isEmpty()) {
            return QString("JBIG2 error. Page data for page %1 is empty.").arg(pageNumber);
        }

        // Check that bitmap is not empty
        imageInfo binfo = jbig2doc.pageInfo(pageNumber);
        if (binfo.isEmpty()) {
            return QString("JBIG2 error. Bitmap for page %1 is empty.").arg(pageNumber);
        }

        // Override resolution info, if relevant
        if (horizontalResolutionOverride.isValid()) {
            binfo._xResolution = horizontalResolutionOverride;
        }
        if (verticalResolutionOverride.isValid()) {
            binfo._yResolution = verticalResolutionOverride;
        }

        // Check that resolution info is valid
        if (!binfo.xResolution().isValid()) {
            return QString("The JBIG2 document does not specify a valid horizontal resolution for page %1, and no valid default resolution was given.").arg(pageNumber);
        }
        if (!binfo.yResolution().isValid()) {
            return QString("The JBIG2 document does not specify a valid vertical resolution for page %1, and no valid default resolution was given.").arg(pageNumber);
        }
    }
    // End of paranoid safety checks.  At this point we are sure that all relevant
    // data is present, so we can proceed to add the JBIG2 file to our PDF.  No
    // error can possibly occur.

    // Thread safety: get exclusive write access to this PDFAWriter
    QWriteLocker locker(&lock);


    /*
     * Read and interpret JBIG2 symbol file
     */
    objects << QtConcurrent::run(generateStreamObject, jbig2doc.getPDFDataChunk(0));
    auto symbolObjectIndex = objects.size();


    /*
     * Read and interpret JBIG2 pages
     */
    foreach(pageNumber, pageNumbers) {
        // get JBIG2 page data
        QByteArray page = jbig2doc.getPDFDataChunk(pageNumber);

        // Extract width and height (in pixels), as well as horizontal and vertical
        // resolutions from the JBIG2 page data. If no default resolution is given,
        // use horizontal resolution read from the JBIG2 file. Otherwise, use
        // default resolution.
        imageInfo binfo = jbig2doc.pageInfo(pageNumber);

        // Handle horizontal resolution
        if (horizontalResolutionOverride.isValid()) {
            binfo._xResolution = horizontalResolutionOverride;
        }
        if (verticalResolutionOverride.isValid()) {
            binfo._yResolution = verticalResolutionOverride;
        }

        // Add data stream for one JBIG2 page and object
        QByteArray JBIG2StreamObject = readFile(":PDFtemplates/JBIG2StreamObject.tmpl");
        JBIG2StreamObject.replace("%width", QString::number(binfo.widthInPixel).toUtf8());
        JBIG2StreamObject.replace("%height", QString::number(binfo.heightInPixel).toUtf8());
        JBIG2StreamObject.replace("%symbolIdx", QString::number(symbolObjectIndex).toUtf8());
        JBIG2StreamObject.replace("%length", QString::number(page.size()).toUtf8());
        JBIG2StreamObject.replace("%content", page);
        objects << JBIG2StreamObject;
        quint32 JBIG2StreamObjectIndex = objects.size();

        if (userSpecifiedOCRData.isEmpty() && _autoOCR) {
            addGFXPage(JBIG2StreamObjectIndex, binfo, jbig2doc[pageNumber]);
        } else {
            addGFXPage(JBIG2StreamObjectIndex, binfo);
        }

    }

    // Return without error
    return QString();
}


auto PDFAWriter::addJBIG2(const QString &fileName, QStringList *warnings) -> QString
{
    // Create JBIG2 document
    JBIG2Document jbig2doc(fileName);

    // Deal with errors
    if (jbig2doc.hasError()) {
        return QString("Error reading JBIG2 file '%1'. %2").arg(fileName,jbig2doc.error());
    }

    // Deal with warnings.
    if (warnings != nullptr) {
        QSet<QString> JBwarnings = jbig2doc.warnings();
        foreach(const QString &warning, JBwarnings) {
            *warnings << QString("The JBIG2 file '%1' contains an error that has been fixed: ").arg(fileName)+warning;
        }
    }

    // Do not do anything for empty documents
    if (jbig2doc.isEmpty()) {
        return QString();
    }

    // Add the JBIG2 file to this document
    return addPages(jbig2doc);
}


auto PDFAWriter::addJPX(const QString &fileName) -> QString
{
    /*
     * Read dimensions and other data from JP2 file
     */
    imageInfo jp2Info(fileName);
    QByteArray fileContent = readFile(fileName);

    // Check for errors
    if (fileContent.isEmpty()) {
        return QString("File error. Cannot read JPEG2000 file %1.").arg(fileName);
    }
    if (!jp2Info.error.isEmpty()) {
        return jp2Info.error;
    }
    if ((jp2Info.widthInPixel == 0) || (jp2Info.heightInPixel == 0)) {
        return QString("File format error. Cannot handle JP2 file '%1' seems to be empty.").arg(fileName);
    }

    // Set override resolution.
    if (horizontalResolutionOverride.isValid()) {
        jp2Info._xResolution = horizontalResolutionOverride;
    }
    if (verticalResolutionOverride.isValid()) {
        jp2Info._yResolution = verticalResolutionOverride;
    }

    // Check that resolution info is valid
    if (!jp2Info.xResolution().isValid()) {
        return QString("The JP2 file '%1' does not specify a valid horizontal resolution and no valid default resolution was given.").arg(fileName);
    }
    if (!jp2Info.yResolution().isValid()) {
        return QString("The JP2 file '%1' does not specify a valid vertical resolution and no valid default resolution was given.").arg(fileName);
    }

    // Load the image file, if need be
    QImage img;
    if (userSpecifiedOCRData.isEmpty() && _autoOCR) {
        img = QImage(fileName);
        if (img.isNull()) {
            QString msg = QString("Failed to read JPX image file '%1', which is necessary to perform OCR.").arg(fileName);
            if (!QImageReader::supportedImageFormats().contains("jp2")) {
                msg += QString(" The Qt library was build without JPEG2000 support.");
                msg += QString(" You might wish to disable OCR for now, and use the command 'ocrPDF' afterwards.");
            }
            return msg;
        }
    }


    /*
     * At this point, everything has been checked. There have not been any errors,
     * so we are sure that we can modify the PDF now without possibility of
     * failure.
     */

    // Thread safety: get exclusive write access to this PDFAWriter
    QWriteLocker locker(&lock);

    // Create the JP2 image object and add it to the PDF
    QByteArray JP2StreamObject("<</Filter[/JPXDecode]/Height %height/Length %length/Subtype/Image/Width %width>>stream\n%content\nendstream\n");
    JP2StreamObject.replace("%width", QString::number(jp2Info.widthInPixel).toUtf8());
    JP2StreamObject.replace("%height", QString::number(jp2Info.heightInPixel).toUtf8());
    JP2StreamObject.replace("%length", QString::number(fileContent.size()).toUtf8());
    JP2StreamObject.replace("%content", fileContent);
    objects << JP2StreamObject;
    quint32 JP2StreamObjectIndex = objects.size();

    // Add graphics page to the PDF
    if (userSpecifiedOCRData.isEmpty() && _autoOCR) {
        addGFXPage(JP2StreamObjectIndex, jp2Info, img);
    } else {
        addGFXPage(JP2StreamObjectIndex, jp2Info);
    }

    // Return without error
    return QString();
}


auto PDFAWriter::addJPEG(const QString &fileName) -> QString
{
    /*
     * Read dimensions and other data from JPEG file
     */
    imageInfo jpegInfo(fileName);
    QByteArray fileContent = readFile(fileName);

    // Check for errors
    if (fileContent.isEmpty()) {
        return QString("File error. Cannot read JPEG file %1.").arg(fileName);
    }
    if (!jpegInfo.error.isEmpty()) {
        return jpegInfo.error;
    }
    if ((jpegInfo.widthInPixel == 0) || (jpegInfo.heightInPixel == 0)) {
        return QString("File format error. Cannot handle JPEG file '%1' seems to be empty.").arg(fileName);
    }
    if ((jpegInfo.numberOfColorComponents != 1) && (jpegInfo.numberOfColorComponents != 3)) {
        return QString("File format error. Cannot handle JPEG file '%1', which has %2 color components. "
                       "This program handles only files with one or three components.").arg(fileName, jpegInfo.numberOfColorComponents);
    }

    // Set override resolution.
    if (horizontalResolutionOverride.isValid()) {
        jpegInfo._xResolution = horizontalResolutionOverride;
    }
    if (verticalResolutionOverride.isValid()) {
        jpegInfo._yResolution = verticalResolutionOverride;
    }

    // Check that resolution info is valid
    if (!jpegInfo.xResolution().isValid()) {
        return QString("The JPEG file '%1' does not specify a valid horizontal resolution and no valid default resolution was given.").arg(fileName);
    }
    if (!jpegInfo.yResolution().isValid()) {
        return QString("The JPEG file '%1' does not specify a valid vertical resolution and no valid default resolution was given.").arg(fileName);
    }


    /*
   * At this point, everything has been checked. There have not been any errors,
   * so we are sure that we can modify the PDF now without possibility of
   * failure.
   */

    // Thread safety: get exclusive write access to this PDFAWriter
    QWriteLocker locker(&lock);

    // Create the JPEG image object and add it to the PDF
    QByteArray JPEGStreamObject("<</BitsPerComponent 8/ColorSpace/%color/Filter[/DCTDecode]/Height %height/Length %length/Subtype/Image/Width %width>>stream\n%content\nendstream\n");
    if (jpegInfo.numberOfColorComponents == 1) {
        JPEGStreamObject.replace("%color", "DeviceGray");
    } else {
        JPEGStreamObject.replace("%color", "DeviceRGB");
    }
    JPEGStreamObject.replace("%width", QString::number(jpegInfo.widthInPixel).toUtf8());
    JPEGStreamObject.replace("%height", QString::number(jpegInfo.heightInPixel).toUtf8());
    JPEGStreamObject.replace("%length", QString::number(fileContent.size()).toUtf8());
    JPEGStreamObject.replace("%content", fileContent);
    objects << JPEGStreamObject;
    quint32 JPEGStreamObjectIndex = objects.size();

    // Add graphics page to the PDF
    if (userSpecifiedOCRData.isEmpty() && _autoOCR) {
        addGFXPage(JPEGStreamObjectIndex, jpegInfo, QImage(fileName));
    } else {
        addGFXPage(JPEGStreamObjectIndex, jpegInfo);
    }

    // Return without error
    return QString();
}


auto PDFAWriter::addTIFF(const QString &fileName) -> QString
{
    TIFFReader tiff(fileName);
    if (tiff.hasError()) {
        return tiff.error();
    }

    for(quint32 i=0; i<tiff.size(); i++) {
        QImage image = tiff[i];
        if (tiff.hasError()) {
            return tiff.error();
        }
        QString error = addPages(image);
        if (!error.isEmpty()) {
            return error;
        }
    }

    return QString();
}


void PDFAWriter::addGFXPage(quint32 graphicObjectIndex, const imageInfo& bInfo, const QImage& imageForOCR)
{
    // Generate a page content object that contains the code to draw the
    // graphicObject, as well as the code for the text overlay.
    QByteArray contentStream("q %width 0 0 %height %deltaX %deltaY cm /Im1 Do Q");

    auto imageWidth  = bInfo.widthInPixel  / bInfo.xResolution();
    auto imageHeight = bInfo.heightInPixel / bInfo.yResolution();
    auto pageWidth   = _pageSize.isEmpty() ? imageWidth  : _pageSize.width();
    auto pageHeight  = _pageSize.isEmpty() ? imageHeight : _pageSize.height();
    auto deltaX      = (pageWidth  - imageWidth )/2.0;
    auto deltaY      = (pageHeight - imageHeight)/2.0;

    contentStream.replace("%width",  QString::number(qRound(imageWidth.get(length::pt)) ).toUtf8());
    contentStream.replace("%height", QString::number(qRound(imageHeight.get(length::pt))).toUtf8());
    contentStream.replace("%deltaX", QString::number(qRound(deltaX.get(length::pt))).toUtf8());
    contentStream.replace("%deltaY", QString::number(qRound(deltaY.get(length::pt))).toUtf8());

    bool doOCR = _autoOCR || !userSpecifiedOCRData.isEmpty(); // True if OCR data will be included into page

    if (userSpecifiedOCRData.isEmpty() && _autoOCR)
    {
        objects << QtConcurrent::run( std::bind(completePageContentObject_b, contentStream, bInfo, deltaX, deltaY, imageForOCR, OCRLanguages) );
    }
    else
    {
        objects << QtConcurrent::run( std::bind(completePageContentObject_a, contentStream, bInfo, deltaX, deltaY, userSpecifiedOCRData.takeFirstPage()) );
    }
    auto pageContentIndex = objects.size();

    // Generate the necessary ressources
    QByteArray ressources("/Resources << ");
    if (doOCR)
    {
        ressources.append("/Font<</F1 %rsc 0 R>>");
        ressources.replace("%rsc", QString::number(getFontObjectIndex()).toUtf8());
    }
    ressources.append("/XObject<</Im1 %imgIdx 0 R>>/ProcSet[/PDF/ImageB]>>");
    ressources.replace("%imgIdx", QString::number(graphicObjectIndex).toUtf8());

    // Generate a page object
    auto pageObject = readFile(":PDFtemplates/pageObject.tmpl");
    pageObject.replace("%width",  QString::number( qRound(pageWidth.get(length::pt) ) ).toUtf8());
    pageObject.replace("%height", QString::number( qRound(pageHeight.get(length::pt)) ).toUtf8());
    pageObject.replace("%ressources", ressources);
    pageObject.replace("%contentidx", QString::number(pageContentIndex).toUtf8());
    pageObject.replace("%pageDirectoryObjectIndex", QString::number(pageDirectoryObjectIndex).toUtf8());
    objects << pageObject;
    pageIndices << objects.size();

    // Regenerate the page directory object
    objects[pageDirectoryObjectIndex-1] = generatePageDirectoryObject();
}


auto PDFAWriter::addPages(const QString &fileName, QStringList *warnings) -> QString
{
    /*
   * If fileName points to a JBIG2, JPEG, JPX or TIFF file, then read the file
   * with one of the specialized private methods, which will add its content to
   * the document
   */
    QMimeDatabase DB;
    QString mimeName = DB.mimeTypeForFile(fileName).name();
    if (fileName.endsWith("JB2", Qt::CaseInsensitive) || fileName.endsWith("JBIG2", Qt::CaseInsensitive)) {
        // Standard mime installation does not know of JBIG2 files, hence we go by
        // filename
        return addJBIG2(fileName, warnings);
    }
    if (mimeName == "image/jpeg") {
        return addJPEG(fileName);
    }
    if (fileName.endsWith("JPF", Qt::CaseInsensitive) || fileName.endsWith("JPX", Qt::CaseInsensitive)) {
        // Standard mime installation incorrectly identifies JPX files as
        // "image/jp2", hence we go by filename
        return addJPX(fileName);
    }
    if (mimeName == "image/tiff") {
        return addTIFF(fileName);
    }


    /*
   * If fileName points to a file that we can open as a bitmap graphic, then add
   * it to the document.
   */
    if (QImageReader::supportedMimeTypes().contains(mimeName.toLatin1())) {
        QImageReader imageReader(fileName);
        if (!imageReader.canRead()) {
            return QString("Error reading file %1. %2").arg(fileName, imageReader.errorString());
        }

        // Read all images contained in the file
        do {
            QImage image = imageReader.read();
            if (image.isNull()) {
                return QString("Error reading file %1. %2").arg(fileName, imageReader.errorString());
            }

            addPages(image);
        } while(imageReader.jumpToNextImage());
        return QString();
    }

    return QString("File %1 has type '%2' (= %3), which is not supported.").arg(fileName, DB.mimeTypeForFile(fileName).name(), DB.mimeTypeForFile(fileName).comment() );
}


PDFAWriter::operator QByteArray()
{
    QWriteLocker locker(&lock);  // Thread safety

    /*
   * Generate meta data objects
   */

    // Need codec for UTF16(Big Endian)
    QTextCodec *utf16codec = QTextCodec::codecForName("UTF-16BE");
    if (utf16codec == nullptr) {
        qFatal("Fatal error. Codec UTF-16BE not found");
    }

    // Need current time in various formats
    QDateTime currentDate = QDateTime::currentDateTimeUtc();
    QString currentDateStringISO = currentDate.toString(Qt::ISODate);
    QString currentDateStringPDF = currentDate.toString("yyyyMMddhhmmss");

    // Generate Object 2: XMP MetaData
    QByteArray XMP = readFile(":PDFtemplates/metadata.xml");
    XMP.replace("%Keywords", _keywords.toUtf8());
    XMP.replace("%ModifyDate", currentDateStringISO.toUtf8());
    XMP.replace("%CreateDate", currentDateStringISO.toUtf8());
    XMP.replace("%Title", _title.toUtf8());
    XMP.replace("%Author", _author.toUtf8());
    XMP.replace("%Description", _subject.toUtf8());
    QByteArray XMPObject = readFile(":PDFtemplates/XMPObject.tmpl");
    XMPObject.replace("%length", QString::number(XMP.size()).toUtf8());
    XMPObject.replace("%XMP", XMP);
    objects[metaDataObjectIndex-1] = XMPObject;

    // Generate Object 3: Info directory
    QByteArray info = readFile(":PDFtemplates/infoObject.tmpl");
    if (_author.isEmpty()) {
        info.replace("%Author", "()");
    } else {
        info.replace("%Author", "<"+utf16codec->fromUnicode(_author).toHex()+">");
    }
    if (_keywords.isEmpty()) {
        info.replace("%Keywords", "()");
    } else {
        info.replace("%Keywords", "<"+utf16codec->fromUnicode(_keywords).toHex()+">");
    }
    if (_subject.isEmpty()) {
        info.replace("%Subject", "()");
    } else {
        info.replace("%Subject", "<"+utf16codec->fromUnicode(_subject).toHex()+">");
    }
    if (_title.isEmpty()) {
        info.replace("%Title", "()");
    } else {
        info.replace("%Title", "<"+utf16codec->fromUnicode(_title).toHex()+">");
    }
    info.replace("%cdate", currentDateStringPDF.toUtf8());
    objects[infoObjectIndex-1] = info;


    /*
     * Generate PDF/A
     */
    QByteArray result;          // Result will be constructed here
    QList<quint32> offsets;     // Addresses of the PDF objects
    offsets.reserve(objects.size());

    // PDF File Header
    result += "%PDF-1.7\n"
              "%¥±ë\n";

    // PDF file body: add objects
    for (int i=0; i<objects.size(); ++i) {
        offsets.append(result.size());
        result += QString("\n%1 0 obj\n").arg(i+1);
        result += objects[i];
        result += "endobj";
    }
    result += "\n";

    // PDF Cross-Reference Table
    quint32 xrefstart = result.size();
    result += QString("xref\n0 %1\n0000000000 65535 f \n").arg(objects.length()+1).toUtf8();
    for (unsigned int offset : offsets) {
        result += QString("%1 00000 n \n").arg(offset+1,10,10,QChar('0')).toUtf8();
    }

    // Generate ID for this document from cryptographic hash. Will be used in the
    // PDF trailer
    QCryptographicHash hash(QCryptographicHash::Md5);
    hash.addData(result);
    QString id = hash.result().toHex();

    // PDF Trailer
    QByteArray trailer = readFile(":PDFtemplates/trailer.tmpl");
    trailer.replace("%rootIdx", QString::number(catalogObjectIndex).toUtf8());
    trailer.replace("%infoObjectIdx", QString::number(infoObjectIndex).toUtf8());
    trailer.replace("%size", QString::number(offsets.length()+1).toUtf8());
    trailer.replace("%ID", id.toUtf8());
    trailer.replace("%xrefstart", QString::number(xrefstart).toUtf8());
    result += trailer;

    // Done
    return result;
};


void PDFAWriter::waitForWorkerThreads()
{
    QReadLocker locker(&lock);  // Thread safety

    int total = objects.size();

    qreal i = 0;
    foreach(auto object, objects) {
        object.future.waitForFinished();

        i++;
        if (total != 0) {
            emit progress(i/static_cast<qreal>(total));
        }
    }

    emit finished();
}


auto PDFAWriter::readFile(const QString& fileName) -> QByteArray
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly)) {
        return file.readAll();
    }
    return QByteArray();
};


auto PDFAWriter::generatePageDirectoryObject() const -> QByteArray
{
    // Produce list of pages, first as a QString
    QString kids;
    foreach(auto idx, pageIndices)
        kids += QString("%1 0 R ").arg(idx);

    // Now generate the page directory
    QByteArray pageDirectory("<</Type/Pages/Kids[%kids]/Count %count>>\n");
    pageDirectory.replace("%kids", kids.toUtf8());
    pageDirectory.replace("%count", QString::number(pageIndices.size()).toUtf8());

    return pageDirectory;
}


auto PDFAWriter::getFontObjectIndex() -> quint32
{
    // If font object has not yet been created, create it.
    if (fontObjectIndex == 0) {
        QByteArray fontObject2("<< /Type /Font /Subtype /Type1 /BaseFont /Times-Roman /Encoding /WinAnsiEncoding >>\n");
        objects << fontObject2;
        fontObjectIndex = objects.size();
    }

    // Return the index.
    return fontObjectIndex;
}
