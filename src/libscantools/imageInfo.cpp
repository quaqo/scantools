/*
 * Copyright © 2017 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cmath>
#include <csetjmp>
#include <cstdio>
#include <jpeglib.h>
#include <QBuffer>
#include <QFile>
#include <QImage>
#include <QMimeDatabase>

#include "imageInfo.h"
#include "JBIG2Document.h"
#include "JP2Box.h"
#include "TIFFReader.h"


/* Beginning of JPEG library wizardry. For error handling without exit(), we
   need to define our own handling routing, and our own errorManager. This is
   done here. For details, see the (sparse) documentation of the jpeg library,
   and the example code there. */

struct errorManager
{
  jpeg_error_mgr pub;               // "public" fields
  jmp_buf        setjmp_buffer;     // for return to caller
  QString *      errorMsgPtr;
};


void errorHandling(j_common_ptr cinfo)
{
  // Create the error message
  char buffer[JMSG_LENGTH_MAX];
  (*cinfo->err->format_message) (cinfo, buffer);
  *(reinterpret_cast<errorManager *>(cinfo->err))->errorMsgPtr = QString::fromLocal8Bit(buffer);

  // Jump back to caller
  longjmp((reinterpret_cast<errorManager *>(cinfo->err))->setjmp_buffer, 1);
}

/* End of JPEG library wizardry */



imageInfo::imageInfo()
{
  clear();
}


bool imageInfo::read(const QImage &image)
{
  clear();
  
  switch(image.format()) {
  case QImage::Format_Invalid:
    error = "The image used to generate the image info was invalid.";
    return false;
  case QImage::Format_Grayscale8:
  case QImage::Format_Mono:
  case QImage::Format_MonoLSB:
    numberOfColorComponents = 1;
    break;
  case QImage::Format_RGB32:
  case QImage::Format_RGB16:
  case QImage::Format_RGB666:
  case QImage::Format_RGB555:
  case QImage::Format_RGB888:
  case QImage::Format_RGB444:
  case QImage::Format_RGBX8888:
    numberOfColorComponents = 3;
    break;
  case QImage::Format_A2BGR30_Premultiplied:
  case QImage::Format_A2RGB30_Premultiplied:
  case QImage::Format_ARGB32:
  case QImage::Format_ARGB32_Premultiplied:
  case QImage::Format_ARGB8565_Premultiplied:
  case QImage::Format_ARGB6666_Premultiplied:
  case QImage::Format_ARGB8555_Premultiplied:
  case QImage::Format_ARGB4444_Premultiplied:
  case QImage::Format_BGR30:
  case QImage::Format_Indexed8:
  case QImage::Format_RGB30:
  case QImage::Format_RGBA8888:
  case QImage::Format_RGBA8888_Premultiplied:
    numberOfColorComponents = 4;
    break;
  default:
    numberOfColorComponents = 0;
    break;
  }
  
  heightInPixel           = image.height();
  widthInPixel            = image.width();
  _xResolution.set(image.dotsPerMeterX(), resolution::dpm);
  _yResolution.set(image.dotsPerMeterY(), resolution::dpm);
  return true;
}


bool imageInfo::read(const QString &fileName)
{
  clear();
  
  /* If fileName points to a JBIG2, JPEG or JPX file, then read the file with
   * one of the specialized private methods */
  
  // Standard mime installation does not know of JBIG2 files, hence we go by
  // filename
  if (fileName.endsWith("JB2", Qt::CaseInsensitive) || fileName.endsWith("JBIG2", Qt::CaseInsensitive)) {
    JBIG2Document jb2(fileName);
    QList<quint32> numbers = jb2.pageNumbers();
    if (!numbers.isEmpty())
      *this = jb2.pageInfo(numbers[0]);
    return (error.isEmpty());
  }
  
  // Standard mime installation incorrectly identifies JPX files as "image/jp2",
  // hence we go by filename
  if (fileName.endsWith("JPF", Qt::CaseInsensitive) || fileName.endsWith("JPX", Qt::CaseInsensitive)) 
    return readJP2(fileName);
  
  QMimeDatabase DB;
  QString mimeName = DB.mimeTypeForFile(fileName).name();
  
  // JPEG
  if (mimeName == "image/jpeg")
    return readJPEG(fileName);
  
  // None of the special file formats detected, so we use standard methods
  QImage image(fileName);
  if (image.isNull()) {
    error = QString("Error reading image file '%1'.").arg(fileName);
    return false;
  }
  return read(image);
}


void imageInfo::clear()
{
  // Set default values
  error                   = QString();
  heightInPixel           = 0;
  widthInPixel            = 0;
  numberOfColorComponents = 0;
  _xResolution.set(0.0, resolution::dpm);
  _yResolution.set(0.0, resolution::dpm);
}


QList<imageInfo> imageInfo::readAll(const QString &fileName)
{
  QList<imageInfo> result;

  /** There are two known file types that may contain more than one image: JBig2
      and TIFF */

  // Standard mime installation does not know of JBIG2 files, hence we go by
  // filename
  if (fileName.endsWith("JB2", Qt::CaseInsensitive) || fileName.endsWith("JBIG2", Qt::CaseInsensitive)) {
    JBIG2Document jb2(fileName);
    QList<quint32> numbers = jb2.pageNumbers();
    quint32 number;
    foreach(number, numbers) {
      imageInfo info = jb2.pageInfo(number);
      if (!info.error.isEmpty())
	return QList<imageInfo>();
      result << info;
    }
    return result;
  }

  // Handle multi-page TIFF files
  QMimeDatabase DB;
  QString mimeName = DB.mimeTypeForFile(fileName).name();
  if (mimeName == "image/tiff") {
    TIFFReader tiff(fileName);
    for(quint32 i=0; i<tiff.size(); i++) {
      imageInfo info(tiff[i]);
      if (!info.error.isEmpty())
	return QList<imageInfo>();
      result << info;
    }
    return result;
  }

  // Otherwise, have single-image file
  imageInfo info(fileName);
  if (!info.error.isEmpty())
    return QList<imageInfo>();
  result << info;
  return result;
}


bool imageInfo::readJPEG(const QString &fileName)
{
  clear();
  
  // Open file
  FILE *infile = fopen(fileName.toUtf8(), "rb");
  if (infile == nullptr) {
    error = QString("File error. Cannot read JPEG file %1.").arg(fileName);
    return false;
  }
  
  // Prepare structures used by jpeglib
  jpeg_decompress_struct cinfo{};
  errorManager jerr{};
  QString jpegLibMessage;
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = errorHandling;
  jerr.errorMsgPtr = &jpegLibMessage;
  
  // Error handling. If any error occurs, the library will jump here.
  if (setjmp(jerr.setjmp_buffer)) {
    jpeg_destroy_decompress(&cinfo);
    fclose(infile);
    if (!jpegLibMessage.isEmpty())
      jpegLibMessage = " "+jpegLibMessage+".";
    error =  QString("The jpeg library was unable to read or interpret the JPEG file '%1'.%2").arg(fileName,jpegLibMessage);
    return false;
  }
  
  // Now read
  jpeg_create_decompress(&cinfo);
  jpeg_stdio_src(&cinfo, infile);
  jpeg_read_header(&cinfo, TRUE);
  
  // Interpret values
  numberOfColorComponents = cinfo.num_components;
  widthInPixel            = cinfo.image_width;
  heightInPixel           = cinfo.image_height;
  if (cinfo.density_unit == 1) {    // Density is given in dots per inch
    _xResolution.set(cinfo.X_density, resolution::dpi);
    _yResolution.set(cinfo.Y_density, resolution::dpi);
  }
  if (cinfo.density_unit == 2) {    // Density is given in dots per cm
    _xResolution.set(cinfo.X_density, resolution::dpcm);
    _yResolution.set(cinfo.Y_density, resolution::dpcm);
  }
  
  // Delete cinfo, close file.
  jpeg_destroy_decompress(&cinfo);
  fclose(infile);
  
  return true;
}


bool imageInfo::readJP2(const QString &fileName)
{
  // Open file
  QFile file(fileName);
  file.open(QIODevice::ReadOnly);
  
  //
  // Find "JP2 Header Box" in the file
  //
  JP2Box JP2HeaderBox(file, 0x6A703268);
  if (JP2HeaderBox.isNull()) {
    error = QString("JP2 Header box not found. %1").arg(JP2HeaderBox.error);
    return false;
  }
  
  //
  // Go through all sub-boxes of the "JP2 Header Box", find the "Image Header
  // Box" in there. Read width and height from the image header box.
  //
  JP2Box ImageHeaderBox = JP2HeaderBox.subBox(0x69686472);
  if (ImageHeaderBox.TBox == 0) {
    error = QString("Image Header box not found. %1").arg(ImageHeaderBox.error);
    return false;
  }
  quint32 height; quint32 width;
  QBuffer buffer2(&ImageHeaderBox.content);
  buffer2.open(QIODevice::ReadOnly);
  QDataStream stream2(&buffer2);
  stream2 >> height;
  stream2 >> width;
  widthInPixel            = width;
  heightInPixel           = height;

  //
  // Go through all sub-boxes of the "JP2 Header Box", find the "Resolution Box"
  // in there. Then, find the "Default Display Resolution Box" in that. Read
  // horizontal and vertical resolution from the "Default Display Resolution
  // Box".
  //
  JP2Box resolutionBox = JP2HeaderBox.subBox(0x72657320);
  if (!resolutionBox.isNull()) {
    JP2Box ddResolutionBox = resolutionBox.subBox(0x72657364);
    if (!ddResolutionBox.isNull()) {
      quint16 VRdN; quint16 VRdD; quint16 HRdN; quint16 HRdD;
      qint8 VRdE; qint8 HRdE;
      QBuffer buffer3(&ddResolutionBox.content);
      buffer3.open(QIODevice::ReadOnly);
      QDataStream stream3(&buffer3);
      stream3 >> VRdN;
      stream3 >> VRdD;
      stream3 >> HRdN;
      stream3 >> HRdD;
      stream3 >> VRdE;
      stream3 >> HRdE;
      if (VRdD != 0)
	_yResolution.set(0.0254*pow(10.0,VRdE)*(double)VRdN/(double)VRdD, resolution::dpi);
      if (HRdD != 0)
	_xResolution.set(0.0254*pow(10.0,HRdE)*(double)HRdN/(double)HRdD, resolution::dpi);
    }
  }
  
  return true;
}
