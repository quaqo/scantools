/*
 * Copyright © 2017--2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef imageINFO
#define imageINFO 1

#include <QImage>
#include <QString>

#include "resolution.h"


/** Trivial class to store elementary info about bitmap graphics. */

class imageInfo
{
 public:

   /** Default constructor

      The default constructor sets all values to zero and clears the error
      string. */
  imageInfo();

  /** Constructs an image info from a QImage

      This constructor simply calls read(const QImage &)

      @param image A QImage whose data is read
   */
  // cppcheck-suppress noExplicitConstructor
  imageInfo(const QImage &image) {
    read(image);
  }

  /** Constructs an image info from a file

      This constructor simply calls read(const QString &)

      @param fileName Name of input image file
  */
  explicit imageInfo(const QString &fileName) {
    read(fileName);
  }
  
  /** Sets all values to zero */
  void clear();

  /** Reads image info from a QImage

      @param image A QImage whose data is read

      @returns False if the image has format QImage::Format_Invalid, and True otherwise.
   */
  bool read(const QImage &image);

  /** Read image info from an image file

      This method reads an image file and inteprets the information found there.
      If the file contains more than one image (e.g. if the file is a multi-page
      TIFF file), only the first image is considered.
      
      If fileName points to a JPEG or JPEG2000 file (in JP2 or JPX format), the
      data is read without decoding the image data.  The method is therefore
      rather inexpensive in these cases. In case of error, an error message is
      saved in the 'error' member, and all other members are set to their
      default values.
      
      @param fileName Name of an image file in JBIG2, JPEG or JPEG2000 format,
      or in any other format that Qt can read.

      @returns False if the image file could not be read, and True otherwise.
   */
  bool read(const QString &fileName);

  /** Construct image infos for all images in a file

      This method constructs an image info for every image contained in the
      file.  

      @note Currently, only TIFF and JBIG2 files are supported. For all other
      file types, only the first image is read.
      
      @param fileName Name of an image file in JBIG2, JPEG or JPEG2000 format,
      or in any other format that Qt can read.
      
      @returns A list of image infos, or an empty list in case of error
   */
  static QList<imageInfo> readAll(const QString &fileName);
  
  /** Checks if image is empty

      @returns True if width or height are zero
  */
  inline bool isEmpty() const {
    return ((widthInPixel == 0) || (heightInPixel == 0));
  };
  
  /** Vertical resolution

      @returns Vertical resolution
  */
  inline resolution xResolution() const {
    return _xResolution;
  }

  /** Horizontal resolution
      
      @returns Horizontal resolution
  */
  inline resolution yResolution() const {
    return _yResolution;
  }
  
  /** Converts the image info to a human readable string

      This is useful mainly for debugging purposes

      @returns QString describing the image
   */
  inline operator QString() const {
    QString result;
    if (!error.isEmpty())
      result += QString("Error: %1; ").arg(error);
    result += QString("Width: %1px; ").arg(widthInPixel);
    result += QString("Height: %1px; ").arg(heightInPixel);
    result += QString("xRes: %1dpi; ").arg(_xResolution.get(resolution::dpi));
    result += QString("yRes: %1dpi; ").arg(_yResolution.get(resolution::dpi));
    result += QString("color components: %1").arg(numberOfColorComponents);
    return result;
  }
  
  /** Error string */
  QString error;
  
  /** Image height in pixels */
  quint32 heightInPixel{};
  
  /** Number of color components */
  quint8  numberOfColorComponents{};
  
  /** Image width in pixels */
  quint32 widthInPixel{};
  
  /** Horizontal resolution of the image, in dots per inch */
  resolution _xResolution;
  
  /** Horizontal resolution of the image, in dots per inch */
  resolution _yResolution;
  
 private:
  /* Reads info from a JPEG file, without decoding the image data.  The method
     is therefore rather inexpensive.  In case of error, an error message is
     saved in the 'error' member,  all other members are set to their default
     values, and 'false' is returned.  Returns 'true' otherwise.
  */
  bool readJPEG(const QString &fileName);

  /* Reads info from a JPEG2000 file (in JP2 or JPX format), without decoding
     the image data.  The method is therefore rather inexpensive.  In case of
     error, an error message is saved in the 'error' member, all other members
     are set to their default values, and 'false' is returned.  Returns 'true'
     otherwise.
  */
  bool readJP2(const QString &fileName);
};

#endif
