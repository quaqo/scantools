/*
 * Copyright © 2017 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef JP2BOX
#define JP2BOX 1

#include <QIODevice>


/* Data structure used to interpret JP2 and JPX files
    
    According to their specification, JP2 and JPX files are organised in
    'boxes'.  This class can be used to read a box and access its content.
*/

class JP2Box
{
 public:
  /** Constructs an empty JP2Box */
  JP2Box();

  /** Reads a JP2Box from a QIODevice

      This constructors reads a JP2Box from a QIODevice. In case of a file
      error, an empty box is returned that has an error message set.

      @param TBox If this parameter is not zero, the constructor will skip all
      boxes whose TBox value is not the given number, and read the first box
      whose TBox equals the given number. If no such box is found, an empty box
      is returned.
  */
  JP2Box(QIODevice &device, quint32 TBox=0);
  
  /** Reads a Sub-Box

      Depending on the value of the TBox field, some boxes are 'superboxes'
      whose content is a list of other boxes. For these boxes, this method can
      be used to read subboxes.

      @param TBox Similar to the parameter used in the constructor.
  */
  JP2Box subBox(quint32 TBox=0);

  /** Clears all fields */
  void clear();

  /** Check error status */
  bool hasError() const {return !error.isEmpty();}

  /** Check if TBox == 0 */
  bool isNull() const {return TBox == 0;}

  /** TBox field, as explained in the JP2 specification */
  quint32 TBox{};

  /** Content of the box, as explained in the JP2 specification */
  QByteArray content;

  /** Storage for an error message, used by the constructor */
  QString error;
  
 private:
  void read(QIODevice &device, quint32 TBox=0);
};

#endif
