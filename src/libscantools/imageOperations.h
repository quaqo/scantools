/*
 * Copyright © 2017--2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef imageOPERATIONS
#define imageOPERATIONS 1

#include <QImage>
#include <QSet>


/** The namespace imageOperations gathers static methods for handling, analyzing and manipulating QImages. */

namespace imageOperations
{
  /** Removes alpha channel from image 

      This method replaces transparent colors with a suitable opaque colors,
      effectively rendering the image over an opaque image with the given
      background color.

      @param image Image whose alpha channel will be removed

      @param background Background color

      @return Copy of the image, with alpha channel removed
  */
  QImage deAlpha(const QImage &image, QRgb background=0xFFFFFFFF);

  /** Finds the set of all colors used in a given image.

      This method returns a vector containing the colors that used in a given
      image.  No color will appear twice in the vector returned.  In particular,
      if 'image' uses a color index, the method returns a vector that contains
      only those colors that are actually used.  If more than 256 colors are
      used, an empty vector is returned.
      
      @warning This method is slow. This method considers fully transparent
      colors different if their RGB-components differ. For instance, the colors
      0x00FF00FF and 0x00AABBCC are considered different, even though their
      visual appearance will be identical.
      
      @param image Image to be analyzed

      @return Vector with all colors that are actually used

      @see simplifyTransparentPixels()
  */
  QVector<QRgb> colors(const QImage &image);
  
  /** Determine whether an image is black-and-white only
      
      This method determines whether the RGB-part of all pixel colors are
      0x000000 of 0xFFFFFF.
      
      @param image Image to be analyzed

      @return True if the image contains black-and-white only
      
      @warning This method is slow.  An image that uses a fully transparent
      color whose RGB-component is not black or white, such as 0x00FF00FF, is
      not considered a black-and-white image.
      
      @see simplifyTransparentPixels()
  */
  bool isBlackAndWhite(const QImage &image);
  
  /** Check if the image is opaque

      @param image Image to be analyzed
      
      @returns Returns 'true' if the image contains only pixels whose
      alpha-component is different from 0xFF = 'opaque'.  In particular, if the
      image has no alpha channel, then 'true' is returned.
      
      @warning This method is slow.  For images with a color table, this method
      can return 'true' even if the color table contains transparent colors, as
      it might happen that these transparent colore are simply not used in the
      acutal image.
  */
  bool isOpaque(const QImage &image);

  /** Convert image to memory-saving format 
      
      This method analyzes the colors in a given image, determines the most
      memory-efficient format for storing the image and converts it to that
      format.  The output image is guaranteed to be in one of the following
      formats.
      
      - QImage::Format_Mono, with a color table that contains one or two
        colors. In case of two colors, the lighter color is guaranteed to come
        first.
      
      - QImage::Format_Indexed8, with a color table, where all colors are
        actually used, and no color appears twice.
      
      - QImage::Format_Grayscale8.
      
      - QImage::Format_RGB32
      
      - QImage::Format_ARGB32
            
      @param inputImage Input image.
      
      @returns An image equivalent to inputImage in one of the formats listed
      above.

      @warning This method is very slow.
  */
  QImage optimizedFormat(const QImage &inputImage);
  
  /** Replaces all fully transparent pixels by pixels of color 0x00FFFFFF (="fully transparent white")

      @param image Image to be optimized

      @returns A copy of the image where all fully transparent pixels are
      replaced by pixels of color 0x00FFFFFF (="fully transparent white")

      @warning This method is slow on images that do support an alpha channel,
      and do not use a color table.
   */
  QImage simplifyTransparentPixels(const QImage &image);

  /** Image Data Filters

      Several image storage formats, including PDF, PNG and TIFF apply losless
      filter algorithms to the image data before compression. The purpose of
      these filters is to prepare the image data for optimum compression. The
      scantools library supports a number of these.
   */
  
  enum imageDataFilter {
    /** The image data is returned without modification */
    identity,

    /** Filter defined in the PNG specification.

	This filter is used in PNG graphics and in PDF files. It combines five
	methods specified in the PNG Specification ('None', 'Sub', 'Up',
	'Average', 'Paeth') and chooses one of these filters for each scanline
	according to the heurestics 'sum of absolute differences'.

	@see Chapter 3.3 in the PDF reference

	@see Chapter 6.6 in the PNG (Portable Network Graphics) Specification,
	Version 1.2
    */
    PNG
  };
  
  /** Returns the image raw data as a consecutive QByteArray

      QImage organises images in scanlines.  For efficiency reasons, the size of
      a scanline can be larger than the number of bytes actually needed to store
      the pixel data. According to Qt documentation, there is no guarantee that
      the image data is stored in one connected memory block.

      For images in one of the supported formats, this method returns the image
      data in one consecutive block, with the minimum number of bytes
      required. If desired, the method applys a lossless filter algorithm on the
      data, to prepare the image data for optimum compression.  The supportes
      image formats are exactly those returned by optimizedFormat().

      - QImage::Format_Mono: The number of bytes in the output QByteArray is
        round-up(width/8)*height.
	
      - QImage::Format_Indexed8: The number of bytes in the output QByteArray is
        width*height.

      - QImage::Format_ARGB32: The number of bytes in the output QByteArray is
        4*width*height, plus additional bytes if PNG filters are used (see
        below).

      - QImage::Format_RGB888: The number of bytes in the output QByteArray is
        3*width*height, plus additional bytes if PNG filters are used (see
        below).

      - QImage::Format_Grayscale8: The number of bytes in the output QByteArray
        is width*height, plus additional bytes if PNG filters are used (see
        below).

      @param image QImage whose data is returned.

      @param filter The filter to be used.
      
      @returns A QByteArray containing the (filtered) image data, or an empty
      array if the image was empty, if the image format was not supported, or if
      the filter was not available for the given format.
  */
  QByteArray QImageToData(const QImage &image, imageDataFilter filter=identity);
  
  /** Reconstructs an image from raw data, as written by QImageToData

      Reconstructs a QImage from raw data, as written by QImageToData.
      
      @param data QByteArray holding the image data. The size of the array must
      match the size of the data exactly.

      @param width Width of the original image

      @param height Height of the original image

      @param format Format of the original image

      @param filter Filter that was used when the raw data was generated.
      
      @returns Reconstructed image. Note that the method does NOT restore image
      metadata, nor does it store the color vector of indexed images.  In case
      of error, a NULL image is returned.
  */
  QImage DataToQImage(const QByteArray &data, int width, int height, QImage::Format format, imageDataFilter filter=identity);
};

#endif
