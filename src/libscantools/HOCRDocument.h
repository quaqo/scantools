/*
 * Copyright © 2016-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HOCRDOCUMENT
#define HOCRDOCUMENT 1


#include "HOCRTextBox.h"
#include "resolution.h"
#include <QPageSize>
#include <QSet>


/** Reads and interprets HOCR files, the standard output file format for Optical
    Character Recognition systems. It also provides an interfact to the
    tesseract OCR engine.
    
    This class reads and interprets HOCR files, or uses the tesseract OCR engine
    directly to turn an image into an HCOR document, which is then read.  The
    result is a list of text boxes that can be rendered (e.g. onto a QImage) or
    turned into PDF drawing code.

    The methods of this class are reentrant, but not thread safe.
*/

class HOCRDocument
{
 public:
  /** Constructs an empty HOCR document */
  HOCRDocument() {clear();};

  /** Constructs an HOCR document from a QIODevice
      
      This is a convenience constructor that constructs an empty document and
      calls read(device).
      
      @param device Pointer to a QIODevice from which the file is read
  */
  explicit HOCRDocument(QIODevice *device) {read(device);};
  
  /** Constructs an HOCR document from a file

      This is a convenience constructor that constructs an empty document and
      calls read(fileName).
      
      @param fileName Name of the HOCR file
  */
  explicit HOCRDocument(QString fileName) {read(fileName);};

  /** Constructs an HOCR document by running the tesseract OCR engine
      
      This is a convenience constructor that constructs an empty document and
      calls read(image, languages).
      
      @param image Image that is to be OCR'ed

      @param languages List of languages that is passed on to the OCR engine. If
      empty, then "eng" (= English) is used as a default language.
  */
  HOCRDocument(const QImage &image, QStringList languages=QStringList()) {read(image,languages);};  

  /** Resets the document
      
      This method clears all errors, warnings, and any file content.
  */ 
  void clear();

  /** Error status
      
      @returns true if read() terminated with an error. In that case, an error
      description can be retrieved using the error() method.
  */
  bool hasError() const {return !_error.isEmpty();};
  
  /** Error message 
      
      @returns If read() succeeded, the method returns an empty string.
      Otherwise, this method returns a description of the error in enlish
      language.
  */
  QString error() const {return _error; };

  /** Warning status

      @returns true if read() found issues in the JBIG2 file that could be fixed.
      For instance, Google's 'jbig2' encoder is known to produce JBIG2 files
      that contain segements whose 'retain bit for this segment' contains wrong
      values.
   */
  bool hasWarnings() const {return !_warnings.isEmpty();};

  /** Warning messages

      @returns A set with descriptions of fixable issues found in the HOCR file.
   */
  QSet<QString> warnings() const {return _warnings;};

  /** System(s) that generated this file

      @returns If specified in the HOCR file, this method returns strings
      describing the system that generated this file ("tesseract 3.04.01"). If
      nothing is specified, an empty set is returned.
  */
  QSet<QString> system() const {return _OCRSystem;};

  /** OCR capabilites

      If specified in the HOCR file, this method returns a set of strings
      describing the OCR capabilites ("ocr_page ocr_carea ocr_par ocr_line
      ocrx_word"). If nothing is specified, an empty set is returned.

      @returns Set of strings that describe the capabilites.
  */ 
  QSet<QString> capabilities() const {return _OCRCapabilities;};
  
  /** Pages in the document

      @returns a list (possibly empty) containing all the pages of the document,
      in order.
  */
  QList<HOCRTextBox> pages() const {return _pages;};
  
  /** Returns true if the document contains no pages

      @returns true if the document contains no pages, otherwise false
   */
  bool isEmpty() const {return _pages.isEmpty();};

    
  /** Check if the document does contain text.
      
      This differs from 'isEmpty()' because a non-empty document can contain
      empty pages.
      
      @returns True if the document does contain text.
  */
  bool hasText() const;

  /** Removes the first page of the document and returns it.

      This function assumes the document contains pages. To avoid failure, call
      isEmpty() before calling this function.

      @returns First page of the document. If the document is empty, an empty
      HOCRTextBox is returned.
  */
  HOCRTextBox takeFirstPage() {
    if (_pages.size() > 0)
      return _pages.takeFirst();
    else
      return HOCRTextBox();
  };
  
  /** Reads an HOCR document from a QIODevice

      This method clears the document, and reads an HOCR file from a QIODevice.
      After method returns, the caller shoud check if an error occurred, by
      using the methods hasError() and/or error().  The caller might also wish
      to check if the file contained fixable errors, by calling the method
      warnings().

      The absense of an error or of warnings does not imply that the HOCR file
      is valid. In fact, only minimal checks are made.
      
      @param device QIODevice from which the file should be read
  */
  void read(QIODevice *device);

  /** Reads an HOCR document from a file
      
      This is a convenience method that opens a file and calls read(QIODevice
      *device).
      
      @param fileName Name of the HOCR file
  */
  void read(const QString& fileName);

  /** Generates an HOCR document by running the tesseract OCR engine
      
      This method runs the tesseract OCR engine on the given image, and reads
      the results into the present document. If languages are not available in
      the present installation of tesseract, an error condition is set.

      @param image Image that is to be OCR'ed
      
      @param languages The languages specified here will be passed on to
      tesseract, in order to improve recognition quality. Tesseract identifies
      languages by their 3-character ISO 639-2 language codes (e.g. "deu" for
      German or "fra" for French).  The languages specified must be present in
      the current tesseract installation.  If empty, then "eng" (= English) is
      used as a default language.

      @see tesseractLanguages()
  */
  void read(const QImage &image, const QStringList& languages=QStringList());
  
  /** Suggest font

      Suggests a font for rendering this document.  Three standard fonts
      ("Helvetica", "Times", "Courier") are tried, and the one is chose that
      fits the text box best.

      @returns Font that fits the text box best

      @warning This method is expensive
   */
  QFont suggestFont() const;
  
  /** Export to PDF

      Renders the document to a PDF file. The file is not in PDF/A format, as we
      expect that this method will mainly be used for debugging purposes. This
      method must not be called if an error condition is set.

      @param fileName Name of the PDF file that will be created.  If the file
      exists, it will be overwritten.

      @param _resolution Resolution with which the document will be rendered.

      @param title Title string that will be set in PDF file's metadata.

      @param overridePageSize If null, then the page size is computed
      individually for each page from the resolution and page's bounding box. If
      not null, then the given page size will be used for all pages of the
      document.

      @param overrideFont If null, the method will try a few standard fonts, to
      see which one fits the document best. The best font will then be taken. If
      not null, then the specified font will be taken.
      
      @returns An error message. On success, an empty string is returned
  */
  QString toPDF(const QString& fileName, resolution _resolution, const QString& title=QString(), const QPageSize& overridePageSize=QPageSize(), QFont *overrideFont=0) const;
  
  /** Export to images
      
      Renders the pages of the document to sequence of images.  We expect that
      this method will mainly be used for debugging purposes. This method must
      not be called if an error condition is set.
      
      @param overrideFont If null, the method will try a few standard fonts, to
      see which one fits the document best. The best font will then be taken. If
      not null, then the specified font will be taken.
      
      @param format Format of the resulting graphics files
      
      @returns A list of images
  */
  QList<QImage> toImages(QFont *overrideFont=0, QImage::Format format=QImage::Format_Grayscale8) const;
  
  /** Export this document as text
      
      @returns Text contained in the document

      This method must not be called if an error condition is set.
  */
  QString toText() const;

  /** Appends other HOCRDocument
      
      Appends another HOCRDocument to this document. This method must not be
      called if this or if the other document has an error status set.

      @param other Document to be appended to the current one.
   */
  void append(const HOCRDocument &other);

  /** List of languages supported by tesseract

      @returns A list of languages that are found in the current installation of
      the tesseract OCR engine. Typical results are strings such as "eng" for
      English or "deu" for German.
   */
  static QStringList tesseractLanguages();

  /** Check if languages are supported by tesseract

      @param lingos A list of language codes, such as "deu" or "fra"

      @returns true is the QStringList lingos contains a subset of the list
      returned by tesseractLanguages()

      @see tesseractLanguages()
   */
  static bool areLanguagesSupportedByTesseract(const QStringList& lingos);
  
 private:
  // This is a convenience method that suggests a page size for a given page of
  // the document, taking resolution and overridePageSize into account. The
  // reason for the existence of this method is that the computation is needed
  // in two different places in the method exportToPDF, and I wanted to avoid
  // duplicated code.
  QPageSize findPageSize(int pageNumber, resolution _resolution, const QPageSize &overridePageSize) const;

  // Error
  QString _error;

  // System(s) that generated this file, as specified in a meta tag of the HOCR
  // file
  QSet<QString> _OCRSystem;

  // OCR capabilites used in this file, as specified in a meta tag of the HOCR
  // file
  QSet<QString> _OCRCapabilities;

  // Pages of the document
  QList<HOCRTextBox> _pages;
  
  // Warnings
  QSet<QString> _warnings;
};

#endif
