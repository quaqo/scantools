# Subdirectories
add_subdirectory(apps/hocr2any)
add_subdirectory(apps/image2pdf)
add_subdirectory(apps/ocrPDF)
add_subdirectory(libscantools)
