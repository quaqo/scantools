/*
 * Copyright © 2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "reporter.h"
#include "terminal.h"

void reporter::progress(qreal percentage)
{
  percentage = qBound(0.0, percentage, 1.0);
  int pnum = qRound(percentage*100);
  printStatusLine(messageTemplate.arg(pnum));
}

void reporter::progress(int prog)
{
  /*
    percentage = qBound(0.0, percentage, 1.0);
    int pnum = qRound(percentage*100);
  */
  printStatusLine(messageTemplate.arg(prog));
}
