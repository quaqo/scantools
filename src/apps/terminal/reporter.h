/*
 * Copyright © 2016-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>

#ifndef REPORTER
#define REPORTER

/* The reporter is a *very* simple class the received progress information via
   one slot, and prints an appropriate message to stderr */

class reporter : public QObject
{
  Q_OBJECT
  
public slots:
  /* Interprets the number percentage, which must be between 0.0 and 1.0 as a
     percent number, and prints an appropriate message to the status line. */
  void progress(qreal percentage);
  
  /* Interprets the number percentage, which must be between 0.0 and 1.0 as a
     percent number, and prints an appropriate message to the status line. */
  void progress(int prog);
  
 public:
    /* Message template, for the form "Working … %1% done." */
    QString messageTemplate;
};

#endif
