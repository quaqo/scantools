/*
 * Copyright © 2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QString>

#include <iostream>
#include <unistd.h>


// Static variables

extern bool quiet;  // If true, then do not show progress messages


// Title says it all

QString wordwrap(const QString& in, quint16 length);


// Sleep until a key is pressed and return value. Inspired by
// http://michael.dipperstein.com/keypress.html

int keypress(void);


// Move cursor to beginning of line, overwrite line with spaces, move cursor
// again to beginning of line.

void clearTextLine();


// Calls clearTextLine() and prints a line of text to stderr, truncating it to
// terminal width if need be

void printStatusLine(QString text);


// Calls clearTextLine() and prints the text to stderr, word-wrapping the text
// to terminal width if need be

void printMessage(const QString& text);
