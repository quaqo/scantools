/*
 * Copyright © 2016-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "terminal.h"

#include <QStringList>
#include <sys/ioctl.h>
#include <termios.h>


// Static variables

bool quiet;  // If true, then do not show progress messages


// Title says it all

QString wordwrap(const QString& in, quint16 length)
{
  QString out;
  quint32 lineStart = 0;
  QStringList words = in.simplified().split(QRegExp("\\s+"));
  foreach(auto word, words) {
    if (out.size()+1+word.size()-lineStart > length) {
      lineStart = out.size()+1;
      out = out.isEmpty() ? word : out + '\n' + word;
    } else
      out = out.isEmpty() ? word : out + ' ' + word;
  }

  return out;
}


// Sleep until a key is pressed and return value. Inspired by
// http://michael.dipperstein.com/keypress.html

int keypress()
{
  // Save terminal state
  struct termios savedState{};
  if (-1 == tcgetattr(STDIN_FILENO, &savedState))
    return EOF;
  
  // Disable canonical input and disable echo.  Set minimal input to 1.
  struct termios newState = savedState;
  newState.c_lflag &= ~ICANON;
  newState.c_cc[VMIN] = 1;
  if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &newState))
    return EOF;
  
  // Wait until we get a keypress
  int c = getchar();
  
  // Restore the saved terminal state
  if (-1 == tcsetattr(STDIN_FILENO, TCSANOW, &savedState))
    return EOF;
  
  return c;
}


// Move cursor to beginning of line, overwrite line with spaces, move cursor
// again to beginning of line.

void clearTextLine()
{
  if (quiet)
    return;
  
  if (!isatty(STDERR_FILENO))
    return;

  struct winsize w{};
  ioctl(STDERR_FILENO, TIOCGWINSZ, &w);
  
  QString text;
  text.fill(' ', w.ws_col);
  text.prepend("\r");
  text.append("\r");
  std::cerr << qPrintable(text);    
  fflush(stderr);
}


// Print a line of text to stderr, truncating it to terminal width if need be

void printStatusLine(QString text)
{
  if (quiet)
    return;
  
  if (!isatty(STDERR_FILENO))
    return;
	 
  clearTextLine();
  
  struct winsize w{};
  ioctl(STDERR_FILENO, TIOCGWINSZ, &w);
  
  if (text.size() >= w.ws_col)
    text = text.left(w.ws_col-2)+"…";
    
  std::cerr << qPrintable(text);
  fflush(stderr);
}


void printMessage(const QString& text)
{
  // If we have no terminal, clear text
  if (!isatty(STDERR_FILENO)) {
    std::cerr << qPrintable(text) << "\n";
    return;
  }

  // If we do have a terminal, clear the text line, and detect terminal width
  clearTextLine();
  struct winsize w{};
  ioctl(STDERR_FILENO, TIOCGWINSZ, &w);
  int length = w.ws_col;

  // Print word wrapped version of the text
  std::cerr << qPrintable(wordwrap(text, length)) << "\n";
}
