/*
 * Copyright © 2016 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCommandLineParser>
#include <QGuiApplication>
#include <QtConcurrent>
#include <cstdlib>

#include "HOCRDocument.h"
#include "PDFAWriter.h"
#include "scantools.h"
#include "terminal.h"
#include "reporter.h"


// Trivial helper function that takes a file name, and returns the maximum size
// of any image contained in the file, or an empty size if there is an error
// reading the file.

QSize maxImageSize(const QString &arg)
{
    QList<imageInfo> localInfo = imageInfo::readAll(arg);

    QSize mySize(0,0);
    foreach(auto info, localInfo)
        mySize = mySize.expandedTo( QSize(info.widthInPixel, info.heightInPixel) );

    return mySize;
}


// Trivial helper function that takes a file name, and creates an HOCRDocument.

HOCRDocument loadHOCRDocument(const QString &arg)
{
    return HOCRDocument(arg);
}



int main(int argc, char *argv[])
{
    // Although we need to construct a GUI application (because we use fonts…), we
    // need not X server or Wayland platform.  So, it seems best to choose Qt's
    // 'offscreen' platform plugin before we construct the QGuiApplication --
    // otherwise our program could fail in a headless setting because of lack of
    // an X server.
    setenv("QT_QPA_PLATFORM", "offscreen", 1);
    setenv("XDG_SESSION_TYPE", "", 1);

    QGuiApplication app(argc, argv);
    QCoreApplication::setOrganizationName("Albert-Ludwigs-Universität Freiburg");
    QCoreApplication::setOrganizationDomain("uni-freiburg.de");
    QCoreApplication::setApplicationName("scantools");
    QCoreApplication::setApplicationVersion(scantools::versionString);

    /*
   * Command line parsing
   */
    QCommandLineParser parser;
    parser.setApplicationDescription("Convert image files to PDF/A, conformance level PDF/A-2b.\n\n"
                                     "By default, the tesseract OCR engine is used to detect text and to generate a\n"
                                     "text layer in the PDF file.  If the list of input files contains HOCR files,\n"
                                     "these will be used instead to generate the text layer.");

    // Specify positional arguments
    parser.addPositionalArgument("file1 file2 ... ", "Names of image files and HOCR files.");

    // Specify command line options
    QCommandLineOption authorOption(QStringList() << "a" << "author", "Set author in metadata of output file. Defaults to 'unknown'.", "name(s)");
    authorOption.setDefaultValue("unknown");
    parser.addOption(authorOption);
    QCommandLineOption bOption(QStringList() << "b" << "best-compression", "Use slow, but very effective compression for the lossless compression of bitmap graphics.");
    parser.addOption(bOption);
    QCommandLineOption forceOption(QStringList() << "f" << "force", "Overwrite existing files without asking.");
    parser.addOption(forceOption);
    parser.addHelpOption();
    QCommandLineOption keywordsOption(QStringList() << "k" << "keywords", "Set keywords in metadata of output file. Defaults to empty string.", "keywords");
    parser.addOption(keywordsOption);
    QCommandLineOption noOCROption("no-ocr", "Do not run the tesseract OCR engine.");
    parser.addOption(noOCROption);
    QCommandLineOption outputFileOption(QStringList() << "o" << "output", "Name for the resulting PDF file.", "filename");
    parser.addOption(outputFileOption);
    QCommandLineOption OCRLanguageOption("ocr-language", QString("Specify the document language to be used by the tesseract OCR engine. "
                                                                 "Currently installed languages are %1. "
                                                                 "Use a string of the form 'eng+fra' to use multiple languages. "
                                                                 "If nothing is specified, English will be used as a default language.").arg(HOCRDocument::tesseractLanguages().join(", ")),
                                         "language(s)");
    parser.addOption(OCRLanguageOption);
    QCommandLineOption sizeOption(QStringList() << "p" << "paper-size", "Set paper size. "
                                                                        "The argument is a string of the form 'A4', 'Letter', '10x20mm', '8.5in × 11in' etc. "
                                                                        "If no paper size is set, the size of each page is chosen to fit the graphic exactly. "
                                                                        "If a paper size is set, the images will be centered on their pages.", "size");
    parser.addOption(sizeOption);
    QCommandLineOption qOption(QStringList() << "q" << "quiet", "Do not show progress messages.");
    parser.addOption(qOption);
    QCommandLineOption resolutionOption(QStringList() << "r" << "resolution", "Override resolution info found in image files. The argument is a number that "
                                                                              "describes the resolution in dots per inch.  Additionally, if a page size has "
                                                                              "been specified using the option 'paper-size', the word 'fit' may be used as a "
                                                                              "resolution.  The program will then automatically choose a resolution to "
                                                                              "guarantee that the images fit the paper size optimally.\n"
                                                                              ""
                                                                              "This option is mandatory if the image files do not contain correct resolution"
                                                                              "information.", "number or 'fit'");
    resolutionOption.setDefaultValue("0");
    parser.addOption(resolutionOption);
    QCommandLineOption subjectOption(QStringList() << "s" << "subject", "Set subject in metadata of output file. Defaults to empty string.", "subject");
    parser.addOption(subjectOption);
    QCommandLineOption titleOption(QStringList() << "t" << "title", "Set title in metadata of output file. Defaults to 'unknown'.", "title");
    titleOption.setDefaultValue("unknown");
    parser.addOption(titleOption);
    parser.addVersionOption();
    QCommandLineOption nwOption(QStringList() << "w" << "no-warnings", "Do not print warnings to stderr.");
    parser.addOption(nwOption);

    // Parse the actual command line arguments given by the user
    parser.process(app);

    // Evaluate results
    quiet = parser.isSet(qOption);
    QStringList args = parser.positionalArguments();
    if (args.length() < 1)
        parser.showHelp(-1);

    // Split HOCR from image files
    QStringList imageFileNames; QStringList HOCRFileNames;
    foreach(const QString &arg, args)
        if (arg.endsWith("HOC", Qt::CaseInsensitive) || arg.endsWith("HOCR", Qt::CaseInsensitive))
            HOCRFileNames << arg;
        else
            imageFileNames << arg;


    /*
   * Create PDF document. Set metadata and other options
   */
    PDFAWriter doc(parser.isSet(bOption));

    // Meta data
    doc.setAuthor(parser.value(authorOption));
    doc.setKeywords(parser.value(keywordsOption));
    doc.setSubject(parser.value(subjectOption));
    doc.setTitle(parser.value(titleOption));

    // OCR
    doc.setAutoOCR(!parser.isSet(noOCROption));
    if (!parser.value(OCRLanguageOption).isEmpty()) {
        QString errorMsg = doc.setAutoOCRLanguages(parser.value(OCRLanguageOption).split("+"));
        if (!errorMsg.isEmpty()) {
            clearTextLine();
            printMessage(errorMsg);
            return -1;
        }
    }

    // Paper size
    paperSize psize;
    if (parser.isSet(sizeOption)) {
        if (!psize.setSize(parser.value(sizeOption))) {
            printMessage(QString("Error. Cannot interprete argument '%1' of the option '-p' as a paper size.").arg(parser.value(sizeOption)));
            return -1;
        }
        if (psize.isEmpty()) {
            printMessage(QString("Error. Argument '%1' of the option '-p' gives empty paper size.").arg(parser.value(sizeOption)));
            return -1;
        }
        doc.setPageSize(psize);
    }

    // Resolution
    if (parser.isSet(resolutionOption)) {
        if (parser.value(resolutionOption) == "fit") {
            // Parser value is 'fit'. Need to go through all document pages to find
            // the resolution that gives the best match
            if (!parser.isSet(sizeOption)) {
                printMessage(QString("Error. The special value 'fit' for the resolution option is only allowed if a page size has been set."));
                return -1;
            }

            // For every input image file, compute maximal size of images contained in
            // that file
            printStatusLine("Reading image file to find best resolution… ");
            auto sizes = QtConcurrent::blockingMapped(imageFileNames, maxImageSize);
            clearTextLine();

            // Computs maximal size of any image that we have seen
            QSize maxSize(0,0);
            int i=0;
            foreach(auto size, sizes) {
                if (size.isEmpty()) {
                    printMessage(QString("Error. Cannot interprete image file '%1'.").arg(imageFileNames[i]));
                    return -1;
                }

                maxSize = maxSize.expandedTo(size);
                i++;
            }

            // Computs resolution from there
            resolution overrideResolution( qMax(maxSize.width()/psize.width(), maxSize.height()/psize.height()) );
            if (!overrideResolution.isValid()) {
                printMessage(QString("Error. Unable to determine optimal resolution."));
                return -1;
            }

            // Set resolution
            doc.setResolutionOverride(overrideResolution);
        } else {
            // Parser value is a number, which is interpreted as a resolution in dots per inch
            bool ok;
            quint32 _resolution = parser.value(resolutionOption).toUInt(&ok);
            if (!ok) {
                printMessage("Error. Cannot parse argument to resolution option.");
                return -1;
            }
            if ((_resolution != 0) && ((_resolution < resolution::minResDPI) || (_resolution > resolution::maxResDPI))) {
                printMessage(QString("Error. Resolution must be in range [%1..%2]. Aborting.").arg(resolution::minResDPI, resolution::maxResDPI));
                return -1;
            }
            doc.setResolutionOverride(resolution(_resolution, resolution::dpi));
        }
    }

    
    // List of warnings that we may pick up in the course of the action
    QStringList warnings;


    /*
   * Collect all HOCR documents and add them to the PDF
   */

    // Read all HOCR files, create a list of HOCRDocument
    printStatusLine("Reading HOCR files… ");
    auto HOCRDocuments = QtConcurrent::blockingMapped(HOCRFileNames, loadHOCRDocument);
    clearTextLine();

    int i=0;
    foreach(auto HOCRDocument, HOCRDocuments) {
        if (HOCRDocument.hasError()) {
            printMessage(QString("Problem in HOCR file '%1'. %2").arg(HOCRFileNames[i], HOCRDocument.error()));
            return -1;
        }

        // Handle warnings
        foreach(auto warning, HOCRDocument.warnings())
            warnings << QString("The HOCRfile '%1' contains an error that has been fixed: ").arg(HOCRFileNames[i])+warning;

        // Add the data to the PDF file
        doc.appendToOCRData(HOCRDocument);

        i++;
    }


    /*
   * Now add all the graphics files to the PDF
   */
    foreach(auto arg, imageFileNames) {
        printStatusLine( QString("Processing image file '%1'… ").arg(arg) );

        QString errorMsg = doc.addPages(arg, &warnings);
        if (!errorMsg.isEmpty()) {
            clearTextLine();
            printMessage(errorMsg);
            return -1;
        }
    }
    clearTextLine();


    /*
   * Print out all warning messages that we encountered so far
   */
    if (!parser.isSet(nwOption)) {
        foreach(auto warning, warnings)
            printMessage(warning);
    }

    /*
   * Wait for worker threads to finish
   */

    // Set up a reporter object that prints progress messages to the command line
    reporter rep;
    if (parser.isSet(noOCROption))
        rep.messageTemplate = "Working (image compression) … %1% done.";
    else
        rep.messageTemplate = "Working (image compression, character recognition) … %1% done.";
    QObject::connect(&doc, SIGNAL(progress(qreal)), &rep, SLOT(progress(qreal)));

    // Start an event loop that waits for all worker threads to finish
    QObject::connect(&doc, SIGNAL(finished()), &app, SLOT(quit()));
    QTimer::singleShot(0, &doc, SLOT(waitForWorkerThreads()));
    QGuiApplication::exec();


    /*
   * Write out PDF file
   */
    printStatusLine("Waiting for background compression jobs, generating PDF… ");
    QByteArray PDFdata(doc);
    clearTextLine();

    QString outputFileName = parser.value(outputFileOption);
    if (outputFileName == "-") {
        // Write to stdout
        QFile outFile;
        if (!outFile.open(stdout, QIODevice::WriteOnly)) {
            printMessage("Error. Cannot stdout as an output file. Aborting.");
            return -1;
        }
        if (outFile.write(PDFdata) == -1) {
            printMessage("Error. Cannot write to stdout. Aborting");
            return -1;
        }
    } else {
        // Write to file specified in 'outputFileName'
        if (QFile::exists(outputFileName) && !parser.isSet(forceOption)) {
            if (isatty(STDERR_FILENO)) {
                // We're running in a terminal. Ask if we shall overwrite.
                std::cerr << "File exists. Overwrite? [y/N] : ";
                auto inputChar = keypress();
                std::cerr << "\n";
                if ((inputChar != 'y') && (inputChar != 'Y')) {
                    std::cerr << "Abort. \n";
                    return 0;
                }
            } else {
                // We're not running in a terminal. Simply abort.
                printMessage(QString("Error. Outputfile '%1' exists. Aborting without overwriting file.").arg(outputFileName));
                return -1;
            }
        }
        QFile outFile(outputFileName);
        if (!outFile.open(QIODevice::WriteOnly)) {
            printMessage(QString("Error. Cannot open output file '%1'. Aborting.").arg(outputFileName));
            return -1;
        }
        if (outFile.write(PDFdata) == -1) {
            printMessage(QString("Error. Cannot write to output file '%1'. Aborting").arg(outputFileName));
            return -1;
        }
    }

    return 0;
}
