#
# Detect build dependencies
#

# Poppler and QPDF
FIND_PACKAGE(PkgConfig REQUIRED)
PKG_CHECK_MODULES(QPDF REQUIRED libqpdf)
PKG_CHECK_MODULES(POPPLER REQUIRED poppler-qt5)


#
# Build documentation
#

ADD_CUSTOM_COMMAND(OUTPUT ocrPDF.1.gz DEPENDS ocrPDF.1 COMMAND gzip ${PROJECT_SOURCE_DIR}/src/apps/ocrPDF/ocrPDF.1 --stdout >ocrPDF.1.gz)

#
# Build executable
#

ADD_EXECUTABLE(ocrPDF
  ocrPDF.1.gz
  main.cpp
  PDFaddOCR.cpp
  PDFmodifier.cpp
  PDFrenderer.cpp
  ../terminal/reporter.cpp
  ../terminal/terminal.cpp
  )
INCLUDE_DIRECTORIES(${PROJECT_BINARY_DIR}/include)
TARGET_LINK_LIBRARIES(ocrPDF scantools Qt5::Concurrent Qt5::Gui ${POPPLER_LIBRARIES} ${QPDF_LIBRARIES})


#
# Install things
#

INSTALL(TARGETS ocrPDF DESTINATION ${CMAKE_INSTALL_BINDIR})
INSTALL(FILES ${PROJECT_BINARY_DIR}/src/apps/ocrPDF/ocrPDF.1.gz DESTINATION ${CMAKE_INSTALL_MANDIR}/man1)
