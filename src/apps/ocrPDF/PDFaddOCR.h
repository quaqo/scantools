/*
 * Copyright © 2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QFutureWatcher>

#include "PDFmodifier.h"
#include "PDFrenderer.h"

#ifndef PDFADDOCR
#define PDFADDOCR

/* The PDFaddOCR is a *very* simple class that uses a PDF renderer and a PDF
   modifyer to add an OCR layer to a PDF file.

   All methods in this class are reentrant and thread safe.
 */

class PDFaddOCR : public QObject
{
  Q_OBJECT
  
public:
  /** Constructor

      Read a PDF file
   */
  explicit PDFaddOCR(const QString& fileName, QStringList langs);
  
  ~PDFaddOCR();

  /** Render page, perform OCR, generate text layer and and add that to the
      PDF */
  int addOCRtoPage(int pageNumber);

  QStringList languages;
  resolution renderingResolution;
  PDFmodifier modifier;
  PDFrenderer renderer;
  QFutureWatcher<void> watcher;
  QVector<int> pageNumbers;
			  
public slots:
  /** Run add OCR on all pages */
  void addOCR();

signals:
  /** Emitted just before addOCR() returns
      
      This signal is emitted by the methods addOCR(), immediately
      before the method returns.
      
      @see waitForWorkerThreads()
  */
  void finished();
  
  /** Progress indicator
      
      This signal is emitted at irregular intervals while the method addOCR() is
      running, in order to provide progress information.
      
      @param percentage Number in the interval [0.0 .. 1.0] that indicates the
      fraction of PDF objects that are still being constructed by worker threads
      among all PDF objects.

      @see waitForWorkerThreads()
   */
  void progress(qreal percentage);

private slots:
  void progressInt(int jobs);
};

#endif
