/*
 * Copyright © 2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMutexLocker>
#include <qpdf/QPDFWriter.hh>

#include "PDFmodifier.h"
#include "compression.h"

PDFmodifier::PDFmodifier(const QString& PDFFileName) {
  QMutexLocker locker(&lock);
  
  try {
    qpdfDocument.processFile(PDFFileName.toLocal8Bit());
    qpdfPages = qpdfDocument.getAllPages();

    // Add an indirect object with a font descriptor for the built-in font
    // "Times-Roman".
    fontCourier = qpdfDocument.makeIndirectObject(
        QPDFObjectHandle::parse("<< /Type /Font /Subtype /Type1 /BaseFont "
                                "/Courier /Encoding /WinAnsiEncoding >>"));
    fontHelvetica = qpdfDocument.makeIndirectObject(
        QPDFObjectHandle::parse("<< /Type /Font /Subtype /Type1 /BaseFont "
                                "/Helvetica /Encoding /WinAnsiEncoding >>"));
    fontTimes = qpdfDocument.makeIndirectObject(
        QPDFObjectHandle::parse("<< /Type /Font /Subtype /Type1 /BaseFont "
                                "/Times-Roman /Encoding /WinAnsiEncoding >>"));
  } catch (std::exception &e) {
    errors += QString::fromStdString(e.what());
  }
}

PDFmodifier::~PDFmodifier() = default;

void PDFmodifier::addTextLayer(const HOCRTextBox &ocrData, int pageNumber,
                               resolution renderingResolution) {
  QMutexLocker locker(&lock);
  
  if (!ocrData.hasText())
    return;

  QFont suggestedFont = ocrData.suggestFont();
  QByteArray rawPDFContentStream = ocrData.toRawPDFContentStream(suggestedFont, renderingResolution, renderingResolution);

  try {
    QPDFObjectHandle qpdfPage = qpdfPages.at(pageNumber);

    // Make sure that resource dictionary exists, and get a handle for it
    if (!qpdfPage.hasKey("/Resources"))
      qpdfPage.replaceKey("/Resources", QPDFObjectHandle::newDictionary());
    QPDFObjectHandle qpdfResourcesObject = qpdfPage.getKey("/Resources");

    // Make sure that font dictionary exists, and get a handle for it
    if (!qpdfResourcesObject.hasKey("/Font"))
      qpdfResourcesObject.replaceKey("/Font",
                                     QPDFObjectHandle::newDictionary());
    QPDFObjectHandle qpdfFontResourcesObject =
        qpdfResourcesObject.getKey("/Font");

    // Find the first available font name that is not in use yet. We try names
    // of the form "/F1".
    std::string fontName;
    {
      bool nameFound = false;
      for (char letter = 'A'; letter <= 'Z'; letter++) {
        for (int i = 1; i < 10; i++) {
          fontName = QString("/%1%2").arg(letter).arg(i).toStdString();
          if (qpdfFontResourcesObject.hasKey(fontName))
            continue;
          nameFound = true;
          break;
        }
        if (nameFound)
          break;
      }
      if (!nameFound) {
        errors += QString("Error modifying page %1. Could not find acceptable, "
                          "unused font name.")
                      .arg(pageNumber);
        return;
      }
    }

    // Substitute the newly found font name in the raw PDF data.
    rawPDFContentStream.replace("/F1", fontName.c_str());

    // Add the font name to the font resources of the page
    if (suggestedFont.family() == "Courier")
      qpdfFontResourcesObject.replaceKey(fontName, fontCourier);
    else if (suggestedFont.family() == "Helvetica")
      qpdfFontResourcesObject.replaceKey(fontName, fontHelvetica);
    else
      qpdfFontResourcesObject.replaceKey(fontName, fontTimes);

    // Construct new page stream in QByteArray, because QByteArrays support
    // appending and compression
    QByteArray newData;
    newData.append(" q BT 3 Tr "); // This will make a deep copy
    newData.append(rawPDFContentStream);
    newData.append(" ET Q");
    QByteArray newDataCompressed = compression::zopfliCompress(newData);
    
    // Add raw PDF stream to page content.  If compressed data saves us at least 100 bytes, we
    // take that.
    QPDFObjectHandle qpdfContentsObject = QPDFObjectHandle::newStream(&qpdfDocument);
    if (newDataCompressed.size() + 100 < newData.size()) {
      PointerHolder<Buffer> data = new Buffer(newDataCompressed.size());
      memcpy(data->getBuffer(), newDataCompressed.data(), newDataCompressed.size());
      qpdfContentsObject.replaceStreamData(data, QPDFObjectHandle::parse("/FlateDecode"), QPDFObjectHandle::newNull());
    } else {
      PointerHolder<Buffer> data = new Buffer(newData.size());
      memcpy(data->getBuffer(), newData.data(), newData.size());
      qpdfContentsObject.replaceStreamData(data, QPDFObjectHandle::newNull(), QPDFObjectHandle::newNull());
    }
    qpdfPage.addPageContents(qpdfContentsObject, false);
  } catch (std::exception &e) {
    errors += QString::fromStdString(e.what());
  }
}


QString PDFmodifier::write(const QString& filename) {
  QMutexLocker locker(&lock);
  
  try {
    QByteArray string = filename.toLocal8Bit();
    QPDFWriter w(qpdfDocument, string);
    w.write();
  } catch (std::exception &e) {
    return QString::fromStdString(e.what());
  }
  return QString();
}
