/*
 * Copyright © 2016-2018 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMutex>
#include <qpdf/QPDF.hh>
#include <QString>

#include "HOCRTextBox.h"
#include "resolution.h"

#ifndef PDFMODIFIER
#define PDFMODIFIER

/* The PDFmodifier is a *very* simple class that renders a PDF document using poppler */

class PDFmodifier
{
 public:
  explicit PDFmodifier(const QString& fileName);

  ~PDFmodifier();

  void addTextLayer(const HOCRTextBox &ocr, int pageNumber, resolution renderingResolution);

  QString write(const QString& fileName);
  
  QMutex lock;
  QPDF qpdfDocument;
  QPDFObjectHandle fontCourier, fontHelvetica, fontTimes;
  std::vector<QPDFObjectHandle> qpdfPages;
  QStringList errors;
};

#endif
