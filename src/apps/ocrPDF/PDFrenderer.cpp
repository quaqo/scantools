/*
 * Copyright © 2018-2020 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "PDFrenderer.h"


PDFrenderer::PDFrenderer(const QString& PDFFileName)
  : numPages(0), document(nullptr)
{
  document = Poppler::Document::load(PDFFileName);
  if (!document || document->isLocked()) {
    errors += "Cannot load document or document is encrypted";
    return;
  }
  numPages = document->numPages();
}


PDFrenderer::~PDFrenderer()
{
  QWriteLocker locker(&lock);
  
  delete document;
}

QImage PDFrenderer::render(int pageNumber, resolution renderingRes)
{
  // Paranoid safety checks
  if ((document == nullptr) || (pageNumber < 0) || (pageNumber >= numPages))
    return QImage();

  // Lock, so document does not get destroyed while rendering
  QReadLocker locker(&lock); 
  
  // Access page of the PDF file
  Poppler::Page* pdfPage = document->page(pageNumber);  // Document starts at page 0
  if (pdfPage == nullptr)  
    return QImage();
  
  // Generate a QImage of the rendered page
  QImage image = pdfPage->renderToImage(renderingRes.get(resolution::dpi), renderingRes.get(resolution::dpi));
  image.setText("Text", pdfPage->text(QRectF()) );
  
  // Clean up and return
  delete pdfPage;
  
  return image;
}


QString PDFrenderer::text(int pageNumber)
{
  // Paranoid safety checks
  if ((document == nullptr) || (pageNumber < 0) || (pageNumber >= numPages))
    return QString();
  
  // Lock, so document does not get destroyed while rendering
  QReadLocker locker(&lock); 
  
  // Access page of the PDF file
  Poppler::Page* pdfPage = document->page(pageNumber);  // Document starts at page 0
  if (pdfPage == nullptr)  
    return QString();
  
  // Generate a QImage of the rendered page
  QString text = pdfPage->text(QRectF());
  
  // Clean up, return
  delete pdfPage;
  return text;
}
