/*
 * Copyright © 2018 - 2019 Stefan Kebekus <stefan.kebekus@math.uni-freiburg.de>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QCommandLineParser>
#include <QFile>
#include <QGuiApplication>
#include <QtConcurrent>

#include "HOCRDocument.h"
#include "PDFaddOCR.h"
#include "scantools.h"

#include "../terminal/terminal.h"
#include "../terminal/reporter.h"


int main(int argc, char *argv[])
{
  // Although we need to construct a GUI application (because we use fonts…), we
  // need not X server or Wayland platform.  So, it seems best to choose Qt's
  // 'offscreen' platform plugin before we construct the QGuiApplication --
  // otherwise our program could fail in a headless setting because of lack of
  // an X server.
  setenv("QT_QPA_PLATFORM", "offscreen", 1);
  setenv("XDG_SESSION_TYPE", "", 1);
  
  QGuiApplication app(argc, argv);
  QCoreApplication::setOrganizationName("Albert-Ludwigs-Universität Freiburg");
  QCoreApplication::setOrganizationDomain("uni-freiburg.de");
  QCoreApplication::setApplicationName("scantools");
  QCoreApplication::setApplicationVersion(scantools::versionString);

  /*
   * Command line parsing
   */
  QCommandLineParser parser;
  parser.setApplicationDescription("Add a text layer to an image-only PDF file.\n\n"
				   "Uses the tesseract OCR engine is used to detect text and to generate a\n"
				   "text layer in the PDF file.");
  
  // Specify positional arguments
  parser.addPositionalArgument("input.pdf", "Input file");
  parser.addPositionalArgument("-o output.pdf ", "Output file");
  
  // Specify command line options
  QCommandLineOption forceOption(QStringList() << "f" << "force", "Overwrite existing files without asking.");
  parser.addOption(forceOption);
  parser.addHelpOption();
  QCommandLineOption outputFileOption(QStringList() << "o" << "output", "Name for the resulting PDF file.", "filename");
  parser.addOption(outputFileOption);
  QCommandLineOption OCRLanguageOption(QStringList() << "l" << "language",
				       QString("Specify the document language to be used by the tesseract OCR engine. "
					       "Currently installed languages are %1. "
					       "For multi-language documents, use a string of the form 'eng+fra'. "
					       "If nothing is specified, English will be used as a default language.").arg(HOCRDocument::tesseractLanguages().join(", ")),
				       "language(s)");
  parser.addOption(OCRLanguageOption);
  QCommandLineOption qOption(QStringList() << "q" << "quiet", "Do not show progress messages.");
  parser.addOption(qOption);
  
  // Parse the actual command line arguments given by the user
  parser.process(app);
  
  // Evaluate positional argument
  QStringList args = parser.positionalArguments();
  if (args.length() != 1)
    parser.showHelp(-1);
  QString PDFFileName = args[0];

  // Check that an outputfile is given
  if (!parser.isSet(outputFileOption)) {
    printMessage("No output file is speficified.  Use the option '-o' to say where the resulting file should be saved.");
    return -1;
  }

  // Evaluate "quiet" option  
  quiet = parser.isSet(qOption);
  
  
  // Evaluate "language" option;
  QStringList languages = parser.value(OCRLanguageOption).split("+", QString::SkipEmptyParts);
  if (!HOCRDocument::areLanguagesSupportedByTesseract(languages)) {
    printMessage(QString("Language not supported.  The present installation of the tesseract OCR engine supports the following languages: %1.").arg(HOCRDocument::tesseractLanguages().join(", ")));
    return -1;
  }
  
  // Set rendering resolution. At present, this is a fixed constant that cannot
  // be changed.
  resolution renderingResolution(300.0, resolution::dpi);
  
  // Create PDFaddOCR object
  PDFaddOCR coordinator(PDFFileName, languages);
  
  // Load PDF document with poppler
  if (!coordinator.renderer.errors.isEmpty()) {
    printMessage(coordinator.renderer.errors.join(" "));
    return -1;
  }

  // Load PDF document with QPDF
  if (!coordinator.modifier.errors.isEmpty()) {
    printMessage(coordinator.modifier.errors.join(" "));
    return -1;
  }

  // Add OCR text layer to pages
  reporter rep;
  rep.messageTemplate = "Working … %1% done.";
  QObject::connect(&coordinator, SIGNAL(progress(qreal)), &rep, SLOT(progress(qreal)));

  // Start an event loop that waits for all worker threads to finish
  QObject::connect(&coordinator, SIGNAL(finished()), &app, SLOT(quit()));
  QTimer::singleShot(0, &coordinator, SLOT(addOCR()));
  QGuiApplication::exec();
  clearTextLine();
  
  // Get file name and check if file exists
  QString outputFileName = parser.value(outputFileOption);
  // Check if file exists
  if (QFile::exists(outputFileName) && !parser.isSet(forceOption)) {
    if (isatty(STDERR_FILENO)) {
      // We're running in a terminal. Ask if we shall overwrite.
      printStatusLine(QString("File '%1' exists. Overwrite? [y/N] : ").arg(outputFileName));
      auto inputChar = keypress();
      if ((inputChar != 'y') && (inputChar != 'Y')) {
	printMessage(QString("Output file '%1' exists.  Aborting without overwriting file.").arg(outputFileName));
	return -1;
      } 
	printMessage(QString("Overwriting file '%1'.").arg(outputFileName));
    } else {
      // We're not running in a terminal. Simply abort.
      printMessage(QString("Output file '%1' exists. Aborting without overwriting file.").arg(outputFileName));
      return -1;
    }
  }
  
  // Write out resulting file
  QString msg = coordinator.modifier.write(outputFileName);
  if (!msg.isEmpty()) {
    printMessage(QString("Error writing file '%1'. %2").arg(outputFileName, msg));
    return -1;
  }
  
  // Done!
  return 0;
}
